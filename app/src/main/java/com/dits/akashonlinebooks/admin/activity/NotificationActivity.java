package com.dits.akashonlinebooks.admin.activity;

import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.android.volley.toolbox.Volley;
import com.dits.akashonlinebooks.admin.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class NotificationActivity extends AppCompatActivity {

    JSONObject info,json;
    TextInputEditText title,body;
    Button sendBtn;
    CheckBox checkBox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        
        title = findViewById(R.id.title);
        body = findViewById(R.id.body);
        sendBtn = findViewById(R.id.sendBtn);
        checkBox = findViewById(R.id.cb);

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                
                if (TextUtils.isEmpty(title.getText().toString()) && TextUtils.isEmpty(body.getText().toString())){

                    Toast.makeText(NotificationActivity.this, "Fill All Fields!!", Toast.LENGTH_SHORT).show();
                }else {
                    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    Date date = new Date();
                    json = new JSONObject();
                    info = new JSONObject();
                    try {
                        info.put("title", title.getText().toString());
                        info.put("body", body.getText().toString());
                        info.put("message", body.getText().toString());
                        info.put("date",dateFormat.format(date));
                        info.put("image_url","-");

                        if (checkBox.isChecked()){
                            json.put("data", info);
                        }else {
                            json.put("notification", info);
                        }
                        json.put("to","/topics/allDevices");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }finally {
                        sendNotification();
                    }
                }
            }
        });
    }
    void sendNotification(){
        String url = "https://fcm.googleapis.com/fcm/send";
        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.POST,url, json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        title.setText("");
                        body.setText("");
                        Toast.makeText(NotificationActivity.this, "sent", Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(NotificationActivity.this, "Opps!! "+error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", "key=AIzaSyCJOrn9A1ZnhnowMrTmxyrNEVqp12758zI");
                return headers;
            }
        };


        queue.add(jsonObjReq);
    }
}
