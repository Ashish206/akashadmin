package com.dits.akashonlinebooks.admin.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.activity.OfferDetailsActivity;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.dits.akashonlinebooks.admin.model.OfferModel;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;

import static com.dits.akashonlinebooks.admin.extra.Constant.offerZoneRef;


public class OfferZoneFragment extends Fragment {
    FloatingActionButton addFab;
    ProgressDialog progressDialog;
    private Uri resultUri;
    private ImageView profileImage;
    RecyclerView itemList;
    TextView notDataText;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v =  inflater.inflate(R.layout.fragment_offer_zone, container, false);

        addFab = v.findViewById(R.id.add_fab_btn);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        notDataText = v.findViewById(R.id.not_data_text);




        addFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddItemDialog();
            }

        });

        itemList = v.findViewById(R.id.itemList);
        itemList.setHasFixedSize(true);
        itemList.setLayoutManager(new GridLayoutManager(getContext(), 1));



        return v;
    }

    private void showAddItemDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setTitle("Add Category");
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View addBooks = inflater.inflate(R.layout.add_offer_activity,null);
        final TextInputEditText fieldFirst = addBooks.findViewById(R.id.field_first);
        fieldFirst.setHint("Category Name");

        final Button saveBtn = addBooks.findViewById(R.id.saveBtn);
        final Button cancelBtn = addBooks.findViewById(R.id.cancelBtn);
        profileImage = addBooks.findViewById(R.id.productImage);


        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, 1);
            }
        });
        dialog.setCancelable(false);
        dialog.setView(addBooks);


        final AlertDialog alertDialog = dialog.create();

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(fieldFirst.getText().toString())
                       ){
                    progressDialog.show();

                    String key =  offerZoneRef.push().getKey();
                    HashMap hm = new HashMap();
                    hm.put("category",fieldFirst.getText().toString().toLowerCase());
                    hm.put("id",key);


                    offerZoneRef.child(key).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(Task task) {
                            if (task.isSuccessful()){
                                progressDialog.dismiss();
                                alertDialog.dismiss();
                                resultUri =null;
                                Toast.makeText(getContext(), "Item Added!!", Toast.LENGTH_SHORT).show();
                            }else {
                                progressDialog.dismiss();
                                alertDialog.dismiss();
                                Toast.makeText(getContext(), "Oops/"+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                }
                else {
                    Toast.makeText(getContext(), "Please Fill All Fields...", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });


        alertDialog.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        loadCategoryList();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void loadCategoryList() {
        FirebaseRecyclerOptions<OfferModel> options =
                new FirebaseRecyclerOptions.Builder<OfferModel>()
                        .setQuery(offerZoneRef, OfferModel.class)
                        .build();

        loadList(options);
    }

    private void loadList(FirebaseRecyclerOptions<OfferModel> options) {
        FirebaseRecyclerAdapter adapter = new FirebaseRecyclerAdapter<OfferModel, ItemViewHolder>(options) {
            @Override
            public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.single_offer_zone_layout, parent, false);

                return new ItemViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(ItemViewHolder holder, int position, final OfferModel model) {
                holder.setItemName(model.getCategory());
                final String id =getRef(position).getKey();
                holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        if (Constant.admin){
                            editDialog(model.getCategory(),id);
                        }
                        return false;
                    }
                });


                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(getContext(), OfferDetailsActivity.class);
                        i.putExtra("id",id);
                        startActivity(i);
                    }
                });

            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                if (progressDialog != null && progressDialog.isShowing()) {
                    if (getItemCount()!=0){
                        notDataText.setVisibility(View.GONE);
                    }else {
                        notDataText.setVisibility(View.VISIBLE);
                    }
                    progressDialog.dismiss();
                }

            }
        };
        adapter.startListening();
        itemList.setAdapter(adapter);
    }

    private void editDialog(String bookName, final String id) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setTitle("Edit Category");
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View addBooks = inflater.inflate(R.layout.add_offer_activity,null);
        final TextInputEditText fieldFirst = addBooks.findViewById(R.id.field_first);
        fieldFirst.setHint("Category");

        final Button saveBtn = addBooks.findViewById(R.id.saveBtn);
        final Button cancelBtn = addBooks.findViewById(R.id.cancelBtn);
        profileImage = addBooks.findViewById(R.id.productImage);
        ImageView deleteBtn = addBooks.findViewById(R.id.delete_btn);

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog(id);
            }
        });

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, 1);
            }
        });
        dialog.setCancelable(false);
        dialog.setView(addBooks);


        //Glide.with(getActivity().getApplicationContext()).load(photo).into(profileImage);
        fieldFirst.setText(bookName);


        final AlertDialog alertDialog = dialog.create();

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(fieldFirst.getText().toString())
                        ){
                    progressDialog.show();

                    HashMap hm = new HashMap();
                    hm.put("category",fieldFirst.getText().toString().toLowerCase());

                    offerZoneRef.child(id).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(Task task) {
                            if (task.isSuccessful()){
                                progressDialog.dismiss();
                                alertDialog.dismiss();
                                Toast.makeText(getContext(), "Item Added!!", Toast.LENGTH_SHORT).show();
                            }else {
                                progressDialog.dismiss();
                                alertDialog.dismiss();
                                Toast.makeText(getContext(), "Oops/"+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                }
                else {
                    Toast.makeText(getContext(), "Please Fill All Fields...", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });


        alertDialog.show();
    }

    private void deleteDialog(final String id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Delete");
        builder.setMessage("Do you want to delete this item?");
        builder.setCancelable(false);
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                offerZoneRef.child(id).setValue(null);
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            final Uri imageUri = data.getData();
            resultUri = imageUri;
            profileImage.setImageURI(resultUri);
        }
    }

    private static class ItemViewHolder extends RecyclerView.ViewHolder {
        View mView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setItemName(String item) {
            TextView book_name = mView.findViewById(R.id.heading);
            book_name.setText(item);

        }

    }
}
