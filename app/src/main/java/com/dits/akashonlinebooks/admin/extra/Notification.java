package com.dits.akashonlinebooks.admin.extra;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Notification {
    Context context;
    JSONObject info,json;

    public Notification(Context ctx){
        context = ctx;
    }

    public void sendNotificationToSubs(String title,String body,String channel){
        json = new JSONObject();
        info = new JSONObject();
        try {
            info.put("title", title);
            info.put("body", body);
            json.put("notification", info);
            json.put("to","/topics/"+channel);

        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            sendNotification();
        }
    }

    void sendNotification(){
        String url = "https://fcm.googleapis.com/fcm/send";
        RequestQueue queue = Volley.newRequestQueue(context);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.POST,url, json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {


                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("-------->",error.getMessage());

            }
        }) {

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", "key=AIzaSyCJOrn9A1ZnhnowMrTmxyrNEVqp12758zI");
                return headers;
            }
        };


        queue.add(jsonObjReq);
    }

}
