package com.dits.akashonlinebooks.admin.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.activity.DeliveryBoy.DeliveryActivity;
import com.dits.akashonlinebooks.admin.activity.DispatchActivity.DispatchActivity;
import com.dits.akashonlinebooks.admin.activity.PackingActivity.PackingActivity;
import com.dits.akashonlinebooks.admin.activity.PickingActivity.PickingDetailsActivity;
import com.dits.akashonlinebooks.admin.adapter.HomeMenuAdapter;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.dits.akashonlinebooks.admin.model.HomeMenuModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements HomeMenuAdapter.ItemListener{
    RecyclerView recyclerView;
    ArrayList arrayList;
    ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        sharedPreferences = getSharedPreferences("Main",MODE_PRIVATE);
        editor = sharedPreferences.edit();

        Constant.adminType = sharedPreferences.getString("AdminType","null");
        Constant.adminName = sharedPreferences.getString("AdminName","null");
        Constant.adminUID = sharedPreferences.getString("Key","null");
        Constant.adminPhone = sharedPreferences.getString("Phone","null");
        Constant.permission = sharedPreferences.getString("permission","null");
        Constant.rootRef = FirebaseDatabase.getInstance().getReference();
        Constant.deliveryBoyRegistrationRef = Constant.rootRef.child("AdminGroup").child("AdminGroup");
        Constant.packingOrderRef = Constant.rootRef.child("PackingOrder").child("Jabalpur").child("PackingOrder");
        Constant.pickingOrderRef = Constant.rootRef.child("AdminGroup").child("AdminGroup").child(Constant.adminUID).child("Orders");
        Constant.deliveryOrderRef = Constant.rootRef.child("AdminGroup").child("AdminGroup").child(Constant.adminUID).child("DeliveryOrder");
        Constant.orderref = Constant.rootRef.child("Orders").child("Jabalpur").child("Orders");
        Constant.requestBookRef = Constant.rootRef.child("Requests").child("Jabalpur");
        Constant.offerZoneRef = Constant.rootRef.child("OfferZone");
        Constant.dispatchOrder = Constant.rootRef.child("DispatchOrders").child("Jabalpur").child("DispatchOrders");
        Constant.deliveryRef = Constant.rootRef.child("DeliveryBoy").child("DeliveryBoy");
        Constant.categoryRef = FirebaseDatabase.getInstance().getReference().child("Study");

        Constant.rootRef.child("Study").child("Category").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Constant.categoryList = new ArrayList();
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    Constant.categoryList.add(ds.child("category").getValue().toString());
                }
                loadAuthor();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



        recyclerView = findViewById(R.id.recyclerView);
        arrayList = new ArrayList();
        if (Constant.permission.toLowerCase().contains(("Home Screen").toLowerCase())){
            arrayList.add(new HomeMenuModel("Home Screen", R.drawable.house, "#607D8B"));

        }
        if (Constant.permission.toLowerCase().contains(("Notification").toLowerCase())){
            arrayList.add(new HomeMenuModel("Notification", R.drawable.notification, "#536DFE"));
        }
        if (Constant.permission.toLowerCase().contains(("Order Details").toLowerCase())){
            arrayList.add(new HomeMenuModel("Order Details", R.drawable.diagram, "#512DA8"));
        }
        if (Constant.permission.toLowerCase().contains(("Book Details").toLowerCase())){
            arrayList.add(new HomeMenuModel("Book Details", R.drawable.bookshelf, "#388E3C"));
        }
        if (Constant.permission.toLowerCase().contains(("Statistics").toLowerCase())){
            arrayList.add(new HomeMenuModel("Statistics", R.drawable.analysis, "#FFC107"));
        }
        if (Constant.permission.toLowerCase().contains(("Admin Group").toLowerCase())){
            arrayList.add(new HomeMenuModel("Admin Group", R.drawable.deliveryman, "#FF5722"));
        }
        if (Constant.permission.toLowerCase().contains(("Offer Zone").toLowerCase())){
            arrayList.add(new HomeMenuModel("Offer Zone", R.drawable.menu_request, "#9E9E9E"));
        }
        if (Constant.permission.toLowerCase().contains(("Book Requests").toLowerCase())){
            arrayList.add(new HomeMenuModel("Book Requests", R.drawable.menu_request, "#9E9E9E"));
        }
        if (Constant.permission.toLowerCase().contains(("Picked Orders").toLowerCase())){
            arrayList.add(new HomeMenuModel("Picked Orders", R.drawable.menu_request, "#9E9E9E"));
        }
        if (Constant.permission.toLowerCase().contains(("Pack Orders").toLowerCase())){
            arrayList.add(new HomeMenuModel("Pack Orders", R.drawable.menu_request, "#9E9E9E"));
        }
        if (Constant.permission.toLowerCase().contains(("Extra").toLowerCase())){
            arrayList.add(new HomeMenuModel("Extra", R.drawable.menu_request, "#9E9E9E"));
        }
        if (Constant.permission.toLowerCase().contains(("Undelivered Packed Orders").toLowerCase())){
            arrayList.add(new HomeMenuModel("Undelivered Packed Orders", R.drawable.menu_request, "#9E9E9E"));
        }if (Constant.permission.toLowerCase().contains(("Order Delivery").toLowerCase())){
            arrayList.add(new HomeMenuModel("Order For Delivery", R.drawable.menu_request, "#9E9E9E"));
        }
        if (Constant.permission.toLowerCase().contains(("Non Image Books").toLowerCase())){
            arrayList.add(new HomeMenuModel("Non Image Books", R.drawable.menu_request, "#9E9E9E"));
        }
        if (Constant.permission.toLowerCase().contains(("FeedBack").toLowerCase())){
            arrayList.add(new HomeMenuModel("FeedBack", R.drawable.menu_request, "#9E9E9E"));
        }

        if (Constant.permission.toLowerCase().contains(("Web Panel").toLowerCase())){
            arrayList.add(new HomeMenuModel("Web Panel", R.drawable.menu_request, "#9E9E9E"));
        }

//        if (Constant.permission.toLowerCase().contains(("Register Delivery Boy").toLowerCase())){
//            arrayList.add(new HomeMenuModel("Register Delivery Boy", R.drawable.menu_request, "#9E9E9E"));
//        }


        HomeMenuAdapter adapter = new HomeMenuAdapter(this, arrayList, this);
        recyclerView.setAdapter(adapter);

        @SuppressLint("WrongConstant") GridLayoutManager manager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setNestedScrollingEnabled(false);

        FirebaseMessaging.getInstance().subscribeToTopic(Constant.adminType);
    }

    private void loadAuthor() {

        Constant.rootRef.child("Study").child("Authors").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Constant.authorList = new ArrayList();
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    Constant.authorList.add(ds.child("category").getValue().toString());
                }
                loadPublishers();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void loadPublishers() {
        Constant.rootRef.child("Study").child("Publishers").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Constant.publishList = new ArrayList();
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    Constant.publishList.add(ds.child("category").getValue().toString());
                }
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mymenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.mybutton) {
            editor.putString("Phone","null");
            editor.apply();
            startActivity(new Intent(MainActivity.this,LoginActivity.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(HomeMenuModel item) {
        
        switch (item.text){
            case "Admin Group":
                startActivity(new Intent(MainActivity.this, RegisterAdminGroup.class));
                break;
            case "Order Details":
                startActivity(new Intent(MainActivity.this,OrderDetails.class));
                break;
            case "Notification":
                startActivity(new Intent(MainActivity.this,NotificationActivity.class));
                break;
            case "Statistics":
                startActivity(new Intent(MainActivity.this,StatisticsActivity.class));
                break;
            case "Book Details":
                startActivity(new Intent(MainActivity.this, BookDetailsActivity.class));
                break;
            case "Book Requests":
                startActivity(new Intent(MainActivity.this, RequestDetails.class));
                break;
            case "Offer Zone":
                startActivity(new Intent(MainActivity.this, OfferActivity.class));
                break;
            case "Extra":
                startActivity(new Intent(MainActivity.this, UserFormActivity.class));
                break;
            case "Picked Orders":
                startActivity(new Intent(MainActivity.this, PickingDetailsActivity.class));
                break;
            case "Pack Orders":
                startActivity(new Intent(MainActivity.this, PackingActivity.class));
                break;
            case "Home Screen":
                startActivity(new Intent(MainActivity.this, HomeEditActivity.class));
                break;
            case "Undelivered Packed Orders":
                startActivity(new Intent(MainActivity.this, DispatchActivity.class));
                break;
            case "FeedBack":
                startActivity(new Intent(MainActivity.this, FeedBackActivity.class));
                break;
            case "Register Delivery Boy":
                startActivity(new Intent(MainActivity.this, RegisterDeliveryBoy.class));
                break;
            case "Order For Delivery":
                startActivity(new Intent(MainActivity.this, DeliveryActivity.class));
                break;
            case "Non Image Books":
                startActivity(new Intent(MainActivity.this, NonImageBooksActivity.class));
                break;
            case "Web Panel":
                startActivity(new Intent(MainActivity.this, WebPanel.class));
                break;
            default:
                Toast.makeText(this, "This feature is currently not available.", Toast.LENGTH_SHORT).show();
        }

    }
}
