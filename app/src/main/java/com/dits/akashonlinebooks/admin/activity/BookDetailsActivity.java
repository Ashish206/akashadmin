package com.dits.akashonlinebooks.admin.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Parcelable;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.adapter.BooksDetailsAdapter;
import com.dits.akashonlinebooks.admin.extra.ApiClient;
import com.dits.akashonlinebooks.admin.extra.ApiInterface;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.dits.akashonlinebooks.admin.fragments.BookListFragment;
import com.dits.akashonlinebooks.admin.model.BookDataModel;
import com.dits.akashonlinebooks.admin.model.OfferGetIdModel;
import com.dits.akashonlinebooks.admin.model.SearchResultModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookDetailsActivity extends AppCompatActivity implements BooksDetailsAdapter.ItemListener,BooksDetailsAdapter.ItemLongClickListener {
    public static SearchView searchView;
    TextView heading;
    RecyclerView recyclerView;
    BooksDetailsAdapter bookDetailsActivity;
    ArrayList<BookDataModel> bookData;
    public static ArrayList<String> isbnList;
    ProgressDialog progressDialog;
    FloatingActionButton addBtn;
    Parcelable state;
    LinearLayoutManager linearLayoutManager;
    FragmentManager fragmentManager;

    List<OfferGetIdModel> offerList;
    DatabaseReference offerBooksRef;
    public static boolean isEdit = false;
    private ApiInterface movieService;
    private SimpleCursorAdapter mAdapter;
    private ArrayList<SearchResultModel> SUGGESTIONS;
    ClearFilterListenerHome clearFilterListenerHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);


        movieService = ApiClient.getClient().create(ApiInterface.class);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);

        fragmentManager = getSupportFragmentManager();
        BookListFragment gridFragment = new BookListFragment();
        fragmentManager.beginTransaction().replace(R.id.frame2,gridFragment)
                .commit();

        bookData = new ArrayList<>();
        isbnList = new ArrayList<>();
        SUGGESTIONS = new ArrayList();

        offerBooksRef = FirebaseDatabase.getInstance().getReference().child("OfferZoneBooks");

        searchView = findViewById(R.id.search_bar);
        addBtn = findViewById(R.id.addBtn);
        heading = findViewById(R.id.titleText);
        heading.setText("Book Details");

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(BookDetailsActivity.this,BookDetailEditActivity.class);
                i.putExtra("key","--");
                startActivity(i);
            }
        });


        offerList = new ArrayList();

        recyclerView = findViewById(R.id.recycler_view);

        linearLayoutManager = new LinearLayoutManager(this);
        if(state != null) {
            linearLayoutManager.onRestoreInstanceState(state);
        }
        recyclerView.setLayoutManager(linearLayoutManager);


        //loadBooks();
        loadOffer();

        final String[] from = new String[] {"filter"};
        final int[] to = new int[] {android.R.id.text1};
        mAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_1,
                null,
                from,
                to,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);


        searchView.setSuggestionsAdapter(mAdapter);

        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int i) {

                return false;
            }

            @Override
            public boolean onSuggestionClick(int i) {
                Cursor cursor = (Cursor) mAdapter.getItem(i);
                String txt = cursor.getString(cursor.getColumnIndex("filter"));
                searchView.setQuery(txt, true);
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                getFinalSearchResult(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                try{
                    getSearchResultFromApi(s);

                }catch (Exception e){

                }

                return false;
            }
        });

        int searchCloseButtonId = searchView.getContext().getResources()
                .getIdentifier("android:id/search_close_btn", null, null);
        ImageView closeButton =  this.searchView.findViewById(searchCloseButtonId);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setQuery(null,false);
                clearFilterListenerHome = BookListFragment.ctx;
                if (clearFilterListenerHome!=null){
                    clearFilterListenerHome.ClearFilterListenerHome();
                }
            }
        });


    }

    private void getFinalSearchResult(String s) {
        BookListFragment.adapter.clear();
        getSearchDetailsResult(s).enqueue(new Callback<List<BookDataModel>>() {
            @Override
            public void onResponse(Call<List<BookDataModel>> call, Response<List<BookDataModel>> response) {
                for(BookDataModel s : response.body()){
                    BookListFragment.adapter.add(s);
                }
                BookListFragment.isLastPage = true;
                BookListFragment.adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<BookDataModel>> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(BookDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getSearchResultFromApi(String key){
        SUGGESTIONS.clear();
        getSearchResult(key).enqueue(new Callback<List<SearchResultModel>>() {
            @Override
            public void onResponse(Call<List<SearchResultModel>> call, Response<List<SearchResultModel>> response) {
                for(SearchResultModel s : response.body()){
                    SUGGESTIONS.add(s);
                }
                populateAdapter();
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<SearchResultModel>> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(BookDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void populateAdapter() {
        final MatrixCursor c = new MatrixCursor(new String[]{ BaseColumns._ID, "filter" });
        for (int i=0; i<SUGGESTIONS.size(); i++) {
            c.addRow(new Object[] {i, SUGGESTIONS.get(i).getBookName()});
        }
        mAdapter.changeCursor(c);
    }

    @Override
    protected void onPause() {
        super.onPause();
        state = linearLayoutManager.onSaveInstanceState();
    }

    private void loadBooks() {
        progressDialog.show();
        Constant.rootRef.child("BooksDetail").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                bookData.clear();
                for (final DataSnapshot ds : dataSnapshot.getChildren()) {
                    BookDataModel bookDataModel = new BookDataModel();
                    bookDataModel.setBookName(ds.child("BookName").getValue().toString());
                    bookDataModel.setAuthor(ds.child("Author").getValue().toString());
                    bookDataModel.setCategory(ds.child("Category").getValue().toString());
                    bookDataModel.setPublisher(ds.child("Publisher").getValue().toString());
                    bookDataModel.setImage(ds.child("Image").getValue().toString());
                    bookDataModel.setMRP(ds.child("MRP").getValue().toString());
                    bookDataModel.setActualCost(ds.child("actualCost").getValue().toString());
                    bookDataModel.setBookKey(ds.child("key").getValue().toString());
                    bookDataModel.setIsbn(ds.child("isbn").getValue().toString());
                    isbnList.add(ds.child("isbn").getValue().toString());
                    bookData.add(bookDataModel);
                }
                bookDetailsActivity = new BooksDetailsAdapter(getApplicationContext(), bookData,BookDetailsActivity.this,BookDetailsActivity.this);
                recyclerView.setAdapter(bookDetailsActivity);
                progressDialog.dismiss();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private Call<List<SearchResultModel>> getSearchResult(String searchKey) {
        return movieService.getSearchResult(
                searchKey
        );
    }

    private Call<List<BookDataModel>> getSearchDetailsResult(String searchKey) {
        return movieService.getSearchDetailsResult(
                searchKey
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (state != null) {
            linearLayoutManager.onRestoreInstanceState(state);
        }
        if (isEdit){
            //loadBooks();
            isEdit=false;
            loadOffer();
            clearFilterListenerHome = BookListFragment.ctx;
            if (clearFilterListenerHome!=null){
                clearFilterListenerHome.ClearFilterListenerHome();
            }
        }

    }

    private void loadOffer() {
        Constant.offerZoneRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    offerList.add(ds.getValue(OfferGetIdModel.class));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onItemClick(String key) {
        Intent i = new Intent(BookDetailsActivity.this,BookDetailEditActivity.class);
        i.putExtra("key",key);
        startActivity(i);
    }

    @Override
    public void onItemLongClickClick(String key) {
        String[] values = new String[offerList.size()];
        for(int i=0; i<offerList.size(); i++){
            values[i] = offerList.get(i).getCategory();
        }
        showDialog(values,key);
    }

    private void showDialog(String[] values, final String id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select");

        builder.setItems(values, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String key = offerBooksRef.child(offerList.get(which).getId()).push().getKey();
                offerBooksRef.child(offerList.get(which).getId()).child(key).child("id").setValue(id).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(BookDetailsActivity.this, "Added.", Toast.LENGTH_SHORT).show();
//                        loadBooks();
                    }
                });
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public interface ClearFilterListenerHome {
        void ClearFilterListenerHome();
    }

}
