package com.dits.akashonlinebooks.admin.activity.DeliveryBoy;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.activity.BookDetailEditActivity;
import com.dits.akashonlinebooks.admin.activity.PackingActivity.PackingActivity;
import com.dits.akashonlinebooks.admin.activity.PackingActivity.PackingDetailActivity;
import com.dits.akashonlinebooks.admin.activity.StatisticsActivity;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.dits.akashonlinebooks.admin.model.HistoryListModel;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DeliveryActivity extends AppCompatActivity {

    FloatingActionButton camBtn;
    private IntentIntegrator qrScan;
    FirebaseRecyclerOptions<HistoryListModel> options;
    RecyclerView historyList;
    LinearLayoutManager linearLayoutManager;
    ProgressDialog progressDialog;
    TextView totalOrders,deliveredOrders,cancelled_orders,pending_orders,totalPayingAmount,tvShowHide;
    LinearLayout lay1,lay2;

    DatePickerDialog datePickerDialog;
    Calendar c;
    int year;
    int month;
    int day;
    DateFormat dateFormat;
    String formatedDate;

    ImageView datePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery);

        camBtn = findViewById(R.id.cameraBtn);
        qrScan = new IntentIntegrator(this);
        qrScan.setOrientationLocked(false);
        camBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                qrScan.initiateScan();
            }
        });

        historyList = findViewById(R.id.historyList);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        historyList.setLayoutManager(linearLayoutManager);
        historyList.setHasFixedSize(true);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        totalOrders = findViewById(R.id.total_orders);
        deliveredOrders = findViewById(R.id.delivered_orders);
        cancelled_orders = findViewById(R.id.cancelled_order);
        pending_orders = findViewById(R.id.pending_orders);
        totalPayingAmount = findViewById(R.id.paying_amount);
        tvShowHide = findViewById(R.id.tvShowHide);

        lay1 = findViewById(R.id.lay_1);
        lay2 = findViewById(R.id.lay_2);

        datePicker = findViewById(R.id.calanderBtn);
        dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);



        tvShowHide.setText("Show");
        lay1.setVisibility(View.GONE);
        lay2.setVisibility(View.GONE);
        tvShowHide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tvShowHide.getText().equals("Hide")){
                    tvShowHide.setText("Show");
                    lay1.setVisibility(View.GONE);
                    lay2.setVisibility(View.GONE);
                }else {
                    tvShowHide.setText("Hide");
                    lay1.setVisibility(View.VISIBLE);
                    lay2.setVisibility(View.VISIBLE);
                }
            }
        });

        Date oldDate = null;
        try {
            oldDate = new SimpleDateFormat("yyyy-MM-dd").parse(year+"-"+month+"-"+day);
            formatedDate = dateFormat.format(oldDate);
            getStats(formatedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                int modMonth = month+1;
                try {
                    Date oldDate = new SimpleDateFormat("yyyy-MM-dd").parse(year+"-"+modMonth+"-"+dayOfMonth);
                    formatedDate = dateFormat.format(oldDate);
                    getStats(formatedDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        },year,month,day);

        datePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        loadHistory("null");
    }

    private void getStats(String date){
        progressDialog.show();
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = Constant.BASE_URL+"getDeliveryBoyStats?date="+date+"&deliveryBoyNo="+Constant.adminPhone.replace("+91","");

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                setData(response);
                Log.d("---------->",response);
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(DeliveryActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(stringRequest);
    }

    private void setData(String response) {
        try {
            JSONArray jsonArray = new JSONArray(response);
            JSONObject job = jsonArray.getJSONObject(0);

            totalOrders.setText("Total\n"+job.optString("order_count"));
            deliveredOrders.setText("Delivered\n"+job.optString("delivered_order"));
            cancelled_orders.setText("Cancelled\n"+job.optString("cancelled_order"));
            pending_orders.setText("Pending\n"+job.optString("pending_orders"));
            totalPayingAmount.setText("Paying Amount\n"+job.optString("paying_amount"));


        }catch (Exception e){
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
            } else {
                if (result.getContents()!=null){
                    processData(result.getContents());
                }else {
                    Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void processData(final String contents) {
           String[] data =contents.split(" ");
           final String orderId = data[0];
           Log.d("---------->",orderId);
           final String region = data[1];
        Constant.dispatchOrder.child(orderId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue()  != null){
                    if (dataSnapshot.child("deliveryBoyName").getValue().toString().toLowerCase().equals("not selected")) {
                        HashMap hm = new HashMap();
                        hm.put("TotalAmount", dataSnapshot.child("TotalAmount").getValue().toString());
                        hm.put("date", dataSnapshot.child("date").getValue().toString());
                        hm.put("orderIdUser", dataSnapshot.child("orderIdUser").getValue().toString());
                        hm.put("order_id", dataSnapshot.child("order_id").getValue().toString());
                        hm.put("paymentMethod", dataSnapshot.child("paymentMethod").getValue().toString());
                        hm.put("userAddress", dataSnapshot.child("userAddress").getValue().toString());
                        hm.put("userCity", dataSnapshot.child("userCity").getValue().toString());
                        hm.put("userId", dataSnapshot.child("userId").getValue().toString());
                        hm.put("userLandmark", dataSnapshot.child("userLandmark").getValue().toString());
                        hm.put("userName", dataSnapshot.child("userName").getValue().toString());
                        hm.put("userPhone", dataSnapshot.child("userPhone").getValue().toString());
                        hm.put("userPostalCode", dataSnapshot.child("userPostalCode").getValue().toString());
                        hm.put("userState", dataSnapshot.child("userState").getValue().toString());
                        hm.put("userLat", dataSnapshot.child("userLat").getValue().toString());
                        hm.put("userLng", dataSnapshot.child("userLng").getValue().toString());
                        hm.put("region", region);
                        HashMap hm2 = new HashMap();
                        hm2.put("deliveryBoyName", Constant.adminName);
                        hm2.put("deliveryBoyNumber", Constant.adminPhone);
                        hm2.put("delivered", "On Way");
                        sendSms(dataSnapshot.child("userPhone").getValue().toString(),"Your order worth rupees "+dataSnapshot.child("TotalAmount").getValue().toString()+" is on the way and delivered by Mr. "+Constant.adminName+". You can make call to him at "+Constant.adminPhone);
                        Constant.dispatchOrder.child(dataSnapshot.getKey()).updateChildren(hm2);
                        Constant.orderref.child(dataSnapshot.child("order_id").getValue().toString()).updateChildren(hm2);
                        FirebaseDatabase.getInstance().getReference().child("UserInfo").child("Jabalpur").child(dataSnapshot.child("userId").getValue().toString()).child("history")
                                .child(dataSnapshot.child("orderIdUser").getValue().toString()).child("delivered").setValue("On Way");
                        Constant.deliveryOrderRef.child(orderId).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                            @Override
                            public void onComplete(@NonNull Task task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(DeliveryActivity.this, "added", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(DeliveryActivity.this, task.getException().toString(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }else {
                        Toast.makeText(DeliveryActivity.this, "Order has taken by another delivery boy.", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(DeliveryActivity.this, "Order is not ready for dispatch.", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    private void loadHistory(String filter) {
        if (filter.equals("null")){
            options = new FirebaseRecyclerOptions.Builder<HistoryListModel>()
                    .setQuery(Constant.deliveryOrderRef,HistoryListModel.class)
                    .build();
        }

        FirebaseRecyclerAdapter firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<HistoryListModel, HistoryViewHolder>(options) {
            @NonNull
            @Override
            public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.delivery_item_single_layout, viewGroup, false);

                return new HistoryViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull final HistoryViewHolder holder, @SuppressLint("RecyclerView") final int position, @NonNull final HistoryListModel model) {
                holder.setOrderId(model.getOrder_id().substring(model.getOrder_id().length()-8));
                holder.setDate(model.getDate());
                holder.setPaymentMethod(model.getPaymentMethod());
                holder.setAmount(model.getTotalAmount());
                holder.setName(model.getUserName());
                holder.setPhone(model.getUserPhone());
                holder.setAddress(model.getUserAddress(),model.getUserLat(),model.getUserLng());
                holder.setRegion(model.getRegion());

                holder.mView.findViewById(R.id.phone).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:"+model.getUserPhone()));
                        startActivity(intent);
                    }
                });

                holder.mView.findViewById(R.id.denyBtn).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(DeliveryActivity.this);
                        builder.setTitle("Cancelled");
                        builder.setMessage("Do you want to cancelled this item?");
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                sendSms(model.getUserPhone(),"Your order worth rupees "+model.getTotalAmount()+" has been cancelled.");

                                FirebaseDatabase.getInstance().getReference().child("Orders").child("Jabalpur").child("Orders").
                                        child(model.getOrder_id()).child("delivered").setValue("Cancelled");
                                FirebaseDatabase.getInstance().getReference().child("UserInfo").child("Jabalpur").child(model.getUserId()).child("history")
                                        .child(model.getOrderIdUser()).child("delivered").setValue("Cancelled");
                                Constant.dispatchOrder.child(getRef(position).getKey()).setValue(null);
                                Constant.deliveryOrderRef.child(getRef(position).getKey()).setValue(null);
                                sendSms(model.getUserPhone(),"Your order worth rupees "+model.getTotalAmount()+" has been cancelled.");

                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                        builder.show();

                    }
                });

                holder.mView.findViewById(R.id.deliveryBtn).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
                        final String date = formatter.format(Calendar.getInstance().getTime());
                        if(model.getRegion().toLowerCase().equals("local")){

                            AlertDialog.Builder builder = new AlertDialog.Builder(DeliveryActivity.this);
                            builder.setTitle("Deliver");
                            builder.setMessage("Do you want to deliver this item?");
                            builder.setPositiveButton("deliver", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    sendSms(model.getUserPhone(),"Your order worth rupees "+model.getTotalAmount()+" has been delivered. Thanks for shopping with us.");
                                    Date today = new Date();
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                                    String dateToStr = format.format(today);
                                    HashMap hm = new HashMap();
                                    hm.put("delivered","Delivered");
                                    hm.put("deliveredDateTime",dateToStr);
                                    hm.put("deliveredDate",date);
                                    FirebaseDatabase.getInstance().getReference().child("Orders").child("Jabalpur").child("Orders").
                                            child(model.getOrder_id()).updateChildren(hm);
                                    FirebaseDatabase.getInstance().getReference().child("UserInfo").child("Jabalpur").child(model.getUserId()).child("history")
                                            .child(model.getOrderIdUser()).child("delivered").setValue("Delivered");
                                    Constant.dispatchOrder.child(getRef(position).getKey()).setValue(null);
                                    Constant.deliveryOrderRef.child(getRef(position).getKey()).setValue(null);
                                }
                            }).setNegativeButton("dismiss", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            });
                            builder.show();
                        }else {
                            LayoutInflater li= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                            final View v=li.inflate(R.layout.docket_no_edit_box,null,false);
                            android.support.v7.app.AlertDialog.Builder abd=new android.support.v7.app.AlertDialog.Builder(DeliveryActivity.this);
                            abd.setView(v);
                            abd.setTitle("Courier Details");
                            abd.setCancelable(false);
                            abd.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    TextView docketNo = v.findViewById(R.id.docketNo);
                                    final String docketNostr = docketNo.getText().toString().trim();

                                    TextView coriurName = v.findViewById(R.id.courierName);
                                    final String coriurNamestr = coriurName.getText().toString().toLowerCase().trim();

                                    if (docketNostr.isEmpty() && coriurNamestr.isEmpty()) {
                                        Toast.makeText(DeliveryActivity.this, "write something", Toast.LENGTH_SHORT).show();
                                    } else {

                                        RequestQueue queue = Volley.newRequestQueue(DeliveryActivity.this);

                                        StringRequest request = new StringRequest(Request.Method.POST, Constant.BASE_URL+"addCourierData/", new com.android.volley.Response.Listener<String>() {

                                            @Override
                                            public void onResponse(String response)
                                            {
                                                progressDialog.dismiss();
                                                sendSms(model.getUserPhone(),"Your order worth rupees "+model.getTotalAmount()+" has been delivered. Thanks for shopping with us.");
                                                HashMap hmp = new HashMap();
                                                hmp.put("deliveredDate",date);
                                                hmp.put("delivered","Delivered");
                                                FirebaseDatabase.getInstance().getReference().child("Orders").child("Jabalpur").child("Orders").
                                                        child(model.getOrder_id()).updateChildren(hmp);
                                                FirebaseDatabase.getInstance().getReference().child("UserInfo").child("Jabalpur").child(model.getUserId()).child("history")
                                                        .child(model.getOrderIdUser()).child("delivered").setValue("Delivered");
                                                Constant.dispatchOrder.child(getRef(position).getKey()).setValue(null);
                                                Constant.deliveryOrderRef.child(getRef(position).getKey()).setValue(null);
                                                Toast.makeText(DeliveryActivity.this, "Done", Toast.LENGTH_SHORT).show();
                                            }
                                        },
                                                new com.android.volley.Response.ErrorListener()
                                                {
                                                    @Override
                                                    public void onErrorResponse(VolleyError error) {
                                                        progressDialog.dismiss();

                                                        Log.d("RESPONSE:          >>> ", error.toString() + "<<<");
                                                        Toast.makeText(DeliveryActivity.this,")pps !! "+error.toString(), Toast.LENGTH_SHORT).show();

                                                    }
                                                })
                                        {
                                            @Override
                                            protected Map<String, String> getParams()
                                            {
                                                Map<String, String> jsonParams = new ArrayMap<>();
                                                jsonParams.put("docketNo", docketNostr);
                                                jsonParams.put("orderId", model.getOrder_id());
                                                jsonParams.put("courierName", coriurNamestr);
                                                jsonParams.put("date", date);
                                                return jsonParams;
                                            }
                                            @Override
                                            public String getBodyContentType(){
                                                return "application/x-www-form-urlencoded";
                                            }
                                        };

                                        queue.add(request);



                                    }
                                }
                            });

                            abd.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });

                            final android.support.v7.app.AlertDialog ad=abd.create();
                            ad.show();
                        }

                    }
                });
            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                progressDialog.dismiss();
            }
            @Override
            public int getItemCount() {
                return super.getItemCount();
            }
        };
        firebaseRecyclerAdapter.startListening();
        historyList.setAdapter(firebaseRecyclerAdapter);
    }

    private void sendSms(String phone,String smstext) {
        String url = "https://api.msg91.com/api/sendhttp.php?country=91&sender=AOBJBP&route=4&mobiles="+phone+"&authkey=106940A18cEbcPcZ75c503f65&message="+smstext+
                "%0a%0aRegards,%0aAkash Online Books";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), String.valueOf(error), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(request);

    }

    private class HistoryViewHolder extends RecyclerView.ViewHolder {

        View mView;
        public HistoryViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setOrderId(String item) {
            TextView subtxt = mView.findViewById(R.id.order_id);
            subtxt.setText(item.toUpperCase());
        }
        public void setRegion(String item) {
            TextView subtxt = mView.findViewById(R.id.region);
            subtxt.setText(item);
        }

        public void setDate(String item) {
            TextView subtxt = mView.findViewById(R.id.date);
            subtxt.setText(item);
        }
        public void setPaymentMethod(String item) {
            TextView subtxt = mView.findViewById(R.id.payment_method);
            subtxt.setText(item);
        }

        public void setAmount(String item) {
            TextView subtxt = mView.findViewById(R.id.amount);
            subtxt.setText("₹"+item);
        }
        public void setName(String item) {
            TextView subtxt = mView.findViewById(R.id.name);
            subtxt.setText(item);
        }
        public void setPhone(String item) {
            TextView subtxt = mView.findViewById(R.id.phone);
            subtxt.setText(item);
        }
        public void setAddress(String item,final String lat ,final String lng) {
            TextView subtxt = mView.findViewById(R.id.address);
            subtxt.setText(item);
            subtxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("-------->",lat+"/"+lng);
                    if (lat.length()>5){
                        Uri gmmIntentUri = Uri.parse("geo:"+lat+","+lng);
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        try{
                            startActivity(mapIntent);
                        }catch (Exception e){
                            Toast.makeText(DeliveryActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(DeliveryActivity.this, "Geo location not available", Toast.LENGTH_SHORT).show();
                    }

                }
            });
        }

    }

}
