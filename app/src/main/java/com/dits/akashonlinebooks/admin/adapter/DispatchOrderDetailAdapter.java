package com.dits.akashonlinebooks.admin.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.activity.DispatchActivity.DispatchDetailActivity;
import com.dits.akashonlinebooks.admin.model.HistoryDetailModel;
import com.mikhaellopez.hfrecyclerview.HFRecyclerView;

import java.util.ArrayList;

public class DispatchOrderDetailAdapter extends HFRecyclerView<HistoryDetailModel> {
    private Context context;
    private ArrayList<HistoryDetailModel> bookDataList;
    HistoryDetailModel temp;
    CancelOrder cancelOrder;

    public DispatchOrderDetailAdapter(Context context, ArrayList values, CancelOrder cancelOrder){
        super(true, true);
        this.context = context;
        this.bookDataList = values;
        this.cancelOrder = cancelOrder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof ViewHolder) {
            ViewHolder holder = (ViewHolder) viewHolder;
            temp = bookDataList.get(i-1);
            holder.setItemName(temp.getBookName());
            holder.setItemImage(temp.getImage(),context);
            holder.setItemCost(temp.getMRP());
            holder.setItemActualCost(temp.getActualCost());
            holder.setPublisherName(temp.getPublisher());
            holder.setAuthorName(temp.getAuthor());
            holder.setPersentageText(Float.parseFloat(temp.getMRP()),Float.parseFloat(temp.getActualCost()));
            holder.setKey(temp.getKey());
            holder.setQuantity(temp.getQuantity());
            holder.ratingBar(temp.getRated(),temp.getHistoryKey());

        } else if (viewHolder instanceof HeaderViewHolder) {
            HeaderViewHolder holder = (HeaderViewHolder) viewHolder;
            holder.setNamePostalCode(DispatchDetailActivity.userName +", "+DispatchDetailActivity.userPostalCode);
            holder.setAddress(DispatchDetailActivity.userAddress);
            holder.setDeliveryStatus(DispatchDetailActivity.delivered);

        } else if (viewHolder instanceof FooterViewHolder) {

            FooterViewHolder holder = (FooterViewHolder) viewHolder;
            holder.setItemCount(String.valueOf(DispatchDetailActivity.count));
            holder.setTotalPrice(String.valueOf(Integer.valueOf(DispatchDetailActivity.Cost)+Integer.valueOf(DispatchDetailActivity.referralAmount)-Integer.valueOf(DispatchDetailActivity.deliveryCharge)));
            holder.setTotalPayable(String.valueOf(DispatchDetailActivity.Cost));
            holder.setDeliveryCharge("₹"+DispatchDetailActivity.deliveryCharge);
            holder.setSaveTxt(String.valueOf(Integer.valueOf(DispatchDetailActivity.ActualCost)-Integer.parseInt(DispatchDetailActivity.Cost) + Integer.valueOf(DispatchDetailActivity.deliveryCharge)));
            holder.setReferralAmount(Integer.valueOf(DispatchDetailActivity.referralAmount));
            holder.cancelBtn(DispatchDetailActivity.delivered,DispatchDetailActivity.orderKey);

        }


    }

    @Override
    protected RecyclerView.ViewHolder getItemView(LayoutInflater inflater, ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.history_detail_view_item_layout, parent, false));
    }

    @Override
    protected RecyclerView.ViewHolder getHeaderView(LayoutInflater inflater, ViewGroup parent) {
        return new HeaderViewHolder(inflater.inflate(R.layout.history_detail_view_header, parent, false));
    }

    @Override
    protected RecyclerView.ViewHolder getFooterView(LayoutInflater inflater, ViewGroup parent) {
        return new FooterViewHolder(inflater.inflate(R.layout.history_detail_view_footer, parent, false));
    }


    public interface CancelOrder{
        void onClacelOrderClick(String key);
    }

    public interface Rating{
        void onRateingListener(String key, float rate, float oldRating, String historyKey);
    }

    public class ViewHolder extends RecyclerView.ViewHolder  {
        String key;
        View mView;
        ImageView itemImage;
        int x = 0;

        public ViewHolder(View itemView) {
            super(itemView);
            mView=itemView;
        }

        public void setKey(String key){
            this.key = key;
        }

        public void setItemName(String item) {
            TextView book_name = mView.findViewById(R.id.book_name);
            book_name.setText(item);

        }

        public void setAuthorName(String item) {
            TextView item_name = mView.findViewById(R.id.author_name);
            item_name.setText(item);

        }

        public void setPublisherName(String item) {
            TextView item_name = mView.findViewById(R.id.publisher_name);
            item_name.setText(item);

        }

        public void setItemCost(String item) {
            TextView book_name = mView.findViewById(R.id.cost_text);
            book_name.setText("₹"+item);
        }

        public void setItemActualCost(String item) {
            TextView book_name = mView.findViewById(R.id.actual_cost_text);
            book_name.setText("₹"+item);
        }

        public void setQuantity(String item) {
            TextView book_name = mView.findViewById(R.id.product_quantity);
            book_name.setText("Qty : "+item);
        }

        public void setItemImage(String item, Context context) {
            itemImage = mView.findViewById(R.id.book_image);
            Glide.with(context).load(item).apply(new RequestOptions().placeholder(R.drawable.no_image).error(R.drawable.no_image)).into(itemImage);
        }

        public void setPersentageText(float numerator,float denominator) {
            TextView itemImage = mView.findViewById(R.id.discount);
            int percentage = (int)(numerator * 100.0 / denominator + 0.5);
            itemImage.setText(100-percentage + "% off");
        }


        public void ratingBar(String rated, final String historyKey){

        }

    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        View mView;
        HeaderViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }
        public void setNamePostalCode(String item) {
            TextView item_name = mView.findViewById(R.id.name_postal_code);
            item_name.setText(item);
        }

        public void setAddress(String item) {
            TextView item_name = mView.findViewById(R.id.address);
            item_name.setText(item);
            item_name.setMaxLines(3);
        }

        public void setDeliveryStatus(String totalCost) {
            TextView book_name = mView.findViewById(R.id.delivery_status);
            book_name.setText(totalCost);
            if (totalCost.equals("Order Placed"))
            {
                book_name.setTextColor(context.getResources().getColor(R.color.yello));
                book_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_yello_dot, 0, 0, 0);
            }
            else if (totalCost.equals("Cancelled"))
            {
                book_name.setTextColor(context.getResources().getColor(R.color.red));
                book_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_red_dot, 0, 0, 0);
            }
            else {
                book_name.setTextColor(context.getResources().getColor(R.color.green));
                book_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_green_dot, 0, 0, 0);
            }

        }

    }

    class FooterViewHolder extends RecyclerView.ViewHolder {
        View mView;
        FooterViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setItemCount(String item) {
            TextView item_name = mView.findViewById(R.id.total_item);
            item_name.setText("Price ("+item+" item)");
        }

        public void setTotalPrice(String item) {
            TextView item_name = mView.findViewById(R.id.total_price);
            item_name.setText("₹"+item);
        }

        public void setTotalPayable(String item) {
            TextView item_name = mView.findViewById(R.id.total_amount_payable);
            item_name.setText("₹"+item);

        }

        public void setDeliveryCharge(String item) {
            TextView item_name = mView.findViewById(R.id.delivery_charge);
            item_name.setText(item);
        }

        public void setSaveTxt(String item) {
            TextView item_name = mView.findViewById(R.id.save_txt);
            item_name.setText("You saved total ₹"+item+" on this order");
        }
        public void setReferralAmount(int item) {
            LinearLayout ll = mView.findViewById(R.id.referral_lay);
            TextView item_name = mView.findViewById(R.id.referral_amount);
            if (item!=0){
                ll.setVisibility(View.VISIBLE);
                item_name.setText("- ₹"+item);
                item_name.setTextColor(context.getResources().getColor(R.color.red));
            }else {
                ll.setVisibility(View.GONE);
                item_name.setText("₹"+0);
            }

        }

        public void cancelBtn(String status, final String key){
            Button cancelBtn = mView.findViewById(R.id.cancel_btn);

//            cancelBtn.setVisibility(View.GONE);


            cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (cancelOrder != null) {
                        cancelOrder.onClacelOrderClick(key);
                    }
                }
            });
        }

    }


}
