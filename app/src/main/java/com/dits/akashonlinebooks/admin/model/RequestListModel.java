package com.dits.akashonlinebooks.admin.model;

public class RequestListModel {
    String bookName,publisherName,description,name,date,phone,uid,isCompleted,image,token;

    public RequestListModel() {
    }

    public RequestListModel(String bookName, String publisherName, String description, String name, String date, String phone, String uid, String isCompleted, String image, String token) {
        this.bookName = bookName;
        this.publisherName = publisherName;
        this.description = description;
        this.name = name;
        this.date = date;
        this.phone = phone;
        this.uid = uid;
        this.isCompleted = isCompleted;
        this.image = image;
        this.token = token;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getPublisherName() {
        return publisherName;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getIsCompleted() {
        return isCompleted;
    }

    public void setIsCompleted(String isCompleted) {
        this.isCompleted = isCompleted;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
