package com.dits.akashonlinebooks.admin.model;

public class CategoryModel {
    String category,key;

    public CategoryModel() {
    }

    public CategoryModel(String category, String key) {
        this.category = category;
        this.key = key;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
