package com.dits.akashonlinebooks.admin.activity.PackingActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.activity.HistoryDetailsActivity;
import com.dits.akashonlinebooks.admin.activity.PickingActivity.PickingOrderDetailActivity;
import com.dits.akashonlinebooks.admin.adapter.HistoryDetailAdapter;
import com.dits.akashonlinebooks.admin.adapter.PackingOrderDetailAdapter;
import com.dits.akashonlinebooks.admin.adapter.PickingOrderDetailAdapter;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.dits.akashonlinebooks.admin.model.HistoryDetailModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class PackingDetailActivity extends AppCompatActivity implements PackingOrderDetailAdapter.CancelOrder{
    ArrayList<HistoryDetailModel> bookDetailList;
    public static String Cost,ActualCost,date,delivered,orderKey,orderref,paymentMethod,userAddress,userId,userName,userPhone,userPostalCode,orderIdUser,deliveryBoyOrderKey,deliveryCharge,referralAmount;
    RecyclerView history_detail;
    LinearLayoutManager linearLayoutManager;
    public static int count = 0;
    ProgressDialog progressDialog;
    WebView disPatchVebView,invoiceWebView;
    String base64String = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_packing_detail);

        Cost = getIntent().getExtras().getString("Cost");
        ActualCost = getIntent().getExtras().getString("ActualCost");
        date = getIntent().getExtras().getString("date");
        delivered = getIntent().getExtras().getString("delivered");
        orderKey = getIntent().getExtras().getString("orderKey");
        orderref = getIntent().getExtras().getString("orderref");
        paymentMethod = getIntent().getExtras().getString("paymentMethod");
        userAddress = getIntent().getExtras().getString("userAddress");
        userId = getIntent().getExtras().getString("userId");
        userName = getIntent().getExtras().getString("userName");
        userPhone = getIntent().getExtras().getString("userPhone");
        userPostalCode = getIntent().getExtras().getString("userPostalCode");
        orderIdUser = getIntent().getExtras().getString("orderIdUser");
        deliveryBoyOrderKey = getIntent().getExtras().getString("deliveryBoyOrderKey");
        deliveryCharge = getIntent().getExtras().getString("deliveryCharge");
        referralAmount = getIntent().getExtras().getString("referralAmount");
        history_detail = findViewById(R.id.history_detail);
        history_detail.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        linearLayoutManager = new LinearLayoutManager(this);
        history_detail.setLayoutManager(linearLayoutManager);
        history_detail.setHasFixedSize(true);
        bookDetailList = new ArrayList<>();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        invoiceWebView = findViewById(R.id.myWebView2);
        invoiceWebView.loadUrl(Constant.BASE_URL+"getInvoice/"+orderKey);
        invoiceWebView.getSettings().setJavaScriptEnabled(true);
        invoiceWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageCommitVisible(WebView view, String url) {
               // progressDialog.dismiss();
            }
        });

        disPatchVebView = findViewById(R.id.myWebView);
        disPatchVebView.getSettings().setDomStorageEnabled(true);
        disPatchVebView.getSettings().setAppCacheEnabled(true);
        disPatchVebView.getSettings().setLoadsImagesAutomatically(true);
        disPatchVebView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);

        disPatchVebView.loadUrl(Constant.BASE_URL+"getDispatchSlipSql/"+orderKey);
        disPatchVebView.getSettings().setJavaScriptEnabled(true);
        disPatchVebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageCommitVisible(WebView view, String url) {
                progressDialog.dismiss();
            }
        });

        loadHistoryDetails();

    }

    void loadHistoryDetails(){
        Constant.packingOrderRef.child(orderref).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                bookDetailList.clear();
                for(DataSnapshot ds : dataSnapshot.getChildren()){
                    if (ds.hasChild("product_id")){
                        HistoryDetailModel historyDetailModel = new HistoryDetailModel();
                        historyDetailModel.setActualAmount(ds.child("ActualAmount").getValue().toString());
                        historyDetailModel.setActualCost(ds.child("Amount").getValue().toString());
                        historyDetailModel.setAuthor(ds.child("Author").getValue().toString());
                        historyDetailModel.setBookName(ds.child("BookName").getValue().toString());
                        historyDetailModel.setImage(ds.child("Image").getValue().toString());
                        historyDetailModel.setMRP(ds.child("MRP").getValue().toString());
                        historyDetailModel.setPublisher(ds.child("Publisher").getValue().toString());
                        historyDetailModel.setKey(ds.child("key").getValue().toString());
                        historyDetailModel.setActualCost(ds.child("actualCost").getValue().toString());
                        historyDetailModel.setQuantity(ds.child("quantity").getValue().toString());
                        historyDetailModel.setActualCost(ds.child("actualCost").getValue().toString());
                        historyDetailModel.setRated(ds.child("Rated").getValue().toString());
                        historyDetailModel.setHistoryKey(ds.getKey());
                        bookDetailList.add(historyDetailModel);
                        count++;
                    }
                }

                PackingOrderDetailAdapter historyDetailAdapter = new PackingOrderDetailAdapter(getApplicationContext(),bookDetailList, PackingDetailActivity.this);
                history_detail.setAdapter(historyDetailAdapter);
                historyDetailAdapter.setData(bookDetailList);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        count = 0;
    }

    @Override
    protected void onStart() {
        super.onStart();
        count = 0;
    }

    @Override
    public void onClacelOrderClick(final String key,String type) {

        Log.d("------->",type);
        if (type.equals("invoice")){
            createWebPrintJob(invoiceWebView);
        }else {
            disPatchVebView.setWebChromeClient(new WebChromeClient() {
                @Override
                public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                    Log.d("", "-------------********------: ");
                    String s = consoleMessage.message();
                    return true;
                }
            });
            disPatchVebView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageCommitVisible(WebView view, String url) {
                    progressDialog.dismiss();
                }
            });
            createWebPrintJob(disPatchVebView);
        }


    }

//    private void createWebPrintJob(WebView webView) {
//
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//            PrintManager printManager = (PrintManager) this
//                    .getSystemService(Context.PRINT_SERVICE);
//
//            PrintDocumentAdapter printAdapter =
//                    null;
//            printAdapter = webView.createPrintDocumentAdapter("MyDocument");
//            String jobName = getString(R.string.app_name) + " Print Test";
//
//            printManager.print(jobName, printAdapter,
//                    new PrintAttributes.Builder().build());
//        }
//        else{
//            Toast.makeText(PackingDetailActivity.this, "Print job has been canceled! ", Toast.LENGTH_SHORT).show();
//        }
//
//    }


    private void createWebPrintJob(WebView webView) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            PrintManager printManager = (PrintManager) this
                    .getSystemService(Context.PRINT_SERVICE);

            PrintDocumentAdapter printAdapter =
                    null;
            printAdapter = webView.createPrintDocumentAdapter("MyDocument");
            String jobName = getString(R.string.app_name) + " Print Test";

            printManager.print(jobName, printAdapter,
                    new PrintAttributes.Builder().build());
        } else {
            Toast.makeText(PackingDetailActivity.this, "Print job has been canceled! ", Toast.LENGTH_SHORT).show();
        }

    }

    @SuppressLint("JavascriptInterface")
    private void loadinvoice()
    {
        progressDialog.show();
        WebView invoiceWebView;
        invoiceWebView = findViewById(R.id.myWebView22);
        invoiceWebView.getSettings().setJavaScriptEnabled(true);
        String newUA= invoiceWebView.getSettings().getUserAgentString().replace("Mobile", "eliboM").replace("Android", "diordnA");
        invoiceWebView.getSettings().setUserAgentString(newUA);
        invoiceWebView.getSettings().setUseWideViewPort(true);
        invoiceWebView.getSettings().setLoadWithOverviewMode(true);
        invoiceWebView.getSettings().setSupportZoom(true);
        invoiceWebView.getSettings().setBuiltInZoomControls(true);
        invoiceWebView.loadUrl(Constant.BASE_URL + "getInvoice/" + orderKey);
        invoiceWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                if(consoleMessage.message().length() > 10000)
                {
                    base64String = consoleMessage.message();
                    android.util.Log.d("WebView", "------------->"+base64String.length());
                    progressDialog.dismiss();
                }
                progressDialog.dismiss();
                return true;
            }
        });

    }



}
