package com.dits.akashonlinebooks.admin.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Parcelable;
import android.provider.BaseColumns;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.adapter.BooksDetailsAdapter;
import com.dits.akashonlinebooks.admin.extra.ApiClient;
import com.dits.akashonlinebooks.admin.extra.ApiInterface;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.dits.akashonlinebooks.admin.fragments.BookListFragment;
import com.dits.akashonlinebooks.admin.model.BookDataModel;
import com.dits.akashonlinebooks.admin.model.OfferGetIdModel;
import com.dits.akashonlinebooks.admin.model.RequestSearchListModel;
import com.dits.akashonlinebooks.admin.model.SearchResultModel;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestSearchActivity extends AppCompatActivity {
    SearchView searchView;
    ListView bookNameList;
    ProgressDialog progressDialog;
    FloatingActionButton addBtn;
    ArrayAdapter arrayAdapter;
    private ApiInterface movieService;
    private SimpleCursorAdapter mAdapter;
    private ArrayList<SearchResultModel> SUGGESTIONS;
    ArrayList<RequestSearchListModel> arrayList;
    ArrayList bookNameArray;
    DatabaseReference databaseReference;
    BookDetailsActivity.ClearFilterListenerHome clearFilterListenerHome;
    String uid,token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_search);

        movieService = ApiClient.getClient().create(ApiInterface.class);
        SUGGESTIONS = new ArrayList();
        arrayList = new ArrayList();
        bookNameArray = new ArrayList();
        searchView = findViewById(R.id.search_bar);
        addBtn = findViewById(R.id.addBtn);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setAllBookOnWistlist();
            }
        });
        uid = getIntent().getExtras().getString("uid");
        token = getIntent().getExtras().getString("token");

        databaseReference = Constant.rootRef.child("UserInfo").child("Jabalpur").child(getIntent().getExtras().getString("uid")).child("wishlist");

        bookNameList = findViewById(R.id.bookNameList);
        arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,bookNameArray);
        bookNameList.setAdapter(arrayAdapter);
        bookNameList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                arrayList.remove(i);
                bookNameArray.remove(i);
                arrayAdapter.notifyDataSetChanged();
            }
        });

        final String[] from = new String[] {"filter"};
        final int[] to = new int[] {android.R.id.text1};
        mAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_1,
                null,
                from,
                to,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);


        searchView.setSuggestionsAdapter(mAdapter);

        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int i) {

                return false;
            }

            @Override
            public boolean onSuggestionClick(int i) {
                Cursor cursor = (Cursor) mAdapter.getItem(i);
                String txt = cursor.getString(cursor.getColumnIndex("filter"));
                searchView.setQuery(txt, true);
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                getFinalSearchResult(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                try{
                    getSearchResultFromApi(s);

                }catch (Exception e){

                }

                return false;
            }
        });

        int searchCloseButtonId = searchView.getContext().getResources()
                .getIdentifier("android:id/search_close_btn", null, null);
        ImageView closeButton =  this.searchView.findViewById(searchCloseButtonId);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setQuery(null,false);
                clearFilterListenerHome = BookListFragment.ctx;
                if (clearFilterListenerHome!=null){
                    clearFilterListenerHome.ClearFilterListenerHome();
                }
            }
        });
    }

    private void setAllBookOnWistlist() {
        progressDialog.show();
        for (int i = 0;i<arrayList.size();i++){
            databaseReference.child(databaseReference.push().getKey()).child("key").setValue(arrayList.get(i).getBookId());
        }
        progressDialog.dismiss();
        Toast.makeText(this, "Done", Toast.LENGTH_LONG).show();
        Constant.sendNotification(this,token,"Book Request Completed","your book request has completed. We have added your book into your wishlist.");
        finish();
    }

    private void getFinalSearchResult(String s) {

        getSearchDetailsResult(s).enqueue(new Callback<List<BookDataModel>>() {
            @Override
            public void onResponse(Call<List<BookDataModel>> call, Response<List<BookDataModel>> response) {
                bookNameArray.add(response.body().get(0).getBookName());
                arrayList.add(new RequestSearchListModel(response.body().get(0).getBookKey(),response.body().get(0).getBookName()));
                arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<BookDataModel>> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(RequestSearchActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getSearchResultFromApi(String key){
        SUGGESTIONS.clear();
        getSearchResult(key).enqueue(new Callback<List<SearchResultModel>>() {
            @Override
            public void onResponse(Call<List<SearchResultModel>> call, Response<List<SearchResultModel>> response) {
                for(SearchResultModel s : response.body()){
                    SUGGESTIONS.add(s);
                }
                populateAdapter();
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<SearchResultModel>> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(RequestSearchActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }



    private void populateAdapter() {
        final MatrixCursor c = new MatrixCursor(new String[]{ BaseColumns._ID, "filter" });
        for (int i=0; i<SUGGESTIONS.size(); i++) {
            c.addRow(new Object[] {i, SUGGESTIONS.get(i).getBookName()});
        }
        mAdapter.changeCursor(c);
    }

    private Call<List<SearchResultModel>> getSearchResult(String searchKey) {
        return movieService.getSearchResult(
                searchKey
        );
    }

    private Call<List<BookDataModel>> getSearchDetailsResult(String searchKey) {
        return movieService.getSearchDetailsResult(
                searchKey
        );
    }

}
