package com.dits.akashonlinebooks.admin.fragments;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.activity.OrderDetails;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.dits.akashonlinebooks.admin.model.DeliveryBoyModel;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;


public class DeliveryBoyList extends Fragment {
    ProgressDialog progressDialog;
    RecyclerView itemList;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_delivery_boy_list, container, false);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        itemList = v.findViewById(R.id.itemList);
        itemList.setHasFixedSize(true);
        itemList.setLayoutManager(new LinearLayoutManager(getContext()));

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        refreshBookRecyclerView();
    }


    private void refreshBookRecyclerView() {
        FirebaseRecyclerOptions<DeliveryBoyModel> options =
                new FirebaseRecyclerOptions.Builder<DeliveryBoyModel>()
                        .setQuery(Constant.deliveryBoyRegistrationRef.orderByChild("deliveryBoyModel"), DeliveryBoyModel.class)
                        .build();

        FirebaseRecyclerAdapter adapter = new FirebaseRecyclerAdapter<DeliveryBoyModel, ItemViewHolder>(options) {
            @Override
            public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.delivery_boy_list_single_layout, parent, false);

                return new ItemViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(ItemViewHolder holder, final int position, final DeliveryBoyModel model) {
                if (model.getSetAvailable().equals("true")){
                    holder.setSchoolName(model.getDeliveryBoyModel());
                    holder.setPhone(model.getAdminType());
                    holder.setAlphabetImage(model.getDeliveryBoyModel().substring(0, 1));
                    final String id =getRef(position).getKey();
                    holder.mView.findViewById(R.id.menu_btn).setVisibility(View.GONE);

                    holder.mView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showDialog(id,model.getDeliveryBoyModel(),model.getPhone());

                        }
                    });
                }else {
                    holder.mView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

            }
        };
        adapter.startListening();
        itemList.setAdapter(adapter);
    }

    private void showDialog(final String id,final String name,final String number) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Send");
        builder.setMessage("Do you want to send?");
        builder.setPositiveButton("Send", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                final String key = Constant.deliveryBoyRegistrationRef.child(id).push().getKey();
                Constant.orderref.child(OrderDetails.orderKey).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                Constant.deliveryBoyRegistrationRef.child(id).child("Orders").child(key).setValue(dataSnapshot.getValue()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()){
                                            HashMap hm = new HashMap();
                                            hm.put("deliveryBoyName",name);
                                            hm.put("deliveryBoyNumber",number);

                                            Constant.orderref.child(OrderDetails.orderKey).updateChildren(hm).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()){
                                                        newDeliveryMsg(number);
                                                        Toast.makeText(getContext(), "Sent", Toast.LENGTH_SHORT).show();
                                                    }else {
                                                        Toast.makeText(getContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

            }
        });
        builder.show();
    }

    void newDeliveryMsg(String number){
        String url = "https://api.msg91.com/api/sendhttp.php?country=91&sender=AOBJBP&route=4&mobiles="+number+"&authkey=106940A18cEbcPcZ75c503f65&message=New Order Alert!!";
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), String.valueOf(error), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(request);
    }


    public static class ItemViewHolder extends RecyclerView.ViewHolder{
        View mView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mView=itemView;
        }
        public void setSchoolName(String name)
        {
            TextView userNameView= mView.findViewById(R.id.school_name);
            userNameView.setText(name);

        }
        public void setPhone(String phone)
        {
            TextView userNumberView= mView.findViewById(R.id.phone_number);
            userNumberView.setText(phone);
        }

        public void setAlphabetImage(String s){
            TextDrawable drawable = TextDrawable.builder()
                    .buildRound(s, randomColor());
            ImageView image = mView.findViewById(R.id.image_view);
            image.setImageDrawable(drawable);
        }
        public int randomColor() {

            int r = (int) (0xff * Math.random());
            int g = (int) (0xff * Math.random());
            int b = (int) (0xff * Math.random());

            return Color.rgb( r, g, b);
        }

    }

}
