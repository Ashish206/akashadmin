package com.dits.akashonlinebooks.admin.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.model.BookDataModel;

import java.util.ArrayList;

public class BooksDetailsAdapter extends  RecyclerView.Adapter<BooksDetailsAdapter.ViewHolder> implements Filterable {
    protected ItemListener mListener;
    protected ItemLongClickListener itemLongClickListener;
    private Context context;
    private ArrayList<BookDataModel> bookDataList;
    BookDataModel temp;
    private ArrayList<BookDataModel> filteredProductList;

    public BooksDetailsAdapter(Context context, ArrayList values,ItemListener itemListener,ItemLongClickListener itemLongClickListener){
        this.context = context;
        this.bookDataList = values;
        this.mListener = itemListener;
        this.itemLongClickListener = itemLongClickListener;
        this.filteredProductList = values;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_book_details_list_layout, viewGroup, false);
        
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        temp = filteredProductList.get(i);
        holder.setItemName(temp.getBookName());
        holder.setItemImage(temp.getImage(),context);
        holder.setItemCost(temp.getMRP());
        holder.setItemActualCost(temp.getActualCost());
        holder.setPublisherName(temp.getPublisher());
        holder.setAuthorName(temp.getAuthor());
        holder.setPersentageText(Float.parseFloat(temp.getMRP()),Float.parseFloat(temp.getActualCost()));
        holder.setKey(temp.getBookKey());
        holder.setQuantity(String.valueOf(temp.getQuantity()));
    }

    @Override
    public int getItemCount() {
        return filteredProductList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredProductList = bookDataList;
                } else {
                    ArrayList filteredList = new ArrayList<>();

                    for (BookDataModel androidVersion : bookDataList) {
                        String row = androidVersion.getBookName()
                                +" "+androidVersion.getAuthor()
                                +" "+androidVersion.getBookKey()
                                +" "+androidVersion.getIsbn()
                                +" "+androidVersion.getCategory()
                                +" "+androidVersion.getPublisher();
                        if (row.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(androidVersion);
                        }
                    }

                    filteredProductList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredProductList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredProductList = (ArrayList) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface ItemListener {
        void onItemClick(String key);
    }

    public interface ItemLongClickListener {
        void onItemLongClickClick(String key);
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnLongClickListener {
        String key;
        View mView;
        ImageView itemImage;

        public ViewHolder(View itemView) {
            super(itemView);
            mView=itemView;
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        public void setKey(String key){
            this.key = key;
        }

        public void setItemName(String item) {
            TextView book_name = mView.findViewById(R.id.book_name);
            book_name.setText(item);

        }

        public void setAuthorName(String item) {
            TextView item_name = mView.findViewById(R.id.author_name);
            item_name.setText(item);

        }

        public void setPublisherName(String item) {
            TextView item_name = mView.findViewById(R.id.publisher_name);
            item_name.setText(item);

        }

        public void setItemCost(String item) {
            TextView book_name = mView.findViewById(R.id.cost_text);
            book_name.setText("₹"+item);
        }

        public void setItemActualCost(String item) {
            TextView book_name = mView.findViewById(R.id.actual_cost_text);
            book_name.setText("₹"+item);
        }

        public void setQuantity(String item) {
            TextView book_name = mView.findViewById(R.id.product_quantity);
            book_name.setText("Qty : "+item);
        }

        public void setItemImage(String item, Context context) {
            itemImage = mView.findViewById(R.id.book_image);
            Glide.with(context).load(item).apply(new RequestOptions().placeholder(R.drawable.no_image).error(R.drawable.no_image)).into(itemImage);
        }

        public void setPersentageText(float numerator,float denominator) {
            TextView itemImage = mView.findViewById(R.id.discount);
            int percentage = (int)(numerator * 100.0 / denominator + 0.5);
            itemImage.setText(100-percentage + "% off");
        }


        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClick(key);
            }
        }

        @Override
        public boolean onLongClick(View view) {
            if (itemLongClickListener != null) {
                itemLongClickListener.onItemLongClickClick(key);
            }
            return false;
        }
    }
}
