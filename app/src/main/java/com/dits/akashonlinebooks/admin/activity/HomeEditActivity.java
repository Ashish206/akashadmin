package com.dits.akashonlinebooks.admin.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.dits.akashonlinebooks.admin.model.HomeFilterModel;
import com.dits.akashonlinebooks.admin.model.OfferGetIdModel;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HomeEditActivity extends AppCompatActivity {
    FloatingActionButton addFab;
    ProgressDialog progressDialog;
    RecyclerView listView;
    String[] values = {"slider","banner","horizontal_list","offer_zone"};
    public static final int REQUEST_IMAGE_1 = 100;
    public static final int REQUEST_IMAGE_2 = 200;
    public static final int REQUEST_IMAGE_3 = 300;
    private Uri resultUri1 = null;
    private Uri resultUri2 = null;
    private Uri resultUri3 = null;
    ImageView imageView1;
    ImageView imageView2;
    ImageView imageView3;
    List<OfferGetIdModel> offerList;
    String[] offerListArray;
    String idKey= "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_edit);

        listView = findViewById(R.id.itemList);
        addFab = findViewById(R.id.add_fab_btn);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        offerList = new ArrayList();
        loadOffer();
        addFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFeatureDialog();
            }
        });

        listView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        listView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void onStart() {
        super.onStart();
        loadCategoryList();
    }


    private void loadCategoryList() {
        FirebaseRecyclerOptions<HomeFilterModel> options =
                new FirebaseRecyclerOptions.Builder<HomeFilterModel>()
                        .setQuery(Constant.rootRef.child("HomeScreen").orderByChild("position").limitToFirst(50), HomeFilterModel.class)
                        .build();

        loadList(options);
    }

    private void loadList(FirebaseRecyclerOptions<HomeFilterModel> options) {
        FirebaseRecyclerAdapter adapter = new FirebaseRecyclerAdapter<HomeFilterModel, ItemViewHolder>(options) {
            @Override
            public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.notes_name_single_layout, parent, false);

                return new ItemViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(ItemViewHolder holder, int position, final HomeFilterModel model) {
                final String id =getRef(position).getKey();
                holder.setItemName(model.getType());
                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        idKey = id;
                        showFeatureDialog();
                    }
                });
                holder.itemCount(model.getPosition(),id);
                holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                       deleteDialog(id);
                        return false;
                    }
                });

            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                progressDialog.dismiss();
            }
        };
        adapter.startListening();
        listView.setAdapter(adapter);
    }

    private void deleteDialog(final String id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeEditActivity.this);
        builder.setTitle("Delete");
        builder.setMessage("Do you want to delete this item?");
        builder.setCancelable(false);
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Constant.rootRef.child("HomeScreen").child(id).setValue(null);
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    private void showFeatureDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select");

        builder.setItems(values, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which==0){
                    showSliderDialog();
                }else if (which == 1){
                    showBannerDialog();
                }else if (which == 2){
                    showHorizontalListDialog();
                }else if (which == 3){
                    showOfferZoneDialog();
                }
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    private void loadOffer() {
        offerList.clear();
        Constant.offerZoneRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    offerList.add(ds.getValue(OfferGetIdModel.class));
                }
                offerList.add(new OfferGetIdModel("null","null"));
                convertArrayListToArray();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    void convertArrayListToArray(){
        offerListArray = new String[offerList.size()];
        for(int i=0; i<offerList.size(); i++){
            offerListArray[i] = offerList.get(i).getCategory();
        }

        //showDialog(values);
    }


    private void showOfferZoneDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeEditActivity.this);
        builder.setTitle("Add");
        LayoutInflater inflater = LayoutInflater.from(HomeEditActivity.this);
        View view = inflater.inflate(R.layout.dialog_layout_for_offer,null,false);
        final EditText heading = view.findViewById(R.id.Heading);
        final Spinner spn1 = view.findViewById(R.id.sinner1);
        ArrayAdapter arrayAdapter = new ArrayAdapter(HomeEditActivity.this,android.R.layout.simple_list_item_1,offerListArray);
        spn1.setAdapter(arrayAdapter);

        builder.setView(view);
        builder.setCancelable(false);

        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (!TextUtils.isEmpty(heading.getText().toString())) {
                    progressDialog.show();
                    if (idKey==""){
                        idKey = Constant.rootRef.child("HomeScreen").push().getKey();
                    }
                    HashMap hm = new HashMap<>();
                    hm.put("category_name",heading.getText().toString());
                    hm.put("filter",offerList.get(spn1.getSelectedItemPosition()).getId());
                    hm.put("type","offer_zone");
                    hm.put("position","1");
                    Constant.rootRef.child("HomeScreen").child(idKey).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            if (task.isSuccessful()){
                                progressDialog.dismiss();
                                idKey = "";
                                Toast.makeText(getApplicationContext(), "Added", Toast.LENGTH_SHORT).show();

                            }   else {
                                Toast.makeText(getApplicationContext(), "Oops/" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();

                            }
                            idKey = "";
                        }
                    });
                }else {
                    Toast.makeText(HomeEditActivity.this, "Fill all fields", Toast.LENGTH_SHORT).show();
                }
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                idKey = "";
            }
        });
        builder.show();

    }

    private void showHorizontalListDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeEditActivity.this);
        builder.setTitle("Add");
        LayoutInflater inflater = LayoutInflater.from(HomeEditActivity.this);
        View view = inflater.inflate(R.layout.dialog_layout_for_horizontal_list,null,false);
        final EditText heading = view.findViewById(R.id.Heading);
        final EditText spn1 = view.findViewById(R.id.category);

        builder.setCancelable(false);
        builder.setView(view);

        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (!TextUtils.isEmpty(heading.getText().toString()) && !TextUtils.isEmpty(spn1.getText().toString())) {
                    if (idKey==""){
                        idKey = Constant.rootRef.child("HomeScreen").push().getKey();
                    }
                    HashMap hm = new HashMap<>();
                    hm.put("category_name",heading.getText().toString());
                    hm.put("filter",spn1.getText().toString());
                    hm.put("type","horizontal_list");
                    hm.put("position","1");
                    Constant.rootRef.child("HomeScreen").child(idKey).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            if (task.isSuccessful()){
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Added", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "Oops/" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                            idKey = "";
                        }
                    });
                }else {
                    Toast.makeText(HomeEditActivity.this, "Fill all fields", Toast.LENGTH_SHORT).show();
                }
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                idKey = "";
            }
        });
        builder.show();

    }

    private void showBannerDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeEditActivity.this);
        builder.setTitle("Add Banner");
        LayoutInflater inflater = LayoutInflater.from(HomeEditActivity.this);
        View view = inflater.inflate(R.layout.dialog_layout_for_banner,null,false);
        imageView1 = view.findViewById(R.id.img1);
        builder.setCancelable(false);
        final Spinner spn1 = view.findViewById(R.id.sinner1);
        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, 100);
            }
        });

        final EditText heading = view.findViewById(R.id.Heading);

        ArrayAdapter arrayAdapter = new ArrayAdapter(HomeEditActivity.this,android.R.layout.simple_list_item_1,offerListArray);
        spn1.setAdapter(arrayAdapter);

        builder.setView(view);

        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (resultUri1 != null && !TextUtils.isEmpty(heading.getText().toString())) {

                    progressDialog.show();
                    final StorageReference filePath = FirebaseStorage.getInstance().getReference().child("HomeScreenSlider/").child(resultUri1.getLastPathSegment());

                    Bitmap bitmap = null;
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), resultUri1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    try {
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 20, baos);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    byte[] data = baos.toByteArray();
                    final UploadTask uploadTask = filePath.putBytes(data);
                    uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if (!task.isSuccessful()) {
                                throw task.getException();
                            }
                            return filePath.getDownloadUrl();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            if (task.isSuccessful()) {
                                Uri downloadUri3 = task.getResult();
                                HashMap hm = new HashMap<>();
                                hm.put("category_name",heading.getText().toString());
                                hm.put("image",downloadUri3.toString());
                                hm.put("banner_click",offerList.get(spn1.getSelectedItemPosition()).getId());
                                hm.put("type","banner");
                                hm.put("position","1");
                                if (idKey==""){
                                    idKey = Constant.rootRef.child("HomeScreen").push().getKey();
                                }
                                Constant.rootRef.child("HomeScreen").child(idKey).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                                    @Override
                                    public void onComplete(@NonNull Task task) {
                                        if (task.isSuccessful()){
                                            progressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Banner Added", Toast.LENGTH_SHORT).show();

                                        }   else {
                                            Toast.makeText(getApplicationContext(), "Oops/" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();

                                        }
                                        idKey = "";
                                    }
                                });


                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Oops/" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });


                }else {
                    Toast.makeText(getApplicationContext(), "Fill all Details", Toast.LENGTH_SHORT).show();

                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                idKey = "";
            }
        });

        builder.show();

    }

    private void showSliderDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeEditActivity.this);
        builder.setTitle("Add Slider");
        LayoutInflater inflater = LayoutInflater.from(HomeEditActivity.this);
        View view = inflater.inflate(R.layout.dialog_layout_for_slider,null,false);
        imageView1 = view.findViewById(R.id.img1);
        imageView2 = view.findViewById(R.id.img2);
        imageView3 = view.findViewById(R.id.img3);
        final Spinner spn1 = view.findViewById(R.id.sinner1);
        final Spinner spn2 = view.findViewById(R.id.sinner2);
        final Spinner spn3 = view.findViewById(R.id.sinner3);
        final EditText heading = view.findViewById(R.id.Heading);

        ArrayAdapter arrayAdapter = new ArrayAdapter(HomeEditActivity.this,android.R.layout.simple_list_item_1,offerListArray);
        spn1.setAdapter(arrayAdapter);
        spn2.setAdapter(arrayAdapter);
        spn3.setAdapter(arrayAdapter);
        builder.setCancelable(false);

        builder.setView(view);
        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, 100);
            }
        });

        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, 200);
            }
        });

        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, 300);
            }
        });


        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if (resultUri1 != null && resultUri2 != null && resultUri3 != null && !TextUtils.isEmpty(heading.getText().toString())) {

                    progressDialog.show();
                    final StorageReference filePath = FirebaseStorage.getInstance().getReference().child("HomeScreenSlider/").child(resultUri1.getLastPathSegment());
                    final StorageReference filePath2 = FirebaseStorage.getInstance().getReference().child("HomeScreenSlider/").child(resultUri2.getLastPathSegment());

                    final StorageReference filePath3 = FirebaseStorage.getInstance().getReference().child("HomeScreenSlider/").child(resultUri3.getLastPathSegment());


                    //--------one
                    Bitmap bitmap = null;
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), resultUri1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    try {
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 20, baos);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    byte[] data = baos.toByteArray();
                    final UploadTask uploadTask = filePath.putBytes(data);

                    //----------two
                    Bitmap bitmap2 = null;
                    try {
                        bitmap2 = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), resultUri2);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
                    try {
                        bitmap2.compress(Bitmap.CompressFormat.JPEG, 20, baos2);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    byte[] data2 = baos2.toByteArray();
                    final UploadTask uploadTask2 = filePath2.putBytes(data2);

                    //----------three
                    Bitmap bitmap3 = null;
                    try {
                        bitmap3 = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), resultUri3);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    ByteArrayOutputStream baos3 = new ByteArrayOutputStream();
                    try {
                        bitmap3.compress(Bitmap.CompressFormat.JPEG, 20, baos3);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    byte[] data3 = baos3.toByteArray();
                    final UploadTask uploadTask3 = filePath3.putBytes(data3);

                    uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if (!task.isSuccessful()) {
                                throw task.getException();
                            }
                            return filePath.getDownloadUrl();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            if (task.isSuccessful()) {
                                final Uri downloadUri1 = task.getResult();
                                uploadTask2.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                    @Override
                                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                        if (!task.isSuccessful()) {
                                            throw task.getException();
                                        }
                                        return filePath2.getDownloadUrl();
                                    }
                                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Uri> task) {
                                        if (task.isSuccessful()) {
                                            final Uri downloadUri2 = task.getResult();
                                            uploadTask3.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                                @Override
                                                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                                    if (!task.isSuccessful()) {
                                                        throw task.getException();
                                                    }
                                                    return filePath3.getDownloadUrl();
                                                }
                                            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Uri> task) {
                                                    if (task.isSuccessful()) {
                                                        Uri downloadUri3 = task.getResult();
                                                        if (idKey==""){
                                                            idKey = Constant.rootRef.child("HomeScreen").push().getKey();
                                                        }
                                                        HashMap hm = new HashMap<>();
                                                        hm.put("category_name",heading.getText().toString());
                                                        hm.put("slider1",downloadUri1.toString());
                                                        hm.put("slider2",downloadUri2.toString());
                                                        hm.put("slider3",downloadUri3.toString());
                                                        hm.put("slider1_click",offerList.get(spn1.getSelectedItemPosition()).getId());
                                                        hm.put("slider2_click",offerList.get(spn2.getSelectedItemPosition()).getId());
                                                        hm.put("slider3_click",offerList.get(spn3.getSelectedItemPosition()).getId());
                                                        hm.put("type","slider");
                                                        hm.put("position","1");
                                                        Constant.rootRef.child("HomeScreen").child(idKey).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                                                            @Override
                                                            public void onComplete(@NonNull Task task) {
                                                                if (task.isSuccessful()){
                                                                    progressDialog.dismiss();
                                                                    Toast.makeText(getApplicationContext(), "Slider Added", Toast.LENGTH_SHORT).show();

                                                                }   else {
                                                                    Toast.makeText(getApplicationContext(), "Oops/" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();

                                                                }
                                                                idKey = "";
                                                            }
                                                        });


                                                    } else {
                                                        progressDialog.dismiss();
                                                        Toast.makeText(getApplicationContext(), "Oops/" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            });

                                        } else {
                                            progressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Oops/" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });

                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Oops/" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }else {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Fill all Details", Toast.LENGTH_SHORT).show();
                }




            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                idKey = "";
            }
        });

        builder.show();




    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {
        View mView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setItemName(String item) {
            TextView book_name = mView.findViewById(R.id.heading);
            book_name.setText(item);
        }

        public void itemCount(int count, final String key){
            final EditText et = mView.findViewById(R.id.count);
            et.setText(String.valueOf(count));
            et.setVisibility(View.VISIBLE);
            et.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if (!b){
                        if (!TextUtils.isEmpty(et.getText().toString().trim())){
                            progressDialog.show();
                            Constant.rootRef.child("HomeScreen").child(key).child("position").setValue(Integer.valueOf(et.getText().toString())).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    progressDialog.dismiss();
                                }
                            });
                        }else {
                            et.setError("Empty");
                        }
                    }
                }
            });
        }

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_1) {
            if (resultCode == Activity.RESULT_OK) {
                final Uri imageUri = data.getData();
                resultUri1 = imageUri;
                imageView1.setImageURI(resultUri1);
            }
        }
        else if (requestCode == REQUEST_IMAGE_2) {
            if (resultCode == Activity.RESULT_OK) {
                final Uri imageUri = data.getData();
                resultUri2 = imageUri;
                imageView2.setImageURI(resultUri2);
            }
        }
        else if (requestCode == REQUEST_IMAGE_3) {
            if (resultCode == Activity.RESULT_OK) {
                final Uri imageUri = data.getData();
                resultUri3 = imageUri;
                imageView3.setImageURI(resultUri3);
            }
        }

    }
}
