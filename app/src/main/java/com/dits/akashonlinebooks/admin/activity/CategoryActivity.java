package com.dits.akashonlinebooks.admin.activity;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.dits.akashonlinebooks.admin.fragments.CategoryFragment;
import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.extra.Constant;

public class CategoryActivity extends AppCompatActivity {
    public static FragmentManager fragmentManager;
    public static Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        context = this;
        Constant.selectedItemName = getIntent().getExtras().getString("ItemName");
        Fragment categoryFragment = new CategoryFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main_frame,categoryFragment)
                .commit();

    }
    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() != 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }
}
