package com.dits.akashonlinebooks.admin.extra;

import com.dits.akashonlinebooks.admin.model.AuthorModel;
import com.dits.akashonlinebooks.admin.model.BookDataModel;
import com.dits.akashonlinebooks.admin.model.PublisherModel;
import com.dits.akashonlinebooks.admin.model.SearchResultModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("bookDetails/forHome/")
    Call<List<BookDataModel>> getHomeBookDetails(@Query("filterBy") String filterBy);

    @GET("bookDetails/forHome/offerList/")
    Call<List<BookDataModel>> getOfferList(@Query("bookKeyList") String bookKeyList);

    @GET("bookDetails/offerList/")
    Call<List<BookDataModel>> getAllOfferList(@Query("bookKeyList") String bookKeyList);

    @GET("bookDetails/bookKey/")
    Call<List<BookDataModel>> getSingleBookDetails(@Query("bookKey") String bookKey);

    @GET("bookDetails/")
    Call<List<BookDataModel>> getAllBookDetails(@Query("page") int page);

    @GET("bookDetails/filterBy/")
    Call<List<BookDataModel>> getFilteredList(@Query("page") int page, @Query("filterKey") String filterKey, @Query("filterBy") String filterBy);

    @GET("bookDetails/search/")
    Call<List<SearchResultModel>> getSearchResult(@Query("searchKey") String searchKey);

    @GET("bookDetails/searchResult/")
    Call<List<BookDataModel>> getSearchDetailsResult(@Query("searchKey") String searchKey);

    @GET("bookDetails/authorsList/")
    Call<List<AuthorModel>> getAuthorList();

    @GET("bookDetails/publisherList/")
    Call<List<PublisherModel>> getPublisherList();

    @GET("getAllWithoutImageBooks")
    Call<List<BookDataModel>> getNonImageBooks();

    @DELETE("deleteBooks/")
    Call<Void> deleteBook(@Query("isbn") String isbn);


}
