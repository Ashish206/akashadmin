package com.dits.akashonlinebooks.admin.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.dits.akashonlinebooks.admin.model.SubCategoryModel;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.HashMap;


public class SubCategoryFragment extends Fragment {
    FloatingActionButton addFab;
    ProgressDialog progressDialog;
    RecyclerView itemList;
//    AddQuestionsFragment addQuestionsFragment;
//    ShowQuestionFragment showQuestionFragment;


    @SuppressLint("RestrictedApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_sub_category, container, false);

        addFab = v.findViewById(R.id.add_fab_btn);
        if (!Constant.admin){
            addFab.setVisibility(View.GONE);
        }
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

//        addQuestionsFragment = new AddQuestionsFragment();
//        showQuestionFragment = new ShowQuestionFragment();


        addFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddItemDialog();
            }

        });

        itemList = v.findViewById(R.id.itemList);
        itemList.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        itemList.setLayoutManager(mLayoutManager);


        return v;
    }

    private void showAddItemDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setTitle("Add Title");
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View addBooks = inflater.inflate(R.layout.add_sub_category_layout,null);
        final TextInputEditText fieldFirst = addBooks.findViewById(R.id.field_first);
        fieldFirst.setHint("Title Name");

        final Button saveBtn = addBooks.findViewById(R.id.saveBtn);
        final Button cancelBtn = addBooks.findViewById(R.id.cancelBtn);

        dialog.setCancelable(false);
        dialog.setView(addBooks);


        final AlertDialog alertDialog = dialog.create();

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(fieldFirst.getText().toString())){
                    progressDialog.show();

                    HashMap hm = new HashMap();
                    hm.put("subcategory",fieldFirst.getText().toString());

                    String key =  Constant.categoryRef.child(Constant.selectedItemName).push().getKey();

                    Constant.subcategoryRef.child(Constant.selectedItemName).child(key).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(Task task) {
                            if (task.isSuccessful()){
                                progressDialog.dismiss();
                                alertDialog.dismiss();
                                Toast.makeText(getContext(), "Item Added!!", Toast.LENGTH_SHORT).show();
                            }else {
                                progressDialog.dismiss();
                                alertDialog.dismiss();
                                Toast.makeText(getContext(), "Oops/"+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                }
                else {
                    Toast.makeText(getContext(), "Please Fill All Fields...", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });


        alertDialog.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        loadCategoryList();
    }

    private void loadCategoryList() {
        FirebaseRecyclerOptions<SubCategoryModel> options =
                new FirebaseRecyclerOptions.Builder<SubCategoryModel>()
                        .setQuery(Constant.subcategoryRef.child(Constant.selectedItemName), SubCategoryModel.class)
                        .build();

        loadList(options);
    }

    private void loadList(FirebaseRecyclerOptions<SubCategoryModel> options) {
        FirebaseRecyclerAdapter adapter = new FirebaseRecyclerAdapter<SubCategoryModel, ItemViewHolder>(options) {
            @Override
            public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.single_sub_category_layout, parent, false);

                return new ItemViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(ItemViewHolder holder, int position, final SubCategoryModel model) {
                final String id =getRef(position).getKey();
                holder.setItemName(model.getSubcategory());
                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Constant.selectedSubCategory = model.getSubcategory();
                        Constant.subCategoryId = id;
//                        if (Constant.admin){
//                            CategoryActivity.fragmentManager.beginTransaction().replace(R.id.main_frame, addQuestionsFragment).addToBackStack(null).commit();
//                        }else {
//                            CategoryActivity.fragmentManager.beginTransaction().replace(R.id.main_frame, showQuestionFragment).addToBackStack(null).commit();
//                        }

                    }
                });

                holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        if (Constant.admin){
                            editDialog(model.getSubcategory(),id);
                        }
                        return false;
                    }
                });

            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                progressDialog.dismiss();
            }
        };
        adapter.startListening();
        itemList.setAdapter(adapter);
    }

    private void editDialog(String bookName, final String id) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setTitle("Edit Subject");
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View addBooks = inflater.inflate(R.layout.add_sub_category_layout,null);
        final TextInputEditText fieldFirst = addBooks.findViewById(R.id.field_first);
        fieldFirst.setHint("Subject");

        final Button saveBtn = addBooks.findViewById(R.id.saveBtn);
        final Button cancelBtn = addBooks.findViewById(R.id.cancelBtn);
        ImageView deleteBtn = addBooks.findViewById(R.id.delete_btn);

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog(id);
            }
        });

        dialog.setCancelable(false);
        dialog.setView(addBooks);


        fieldFirst.setText(bookName);


        final AlertDialog alertDialog = dialog.create();

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(fieldFirst.getText().toString())){
                    progressDialog.show();

                    HashMap hm = new HashMap();
                    hm.put("subcategory",fieldFirst.getText().toString());

                    Constant.subcategoryRef.child(Constant.selectedItemName).child(id).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(Task task) {
                            if (task.isSuccessful()){
                                progressDialog.dismiss();
                                alertDialog.dismiss();
                                Toast.makeText(getContext(), "Item Added!!", Toast.LENGTH_SHORT).show();
                            }else {
                                progressDialog.dismiss();
                                alertDialog.dismiss();
                                Toast.makeText(getContext(), "Oops/"+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                }
                else {
                    Toast.makeText(getContext(), "Please Fill All Fields...", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });


        alertDialog.show();
    }

    private void deleteDialog(final String id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Delete");
        builder.setMessage("Do you want to delete this item?");
        builder.setCancelable(false);
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Constant.subcategoryRef.child(Constant.selectedItemName).child(id).setValue(null);
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    private static class ItemViewHolder extends RecyclerView.ViewHolder {
        View mView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setItemName(String item) {
            TextView book_name = mView.findViewById(R.id.heading);
            book_name.setText(item);

        }
    }
}

