package com.dits.akashonlinebooks.admin.model;

public class HomeFilterModel {
    String type;
    int position;

    public HomeFilterModel() {
    }

    public HomeFilterModel(String type, int position) {
        this.type = type;
        this.position = position;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
