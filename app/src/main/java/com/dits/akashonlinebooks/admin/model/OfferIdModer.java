package com.dits.akashonlinebooks.admin.model;

public class OfferIdModer {
    String id;

    public OfferIdModer() {
    }

    public OfferIdModer(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
