package com.dits.akashonlinebooks.admin.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.adapter.BookDetailGridAdapter;
import com.dits.akashonlinebooks.admin.adapter.BooksDetailsAdapter;
import com.dits.akashonlinebooks.admin.extra.ApiClient;
import com.dits.akashonlinebooks.admin.extra.ApiInterface;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.dits.akashonlinebooks.admin.model.BookDataModel;
import com.dits.akashonlinebooks.admin.model.OfferGetIdModel;
import com.dits.akashonlinebooks.admin.model.OfferIdModer;
import com.dits.akashonlinebooks.admin.model.WishListModel;
import com.dits.akashonlinebooks.admin.utils.PaginationAdapterCallback;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class OfferDetailsActivity extends AppCompatActivity  implements PaginationAdapterCallback,BookDetailGridAdapter.ItemLongClickListener,
    BookDetailGridAdapter.ItemListener{
    SearchView searchView;
    TextView heading;
    RecyclerView recyclerView;
    BookDetailGridAdapter bookDetailsActivity;
    ArrayList<BookDataModel> bookData;
    ProgressDialog progressDialog;
    ArrayList<BookDataModel> bookList;
    FloatingActionButton addBtn;
    Parcelable state;
    LinearLayoutManager linearLayoutManager;
    List<OfferGetIdModel> offerList;
    DatabaseReference offerBooksRef;

    String id;

    Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_details);

        ctx = this;
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        bookList = new ArrayList<>();



        bookData = new ArrayList<>();

        id = getIntent().getExtras().getString("id");

        offerBooksRef = FirebaseDatabase.getInstance().getReference().child("OfferZoneBooks");

        searchView = findViewById(R.id.search_bar);
        addBtn = findViewById(R.id.addBtn);
        heading = findViewById(R.id.titleText);
        heading.setText("Book Details");

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                builder.setTitle("Delete");
                builder.setMessage("Do you want to delete All books from offer zone?");
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        progressDialog.show();

                        offerBooksRef.child(id).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()){
                                    loadDetails();
                                    Toast.makeText(OfferDetailsActivity.this, "deleted.", Toast.LENGTH_SHORT).show();
                                }else {
                                    Toast.makeText(OfferDetailsActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                                progressDialog.dismiss();
                            }
                        });

                    }
                });
                builder.show();
            }
        });

        offerList = new ArrayList();

        recyclerView = findViewById(R.id.recycler_view);

        linearLayoutManager = new LinearLayoutManager(this);
        if(state != null) {
            linearLayoutManager.onRestoreInstanceState(state);
        }
        recyclerView.setLayoutManager( new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false));

        bookDetailsActivity = new BookDetailGridAdapter(getApplicationContext(),OfferDetailsActivity.this,OfferDetailsActivity.this,OfferDetailsActivity.this);
        recyclerView.setAdapter(bookDetailsActivity);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                bookDetailsActivity.getFilter().filter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                bookDetailsActivity.getFilter().filter(s);
                return false;
            }
        });
        loadDetails();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }


    private void loadDetails() {
        bookList.clear();
        progressDialog.show();
        Constant.rootRef.child("OfferZoneBooks").child(getIntent().getExtras().getString("id")).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount()!=0){
                for (DataSnapshot ds : dataSnapshot.getChildren()) {

                        ApiInterface apiService =
                                ApiClient.getClient().create(ApiInterface.class);

                        Call<List<BookDataModel>> call = apiService.getSingleBookDetails(ds.child("id").getValue().toString());
                        call.enqueue(new Callback<List<BookDataModel>>() {
                            @Override
                            public void onResponse(Call<List<BookDataModel>> call, retrofit2.Response<List<BookDataModel>> response) {
                                for (BookDataModel result : response.body()) {
                                    bookList.add(result);
                                }

                                Log.d("---------->",""+dataSnapshot.getChildrenCount()+bookList.size());
                                if (dataSnapshot.getChildrenCount() == bookList.size()) {
                                    showList();
                                }
                            }

                            @Override
                            public void onFailure(Call<List<BookDataModel>> call, Throwable t) {
                                progressDialog.dismiss();
                            }
                        });


                }
                }else {
                    progressDialog.dismiss();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progressDialog.dismiss();
            }
        });
    }
    private void showList(){
        progressDialog.dismiss();
        bookDetailsActivity.addList(bookList);
        bookDetailsActivity.notifyDataSetChanged();
    }


    @Override
    protected void onResume() {
        super.onResume();
        bookDetailsActivity.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(String key, ImageView imageView) {
        Intent i = new Intent(OfferDetailsActivity.this,BookDetailEditActivity.class);
        i.putExtra("key",key);
        startActivity(i);
    }

    @Override
    public void onItemLongClickListener(final String key) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete");
        builder.setMessage("Do you want to delete this book from offer zone?");
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                progressDialog.show();

                offerBooksRef.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot ds : dataSnapshot.getChildren()){
                            if (ds.child("id").getValue().toString().equals(key)){
                                offerBooksRef.child(id).child(ds.getKey()).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()){
                                            loadDetails();
                                            Toast.makeText(OfferDetailsActivity.this, "deleted.", Toast.LENGTH_SHORT).show();
                                        }else {
                                            Toast.makeText(OfferDetailsActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                        progressDialog.dismiss();
                                    }
                                });
                                break;
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        });
        builder.show();
    }

    @Override
    public void retryPageLoad() {

    }
}
