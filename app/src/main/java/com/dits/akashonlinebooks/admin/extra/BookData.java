package com.dits.akashonlinebooks.admin.extra;

import android.support.annotation.NonNull;

import com.dits.akashonlinebooks.admin.model.BookDataModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class BookData {

    public static ArrayList<BookDataModel> bookData;

    public static void BookData(){
        bookData = new ArrayList<>();


        Constant.rootRef.child("BooksDetail").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                bookData.clear();
                for (final DataSnapshot ds : dataSnapshot.getChildren()) {
                    BookDataModel bookDataModel = new BookDataModel();
                    bookDataModel.setBookName(ds.child("BookName").getValue().toString());
                    bookDataModel.setAuthor(ds.child("Author").getValue().toString());
                    bookDataModel.setCategory(ds.child("Category").getValue().toString());
                    bookDataModel.setPublisher(ds.child("Publisher").getValue().toString());
                    bookDataModel.setImage(ds.child("Image").getValue().toString());
                    bookDataModel.setMRP(ds.child("MRP").getValue().toString());
                    bookDataModel.setActualCost(ds.child("actualCost").getValue().toString());
                    bookDataModel.setBookKey(ds.child("key").getValue().toString());
                    bookDataModel.setIsbn(ds.child("isbn").getValue().toString());
                    bookData.add(bookDataModel);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
