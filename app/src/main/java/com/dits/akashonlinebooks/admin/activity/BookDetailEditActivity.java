package com.dits.akashonlinebooks.admin.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.extra.ApiClient;
import com.dits.akashonlinebooks.admin.extra.ApiInterface;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.dits.akashonlinebooks.admin.fragments.BookListFragment;
import com.dits.akashonlinebooks.admin.model.BookDataModel;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dits.akashonlinebooks.admin.activity.BookDetailsActivity.isbnList;

public class BookDetailEditActivity extends AppCompatActivity {

    TextInputEditText isbnField,bookNameField,MRPField,printedPriceField,DescriptionField,
    pagesField,EditionField,BookTagsField,BookAboutField,AboutAuthorField,BindingField,DimensionsField
            ,WeightField,QuantityField,AdditionalBookCodeField;
    private static final int CAMERA_REQUEST_CODE = 100;
    Spinner mediumField,saleCurrency;
    ImageView imageField;
    Button submitBtn;
    String medium[] ={"English Medium","Hindi Medium","Bilingual"};
    String currency[] ={"Rs","$","€"};
    String newPushKey;
    DatabaseReference mRef;
    private Uri resultUri = null;
    ProgressDialog progressDialog;
    String key = null;
    AutoCompleteTextView categoryField,authorField,PublisherField;
    ArrayList<BookDataModel> bookDetailList;
    public static final int REQUEST_IMAGE = 100;
    private ApiInterface movieService;

    ImageView addAuthor,addPublisher,addCategory;
    ArrayAdapter<String> stringArrayAdapterPatient;
    ArrayAdapter<String> authorAdapter;
    ArrayAdapter<String> publisherAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_detail_edit);
        bookDetailList = new ArrayList<>();
        isbnField = findViewById(R.id.isbnField);
        authorField = findViewById(R.id.authorField);
        bookNameField = findViewById(R.id.bookNameField);
        PublisherField = findViewById(R.id.PublisherField);
        MRPField = findViewById(R.id.MRPField);
        printedPriceField = findViewById(R.id.printedPriceField);
        DescriptionField = findViewById(R.id.DescriptionField);
        pagesField = findViewById(R.id.pagesField);
        EditionField = findViewById(R.id.EditionField);
        BookTagsField = findViewById(R.id.BookTagsField);
        BookAboutField = findViewById(R.id.BookAboutField);
        AboutAuthorField = findViewById(R.id.AboutAuthorField);
        BindingField = findViewById(R.id.BindingField);
        DimensionsField = findViewById(R.id.DimensionsField);
        WeightField = findViewById(R.id.WeightField);
        QuantityField = findViewById(R.id.QuantityField);
        AdditionalBookCodeField = findViewById(R.id.AdditionalBookCodeField);
        mediumField = findViewById(R.id.mediumField);
        saleCurrency = findViewById(R.id.saleCurrency);
        imageField = findViewById(R.id.imageField);
        submitBtn = findViewById(R.id.submitBtn);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        mRef = Constant.rootRef.child("BooksDetail");
        key = getIntent().getExtras().getString("key");

        addAuthor = findViewById(R.id.add_author);
        addPublisher = findViewById(R.id.add_publisher);
        addCategory = findViewById(R.id.add_category);

        addAuthor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddItemDialog("Authors");
            }
        });

        addPublisher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddItemDialog("Publishers");
            }
        });

        addCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddItemDialog("Category");
            }
        });


        ArrayAdapter<String> mediumAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, medium);
        ArrayAdapter<String> currencyAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, currency);
        mediumField.setAdapter(mediumAdapter);
        saleCurrency.setAdapter(currencyAdapter);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA},
                        CAMERA_REQUEST_CODE);
            }
        }

        movieService = ApiClient.getClient().create(ApiInterface.class);

        categoryField = findViewById(R.id.input);

        stringArrayAdapterPatient= new ArrayAdapter<String>(this,android.support.v7.appcompat.R.layout.select_dialog_item_material,Constant.categoryList);
        authorAdapter= new ArrayAdapter<String>(this,android.support.v7.appcompat.R.layout.select_dialog_item_material,Constant.authorList);

        publisherAdapter= new ArrayAdapter<String>(this,android.support.v7.appcompat.R.layout.select_dialog_item_material,Constant.publishList);

        authorField.setAdapter(authorAdapter);
        PublisherField.setAdapter(publisherAdapter);
        categoryField.setAdapter(stringArrayAdapterPatient);

        authorField.setValidator(new Validator());
        authorField.setOnFocusChangeListener(new FocusListener());

        PublisherField.setValidator(new Validator());
        PublisherField.setOnFocusChangeListener(new FocusListener());

        categoryField.setValidator(new Validator());
        categoryField.setOnFocusChangeListener(new FocusListener());

        stringArrayAdapterPatient.notifyDataSetChanged();

        imageField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isStoragePermissionGranted()) {
                    onProfileImageClick();
                }

            }
        });



        if (getIntent().getExtras().getString("key").length() > 2){

            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<List<BookDataModel>> call = apiService.getSingleBookDetails(key);
            call.enqueue(new Callback<List<BookDataModel>>() {
                @Override
                public void onResponse(Call<List<BookDataModel>> call, Response<List<BookDataModel>> response) {

                    for (BookDataModel result : response.body()) {
                        bookDetailList.add(result);
                    }

                    newPushKey = bookDetailList.get(0).getBookKey();
                    isbnField.setText(bookDetailList.get(0).getIsbn());
                    authorField.setText(bookDetailList.get(0).getAuthor());
                    bookNameField.setText(bookDetailList.get(0).getBookName());
                    PublisherField.setText(bookDetailList.get(0).getPublisher());
                    MRPField.setText(bookDetailList.get(0).getMRP());
                    printedPriceField.setText(bookDetailList.get(0).getActualCost());
                    DescriptionField.setText(bookDetailList.get(0).getDescriptions());
                    pagesField.setText(String.valueOf(bookDetailList.get(0).getNumberOfPages()));
                    EditionField.setText(bookDetailList.get(0).getEdition());
                    BookTagsField.setText(bookDetailList.get(0).getBookTags());
                    BookAboutField.setText(bookDetailList.get(0).getBookAbout());
                    AboutAuthorField.setText(bookDetailList.get(0).getAboutAuthor());
                    DimensionsField.setText(bookDetailList.get(0).getDimensions());
                    WeightField.setText(bookDetailList.get(0).getWeight());
                    QuantityField.setText(String.valueOf(bookDetailList.get(0).getQuantity()));
                    AdditionalBookCodeField.setText(bookDetailList.get(0).getAdditionalBookCode());
                    mediumField.setSelection(getIndex(medium,bookDetailList.get(0).getMediumType()));
                    saleCurrency.setSelection(getIndex(currency,bookDetailList.get(0).getSaleCurrency()));
                    categoryField.setText(bookDetailList.get(0).getCategory());
                    Glide.with(getApplicationContext()).load(bookDetailList.get(0).getImage()).apply(new RequestOptions().placeholder(R.drawable.no_image).error(R.drawable.no_image)).into(imageField);
                    progressDialog.dismiss();


                }

                @Override
                public void onFailure(Call<List<BookDataModel>> call, Throwable t) {

                }
            });
        }

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(isbnField.getText().toString()) && !TextUtils.isEmpty(authorField.getText().toString()) &&
                        !TextUtils.isEmpty(authorField.getText().toString()) && !TextUtils.isEmpty(bookNameField.getText().toString()) &&
                        !TextUtils.isEmpty(PublisherField.getText().toString()) && !TextUtils.isEmpty(MRPField.getText().toString()) &&
                        !TextUtils.isEmpty(printedPriceField.getText().toString()) && !TextUtils.isEmpty(DescriptionField.getText().toString()) &&
                        !TextUtils.isEmpty(pagesField.getText().toString()) && !TextUtils.isEmpty(EditionField.getText().toString()) &&
                       !TextUtils.isEmpty(BookTagsField.getText().toString()) && !TextUtils.isEmpty(BookAboutField.getText().toString()) &&
                        !TextUtils.isEmpty(AboutAuthorField.getText().toString()) && !TextUtils.isEmpty(AboutAuthorField.getText().toString()) &&
                        !TextUtils.isEmpty(DimensionsField.getText().toString()) && !TextUtils.isEmpty(DimensionsField.getText().toString()) &&
                        !TextUtils.isEmpty(WeightField.getText().toString()) &&
                        !TextUtils.isEmpty(QuantityField.getText().toString()) && !TextUtils.isEmpty(AdditionalBookCodeField.getText().toString())
                && !TextUtils.isEmpty(categoryField.getText().toString())){
                    progressDialog.show();


                        if (getIntent().getExtras().getString("key").length() > 2){
                            if (resultUri == null){
                                uploadDataWithoutImage();
                            }else {
                                uploadData();
                            }
                            BookDetailsActivity.isEdit = true;
                        }
                        else {
                            newPushKey = mRef.push().getKey();
                            insertNewData();
                            BookDetailsActivity.isEdit = true;

                        }



                }else {
                    Toast.makeText(BookDetailEditActivity.this, "Fill All Fields!!", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }


    private void showAddItemDialog(final String cat_name) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Add "+cat_name);
        LayoutInflater inflater = LayoutInflater.from(this);
        View addBooks = inflater.inflate(R.layout.add_category,null);
        final TextInputEditText fieldFirst = addBooks.findViewById(R.id.field_first);
        fieldFirst.setHint(cat_name+" Name");

        final Button saveBtn = addBooks.findViewById(R.id.saveBtn);
        final Button cancelBtn = addBooks.findViewById(R.id.cancelBtn);

        dialog.setCancelable(false);
        dialog.setView(addBooks);


        final AlertDialog alertDialog = dialog.create();

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(fieldFirst.getText().toString())){
                    progressDialog.show();

                    HashMap hm = new HashMap();
                    hm.put("category",fieldFirst.getText().toString().trim().toLowerCase());
                    // hm.put("photo",String.valueOf(downloadUri));
                    String key =  Constant.categoryRef.child(cat_name).push().getKey();

                    Constant.categoryRef.child(cat_name).child(key).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(Task task) {
                            if (task.isSuccessful()){
                                loadCategory();
                                alertDialog.dismiss();
                                Toast.makeText(BookDetailEditActivity.this, "Item Added!!", Toast.LENGTH_SHORT).show();
                            }else {
                                progressDialog.dismiss();
                                alertDialog.dismiss();
                                Toast.makeText(BookDetailEditActivity.this, "Oops/"+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                else {
                    Toast.makeText(BookDetailEditActivity.this, "Please Fill All Fields...", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });


        alertDialog.show();
    }

    private void loadCategory() {
        Constant.rootRef.child("Study").child("Category").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Constant.categoryList = new ArrayList();
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    Constant.categoryList.add(ds.child("category").getValue().toString());
                }
                loadAuthor();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void loadAuthor() {

        Constant.rootRef.child("Study").child("Authors").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Constant.authorList = new ArrayList();
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    Constant.authorList.add(ds.child("category").getValue().toString());
                }
                loadPublishers();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void loadPublishers() {
        Constant.rootRef.child("Study").child("Publishers").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Constant.publishList = new ArrayList();
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    Constant.publishList.add(ds.child("category").getValue().toString());
                }
                stringArrayAdapterPatient= new ArrayAdapter<String>(BookDetailEditActivity.this,R.layout.select_dialog_item_material,Constant.categoryList);
                authorAdapter= new ArrayAdapter<String>(BookDetailEditActivity.this,R.layout.select_dialog_item_material,Constant.authorList);
                publisherAdapter= new ArrayAdapter<String>(BookDetailEditActivity.this,R.layout.select_dialog_item_material,Constant.publishList);
                authorField.setAdapter(authorAdapter);
                PublisherField.setAdapter(publisherAdapter);
                categoryField.setAdapter(stringArrayAdapterPatient);
                progressDialog.dismiss();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.delete_book_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.mybutton) {
            deleteDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(BookDetailEditActivity.this);
        builder.setTitle("Delete");
        builder.setMessage("Do you want to delete this item?");
        builder.setCancelable(false);
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Constant.rootRef.child("BooksDetail").child(key).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            deleteBook(isbnField.getText().toString()).enqueue(new Callback<Void>() {
                                @Override
                                public void onResponse(Call<Void> call, Response<Void> response) {
                                    finish();
                                }
                                @Override
                                public void onFailure(Call<Void> call, Throwable t) {
                                    t.printStackTrace();
                                    Toast.makeText(BookDetailEditActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                });
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }


    private Call<Void> deleteBook(String isbn) {
        return movieService.deleteBook(
                isbn
        );
    }

    class Validator implements AutoCompleteTextView.Validator {

        @Override
        public boolean isValid(CharSequence text) {
            Log.v("Test", "Checking if valid: "+ text);
            Arrays.sort(Constant.categoryList.toArray());
            if (Arrays.binarySearch(Constant.categoryList.toArray(), text.toString()) > 0) {
                return true;
            }

            return false;
        }

        @Override
        public CharSequence fixText(CharSequence invalidText) {
            Log.v("Test", "Returning fixed text");

            /* I'm just returning an empty string here, so the field will be blanked,
             * but you could put any kind of action here, like popping up a dialog?
             *
             * Whatever value you return here must be in the list of valid words.
             */

            return invalidText;
        }
    }

    class FocusListener implements View.OnFocusChangeListener {

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            Log.v("Test", "Focus changed");
            if (v.getId() == R.id.input && !hasFocus) {
                Log.v("Test", "Performing validation");
                ((AutoCompleteTextView)v).performValidation();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == CAMERA_REQUEST_CODE) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                 Toast.makeText(this, "Camera permission granted.", Toast.LENGTH_LONG).show();

            } else {


            }

        }
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("-->","Permission is granted");
                return true;
            } else {

                Log.v("-->","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("-->","Permission is granted");
            return true;
        }
    }

    private void uploadDataWithoutImage() {
//        HashMap hm = new HashMap<>();
//        hm.put("isbn", isbnField.getText().toString());
//        hm.put("Author", authorField.getText().toString());
//        hm.put("BookName", bookNameField.getText().toString());
//        hm.put("Publisher", PublisherField.getText().toString());
//        hm.put("MRP", MRPField.getText().toString());
//        hm.put("actualCost", printedPriceField.getText().toString());
//        hm.put("Description", DescriptionField.getText().toString());
////            hm.put("Image", downloadUri.toString());
//        hm.put("Category", categoryField.getText().toString());
//        hm.put("Medium", mediumField.getSelectedItem().toString());
//        hm.put("NumberOfPages", pagesField.getText().toString());
//        hm.put("Edition", EditionField.getText().toString());
//        hm.put("BookTags", BookTagsField.getText().toString());
//        hm.put("BookAbout", BookAboutField.getText().toString());
//        hm.put("AboutAuthor", AboutAuthorField.getText().toString());
//        hm.put("Binding", BindingField.getText().toString());
//        hm.put("Dimensions", DimensionsField.getText().toString());
//        hm.put("Weight", WeightField.getText().toString());
//        hm.put("AdditionalBookCode", AdditionalBookCodeField.getText().toString());
//        hm.put("SaleCurrency", saleCurrency.getSelectedItem().toString());
//        hm.put("Quantity", QuantityField.getText().toString());
//        hm.put("Rating", "0");
//        hm.put("Ranking", "0");
//        uploadDataToFirebase(hm);



        RequestQueue queue = Volley.newRequestQueue(BookDetailEditActivity.this);

        StringRequest request = new StringRequest(Request.Method.PUT, Constant.BASE_URL+"updateBookDetails/", new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response)
            {

                progressDialog.dismiss();
                showDoneDialog();
            }
        },
                new com.android.volley.Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.d("RESPONSE:          >>> ", error.toString() + "<<<");
                        Toast.makeText(BookDetailEditActivity.this,"Opps !! "+error.toString() , Toast.LENGTH_SHORT).show();

                    }
                })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> jsonParams = new HashMap<>();
                jsonParams.put("imgFlag", "0");
                jsonParams.put("AboutAuthor",  AboutAuthorField.getText().toString());
                jsonParams.put("actualCost",  printedPriceField.getText().toString());
                jsonParams.put("AdditionalBookCode", AdditionalBookCodeField.getText().toString());
                jsonParams.put("Author", authorField.getText().toString());
                jsonParams.put("Binding",  BindingField.getText().toString());
                jsonParams.put("BookAbout", BookAboutField.getText().toString());
                jsonParams.put("BookName", bookNameField.getText().toString());
                jsonParams.put("BookTags",  BookTagsField.getText().toString());
                jsonParams.put("Category", categoryField.getText().toString());
                jsonParams.put("Descriptions", DescriptionField.getText().toString());
                jsonParams.put("Dimensions", DimensionsField.getText().toString());
                jsonParams.put("Edition", EditionField.getText().toString());
                jsonParams.put("isbn", isbnField.getText().toString());
                jsonParams.put("bookKey", newPushKey);
                jsonParams.put("MediumType", mediumField.getSelectedItem().toString());
                jsonParams.put("MRP", MRPField.getText().toString());
                jsonParams.put("NumberOfPages", pagesField.getText().toString());
                jsonParams.put("Publisher", PublisherField.getText().toString());
                jsonParams.put("Quantity", QuantityField.getText().toString());
                jsonParams.put("SaleCurrency", saleCurrency.getSelectedItem().toString());
                jsonParams.put("Weight", WeightField.getText().toString());


                return jsonParams;
            }
            @Override
            public String getBodyContentType(){
                return "application/x-www-form-urlencoded";
            }

        };

        queue.add(request);
    }

    private void insertNewData(){
        if (resultUri != null) {

            final StorageReference filePath = FirebaseStorage.getInstance().getReference().child("book_images").child(resultUri.getLastPathSegment());

            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), resultUri);
            } catch (IOException e) {
                e.printStackTrace();
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 20, baos);
            } catch (Exception e) {
                e.printStackTrace();
            }
            byte[] data = baos.toByteArray();
            UploadTask uploadTask = filePath.putBytes(data);

            uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return filePath.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        final Uri downloadUri = task.getResult();

                        HashMap hm = new HashMap();
                        hm.put("Ranking","0");
                        hm.put("Rating","0");
                        hm.put("key",newPushKey);


                       mRef.child(newPushKey).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                           @Override
                           public void onComplete(@NonNull Task task) {
                              if (task.isSuccessful()){
                                  RequestQueue queue = Volley.newRequestQueue(BookDetailEditActivity.this);

                                  StringRequest request = new StringRequest(Request.Method.POST, Constant.BASE_URL+"addNewBookDetails/", new com.android.volley.Response.Listener<String>() {

                                      @Override
                                      public void onResponse(String response)
                                      {

                                          progressDialog.dismiss();
                                          showDoneDialog();

                                      }
                                  },
                                          new com.android.volley.Response.ErrorListener()
                                          {
                                              @Override
                                              public void onErrorResponse(VolleyError error) {
                                                  progressDialog.dismiss();

                                                  Log.d("RESPONSE:          >>> ", error.toString() + "<<<");
                                                  Toast.makeText(BookDetailEditActivity.this,")pps !! "+error.toString(), Toast.LENGTH_SHORT).show();

                                              }
                                          })
                                  {
                                      @Override
                                      protected Map<String, String> getParams()
                                      {
                                          Map<String, String> jsonParams = new ArrayMap<>();
                                          jsonParams.put("imgFlag", "1");
                                          jsonParams.put("AboutAuthor",  AboutAuthorField.getText().toString());
                                          jsonParams.put("actualCost",  printedPriceField.getText().toString());
                                          jsonParams.put("AdditionalBookCode", AdditionalBookCodeField.getText().toString());
                                          jsonParams.put("Author", authorField.getText().toString());
                                          jsonParams.put("Binding",  BindingField.getText().toString());
                                          jsonParams.put("BookAbout", BookAboutField.getText().toString());
                                          jsonParams.put("BookName", bookNameField.getText().toString());
                                          jsonParams.put("BookTags",  BookTagsField.getText().toString());
                                          jsonParams.put("Category", categoryField.getText().toString());
                                          jsonParams.put("Descriptions", DescriptionField.getText().toString());
                                          jsonParams.put("Dimensions", DimensionsField.getText().toString());
                                          jsonParams.put("Edition", EditionField.getText().toString());
                                          jsonParams.put("Image", downloadUri.toString());
                                          jsonParams.put("isbn", isbnField.getText().toString());
                                          jsonParams.put("bookKey", newPushKey);
                                          jsonParams.put("MediumType", mediumField.getSelectedItem().toString());
                                          jsonParams.put("MRP", MRPField.getText().toString());
                                          jsonParams.put("NumberOfPages", pagesField.getText().toString());
                                          jsonParams.put("Publisher", PublisherField.getText().toString());
                                          jsonParams.put("Quantity", QuantityField.getText().toString());
                                          jsonParams.put("SaleCurrency", saleCurrency.getSelectedItem().toString());
                                          jsonParams.put("Weight", WeightField.getText().toString());
                                          return jsonParams;
                                      }
                                      @Override
                                      public String getBodyContentType(){
                                          return "application/x-www-form-urlencoded";
                                      }
                                  };

                                  queue.add(request);
                              }else {
                                  Toast.makeText(BookDetailEditActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                              }
                           }
                       });


//                            HashMap hm = new HashMap<>();
//                            hm.put("isbn", isbnField.getText().toString());
//                            hm.put("Author", authorField.getText().toString());
//                            hm.put("BookName", bookNameField.getText().toString());
//                            hm.put("Publisher", PublisherField.getText().toString());
//                            hm.put("MRP", MRPField.getText().toString());
//                            hm.put("actualCost", printedPriceField.getText().toString());
//                            hm.put("Description", DescriptionField.getText().toString());
//                            hm.put("Image", downloadUri.toString());
//                            hm.put("Category", categoryField.getText().toString());
//                            hm.put("Medium", mediumField.getSelectedItem().toString());
//                            hm.put("NumberOfPages", pagesField.getText().toString());
//                            hm.put("Edition", EditionField.getText().toString());
//                            hm.put("BookTags", BookTagsField.getText().toString());
//                            hm.put("BookAbout", BookAboutField.getText().toString());
//                            hm.put("AboutAuthor", AboutAuthorField.getText().toString());
//                            hm.put("Binding", BindingField.getText().toString());
//                            hm.put("Dimensions", DimensionsField.getText().toString());
//                            hm.put("Weight", WeightField.getText().toString());
//                            hm.put("key", newPushKey);
//                            hm.put("AdditionalBookCode", AdditionalBookCodeField.getText().toString());
//                            hm.put("SaleCurrency", saleCurrency.getSelectedItem().toString());
//                            hm.put("Quantity", QuantityField.getText().toString());
//                            hm.put("Rating", "0");
//                            hm.put("Ranking", "0");
//
//                            uploadDataToFirebase(hm);
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Oops/" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }else {
            progressDialog.dismiss();
            Toast.makeText(this, "Select Image!!", Toast.LENGTH_SHORT).show();
        }
    }

    private void uploadData() {
            if (resultUri != null) {

                final StorageReference filePath = FirebaseStorage.getInstance().getReference().child("book_images").child(resultUri.getLastPathSegment());

                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), resultUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                try {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 20, baos);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                byte[] data = baos.toByteArray();
                UploadTask uploadTask = filePath.putBytes(data);

                uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }
                        return filePath.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if (task.isSuccessful()) {
                            final Uri downloadUri = task.getResult();


                            RequestQueue queue = Volley.newRequestQueue(BookDetailEditActivity.this);

                            StringRequest request = new StringRequest(Request.Method.PUT, Constant.BASE_URL+"updateBookDetails/", new com.android.volley.Response.Listener<String>() {

                                @Override
                                public void onResponse(String response)
                                {

                                    progressDialog.dismiss();
                                    showDoneDialog();

                                }
                            },
                                    new com.android.volley.Response.ErrorListener()
                                    {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            progressDialog.dismiss();

                                            Log.d("RESPONSE:          >>> ", error.toString() + "<<<");
                                            Toast.makeText(getApplicationContext(), "Oops/" + error.getMessage(), Toast.LENGTH_SHORT).show();

                                        }
                                    })
                            {
                                @Override
                                protected Map<String, String> getParams()
                                {
                                    Map<String, String> jsonParams = new ArrayMap<>();
                                    jsonParams.put("imgFlag", "1");
                                    jsonParams.put("AboutAuthor",  AboutAuthorField.getText().toString());
                                    jsonParams.put("actualCost",  printedPriceField.getText().toString());
                                    jsonParams.put("AdditionalBookCode", AdditionalBookCodeField.getText().toString());
                                    jsonParams.put("Author", authorField.getText().toString());
                                    jsonParams.put("Binding",  BindingField.getText().toString());
                                    jsonParams.put("BookAbout", BookAboutField.getText().toString());
                                    jsonParams.put("BookName", bookNameField.getText().toString());
                                    jsonParams.put("BookTags",  BookTagsField.getText().toString());
                                    jsonParams.put("Category", categoryField.getText().toString());
                                    jsonParams.put("Descriptions", DescriptionField.getText().toString());
                                    jsonParams.put("Dimensions", DimensionsField.getText().toString());
                                    jsonParams.put("Edition", EditionField.getText().toString());
                                    jsonParams.put("Image", downloadUri.toString());
                                    jsonParams.put("isbn", isbnField.getText().toString());
                                    jsonParams.put("bookKey", newPushKey);
                                    jsonParams.put("MediumType", mediumField.getSelectedItem().toString());
                                    jsonParams.put("MRP", MRPField.getText().toString());
                                    jsonParams.put("NumberOfPages", pagesField.getText().toString());
                                    jsonParams.put("Publisher", PublisherField.getText().toString());
                                    jsonParams.put("Quantity", QuantityField.getText().toString());
                                    jsonParams.put("SaleCurrency", saleCurrency.getSelectedItem().toString());
                                    jsonParams.put("Weight", WeightField.getText().toString());
                                    return jsonParams;
                                }
                                @Override
                                public String getBodyContentType(){
                                    return "application/x-www-form-urlencoded";
                                }
                            };

                            queue.add(request);
//                            HashMap hm = new HashMap<>();
//                            hm.put("isbn", isbnField.getText().toString());
//                            hm.put("Author", authorField.getText().toString());
//                            hm.put("BookName", bookNameField.getText().toString());
//                            hm.put("Publisher", PublisherField.getText().toString());
//                            hm.put("MRP", MRPField.getText().toString());
//                            hm.put("actualCost", printedPriceField.getText().toString());
//                            hm.put("Description", DescriptionField.getText().toString());
//                            hm.put("Image", downloadUri.toString());
//                            hm.put("Category", categoryField.getText().toString());
//                            hm.put("Medium", mediumField.getSelectedItem().toString());
//                            hm.put("NumberOfPages", pagesField.getText().toString());
//                            hm.put("Edition", EditionField.getText().toString());
//                            hm.put("BookTags", BookTagsField.getText().toString());
//                            hm.put("BookAbout", BookAboutField.getText().toString());
//                            hm.put("AboutAuthor", AboutAuthorField.getText().toString());
//                            hm.put("Binding", BindingField.getText().toString());
//                            hm.put("Dimensions", DimensionsField.getText().toString());
//                            hm.put("Weight", WeightField.getText().toString());
//                            hm.put("key", newPushKey);
//                            hm.put("AdditionalBookCode", AdditionalBookCodeField.getText().toString());
//                            hm.put("SaleCurrency", saleCurrency.getSelectedItem().toString());
//                            hm.put("Quantity", QuantityField.getText().toString());
//                            hm.put("Rating", "0");
//                            hm.put("Ranking", "0");
//
//                            uploadDataToFirebase(hm);
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Oops/" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }else {
                progressDialog.dismiss();
                Toast.makeText(this, "Select Image!!", Toast.LENGTH_SHORT).show();
            }

    }

    private void uploadDataToFirebase(HashMap hm) {
        mRef.child(newPushKey).updateChildren(hm).addOnSuccessListener(new OnSuccessListener() {
            @Override
            public void onSuccess(Object o) {
                progressDialog.dismiss();
                showDoneDialog();
            }
        });
    }

    private void showDoneDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Uploaded");
        builder.setCancelable(false);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                finish();
            }
        });
        builder.show();
    }

    int getIndex(String[] test,String txt) {
        List<String> str = Arrays.asList(test);
        return str.indexOf(txt);
    }


    void onProfileImageClick() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        } else {
                            // TODO - handle permission denied case
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(BookDetailEditActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(BookDetailEditActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                resultUri = data.getParcelableExtra("path");
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                    imageField.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
