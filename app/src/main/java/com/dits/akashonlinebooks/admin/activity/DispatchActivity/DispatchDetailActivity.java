package com.dits.akashonlinebooks.admin.activity.DispatchActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.activity.PickingActivity.PickingOrderDetailActivity;
import com.dits.akashonlinebooks.admin.adapter.DispatchOrderDetailAdapter;
import com.dits.akashonlinebooks.admin.adapter.PickingOrderDetailAdapter;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.dits.akashonlinebooks.admin.model.HistoryDetailModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class DispatchDetailActivity extends AppCompatActivity implements DispatchOrderDetailAdapter.CancelOrder{
    ArrayList<HistoryDetailModel> bookDetailList;
    public static String Cost,ActualCost,date,delivered,orderKey,orderref,paymentMethod,userAddress,userId,userName,userPhone,userPostalCode,orderIdUser,deliveryBoyOrderKey,deliveryCharge,referralAmount;
    RecyclerView history_detail;
    LinearLayoutManager linearLayoutManager;
    public static int count = 0;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dispatch_detail);

        Cost = getIntent().getExtras().getString("Cost");
        ActualCost = getIntent().getExtras().getString("ActualCost");
        date = getIntent().getExtras().getString("date");
        delivered = getIntent().getExtras().getString("delivered");
        orderKey = getIntent().getExtras().getString("orderKey");
        orderref = getIntent().getExtras().getString("orderref");
        paymentMethod = getIntent().getExtras().getString("paymentMethod");
        userAddress = getIntent().getExtras().getString("userAddress");
        userId = getIntent().getExtras().getString("userId");
        userName = getIntent().getExtras().getString("userName");
        userPhone = getIntent().getExtras().getString("userPhone");
        userPostalCode = getIntent().getExtras().getString("userPostalCode");
        orderIdUser = getIntent().getExtras().getString("orderIdUser");
        deliveryBoyOrderKey = getIntent().getExtras().getString("deliveryBoyOrderKey");
        deliveryCharge = getIntent().getExtras().getString("deliveryCharge");
        referralAmount = getIntent().getExtras().getString("referralAmount");

        history_detail = findViewById(R.id.history_detail);
        history_detail.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        linearLayoutManager = new LinearLayoutManager(this);
        history_detail.setLayoutManager(linearLayoutManager);
        history_detail.setHasFixedSize(true);
        bookDetailList = new ArrayList<>();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);

        loadHistoryDetails();
    }
    void loadHistoryDetails(){
        Constant.dispatchOrder.child(orderref).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                bookDetailList.clear();
                for(DataSnapshot ds : dataSnapshot.getChildren()){
                    if (ds.hasChild("product_id")){
                        HistoryDetailModel historyDetailModel = new HistoryDetailModel();
                        historyDetailModel.setActualAmount(ds.child("ActualAmount").getValue().toString());
                        historyDetailModel.setActualCost(ds.child("Amount").getValue().toString());
                        historyDetailModel.setAuthor(ds.child("Author").getValue().toString());
                        historyDetailModel.setBookName(ds.child("BookName").getValue().toString());
                        historyDetailModel.setImage(ds.child("Image").getValue().toString());
                        historyDetailModel.setMRP(ds.child("MRP").getValue().toString());
                        historyDetailModel.setPublisher(ds.child("Publisher").getValue().toString());
                        historyDetailModel.setKey(ds.child("key").getValue().toString());
                        historyDetailModel.setActualCost(ds.child("actualCost").getValue().toString());
                        historyDetailModel.setQuantity(ds.child("quantity").getValue().toString());
                        historyDetailModel.setActualCost(ds.child("actualCost").getValue().toString());
                        historyDetailModel.setRated(ds.child("Rated").getValue().toString());
                        historyDetailModel.setHistoryKey(ds.getKey());
                        bookDetailList.add(historyDetailModel);
                        count++;
                    }
                }

                DispatchOrderDetailAdapter historyDetailAdapter = new DispatchOrderDetailAdapter(getApplicationContext(),bookDetailList, DispatchDetailActivity.this);
                history_detail.setAdapter(historyDetailAdapter);
                historyDetailAdapter.setData(bookDetailList);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        count = 0;
    }

    @Override
    protected void onStart() {
        super.onStart();
        count = 0;
    }

    @Override
    public void onClacelOrderClick(final String key) {
        AlertDialog.Builder builder = new AlertDialog.Builder(DispatchDetailActivity.this);
        builder.setMessage("Do you really want to cancel this order?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Constant.dispatchOrder.child(orderref).setValue(null);
                        FirebaseDatabase.getInstance().getReference().child("Orders").child("Jabalpur").child("Orders").
                                child(key).child("delivered").setValue("Cancelled");
                        FirebaseDatabase.getInstance().getReference().child("UserInfo").child("Jabalpur").child(userId).child("history")
                                .child(orderIdUser).child("delivered").setValue("Cancelled");
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }
}
