package com.dits.akashonlinebooks.admin.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.adapter.HomeMenuAdapter;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.dits.akashonlinebooks.admin.model.HomeMenuModel;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class UserFormActivity extends AppCompatActivity implements HomeMenuAdapter.ItemListener{
    RecyclerView recyclerView;
    ArrayList arrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_form);

        recyclerView = findViewById(R.id.recyclerView);
        arrayList = new ArrayList();


        Constant.subcategoryRef = FirebaseDatabase.getInstance().getReference().child("Course");
        Constant.questionRef = FirebaseDatabase.getInstance().getReference().child("Questions");
        arrayList.add(new HomeMenuModel("School", R.drawable.diagram, "#ff000000"));
        arrayList.add(new HomeMenuModel("College", R.drawable.diagram, "#ff000000"));
        arrayList.add(new HomeMenuModel("Coaching", R.drawable.diagram, "#ff000000"));
        arrayList.add(new HomeMenuModel("Course", R.drawable.diagram, "#ff000000"));
        arrayList.add(new HomeMenuModel("Category", R.drawable.diagram, "#ff000000"));
        arrayList.add(new HomeMenuModel("Authors", R.drawable.diagram, "#ff000000"));
        arrayList.add(new HomeMenuModel("Publishers", R.drawable.diagram, "#ff000000"));


        HomeMenuAdapter adapter = new HomeMenuAdapter(this, arrayList, this);
        recyclerView.setAdapter(adapter);

        @SuppressLint("WrongConstant") GridLayoutManager manager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setNestedScrollingEnabled(false);
    }

    @Override
    public void onItemClick(HomeMenuModel item) {
        if (item.text.equals("School")){
            Intent i = new Intent(UserFormActivity.this,CategoryActivity.class);
            i.putExtra("ItemName","School");
            startActivity(i);
        }else if (item.text.equals("College")){
            Intent i = new Intent(UserFormActivity.this,CategoryActivity.class);
            i.putExtra("ItemName","College");
            startActivity(i);
        }else if (item.text.equals("Coaching")){
            Intent i = new Intent(UserFormActivity.this,CategoryActivity.class);
            i.putExtra("ItemName","Coaching");
            startActivity(i);

        }
        else if (item.text.equals("Category")){
            Intent i = new Intent(UserFormActivity.this,CategoryActivity.class);
            i.putExtra("ItemName","Category");
            startActivity(i);

        }else if (item.text.equals("Authors")){
            Intent i = new Intent(UserFormActivity.this,CategoryActivity.class);
            i.putExtra("ItemName","Authors");
            startActivity(i);

        }else if (item.text.equals("Publishers")){
            Intent i = new Intent(UserFormActivity.this,CategoryActivity.class);
            i.putExtra("ItemName","Publishers");
            startActivity(i);

        }else if (item.text.equals("Course")){
            Intent i = new Intent(UserFormActivity.this,CategoryActivity.class);
            i.putExtra("ItemName","Course");
            startActivity(i);

        }

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
