package com.dits.akashonlinebooks.admin.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.dits.akashonlinebooks.admin.model.DeliveryBoyModel;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.HashMap;

public class RegisterDeliveryBoy extends AppCompatActivity {
    FloatingActionButton addFab;
    ProgressDialog progressDialog;
    RecyclerView itemList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_delivery);

        addFab = findViewById(R.id.add_fab_btn);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        addFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddItemDialog();
            }
        });


        itemList = findViewById(R.id.itemList);
        itemList.setHasFixedSize(true);
        itemList.setLayoutManager(new LinearLayoutManager(this));
    }

    private void showAddItemDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterDeliveryBoy.this);
        builder.setTitle("Add Delivery Boy");
        LayoutInflater inflater = LayoutInflater.from(RegisterDeliveryBoy.this);
        View editDialog = inflater.inflate(R.layout.add_delivery_boy_layout2,null,false);
        final TextInputEditText copyTypeEt = editDialog.findViewById(R.id.school_name);
        final TextInputEditText pageCountEt = editDialog.findViewById(R.id.schoolAddress);
        final TextInputEditText copyCostEt = editDialog.findViewById(R.id.school_phone);
        final CheckBox isCopyAvailable = editDialog.findViewById(R.id.new_set_available);
        Button deleteBtn = editDialog.findViewById(R.id.delete_btn);
        deleteBtn.setVisibility(View.GONE);
        isCopyAvailable.setChecked(true);
        builder.setCancelable(false);
        builder.setView(editDialog);
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!TextUtils.isEmpty(copyTypeEt.getText().toString()) && !TextUtils.isEmpty(pageCountEt.getText().toString()) &&
                        !TextUtils.isEmpty(copyCostEt.getText().toString())){
                    progressDialog.show();
                    HashMap hm = new HashMap();
                    hm.put("SchoolName",copyTypeEt.getText().toString());
                    hm.put("Address",pageCountEt.getText().toString());
                    hm.put("Phone",copyCostEt.getText().toString());
                    if (isCopyAvailable.isChecked()) {
                        hm.put("SetAvailable","true");
                    }
                    else {
                        hm.put("SetAvailable","false");
                    }

                    String key =  Constant.deliveryRef.push().getKey();
                    Constant.deliveryRef.child(key).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            if (task.isSuccessful()){
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "School Info Edited!!", Toast.LENGTH_SHORT).show();
                            }
                            else {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                else {
                    Toast.makeText(getApplicationContext(), "Please Fill All Fields...", Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.show();
    }


    @Override
    public void onStart() {
        super.onStart();
        refreshBookRecyclerView();
    }

    private void refreshBookRecyclerView() {
        FirebaseRecyclerOptions<DeliveryBoyModel> options =
                new FirebaseRecyclerOptions.Builder<DeliveryBoyModel>()
                        .setQuery(Constant.deliveryRef.orderByChild("deliveryBoyModel"), DeliveryBoyModel.class)
                        .build();

        FirebaseRecyclerAdapter adapter = new FirebaseRecyclerAdapter<DeliveryBoyModel, ItemViewHolder>(options) {
            @Override
            public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.delivery_boy_list_single_layout , parent, false);

                return new ItemViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(ItemViewHolder holder, final int position, final DeliveryBoyModel model) {
                holder.setSchoolName(model.getDeliveryBoyModel());
                holder.setPhone(model.getPhone());
                holder.setAlphabetImage(model.getDeliveryBoyModel().substring(0, 1));
                final String id =getRef(position).getKey();


                holder.mView.findViewById(R.id.menu_btn).setVisibility(View.VISIBLE);
                holder.mView.findViewById(R.id.menu_btn).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Constant.admin){
                            showEditSchoolInfoDialog(id,model.getDeliveryBoyModel(),model.getAdminType(),model.getPhone(),model.getSetAvailable());

                        }
                    }
                });

            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

            }
        };
        adapter.startListening();
        itemList.setAdapter(adapter);
    }


    private void showEditSchoolInfoDialog(final String schoolId, final String school_name, String schooladdress, String schoolPhone, String setAvailavle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterDeliveryBoy.this);
        builder.setTitle("Edit Delivery Boy Details");
        LayoutInflater inflater = LayoutInflater.from(RegisterDeliveryBoy.this);
        View editDialog = inflater.inflate(R.layout.add_delivery_boy_layout2,null,false);
        final TextInputEditText copyTypeEt = editDialog.findViewById(R.id.school_name);
        final TextInputEditText pageCountEt = editDialog.findViewById(R.id.schoolAddress);
        final TextInputEditText copyCostEt = editDialog.findViewById(R.id.school_phone);
        final CheckBox isCopyAvailable = editDialog.findViewById(R.id.new_set_available);
        Button deleteBtn = editDialog.findViewById(R.id.delete_btn);
        copyTypeEt.setText(school_name);
        pageCountEt.setText(schooladdress);
        copyCostEt.setText(schoolPhone);

        if (setAvailavle.equals("true")){
            isCopyAvailable.setChecked(true);
        }else {
            isCopyAvailable.setChecked(false);
        }

        builder.setCancelable(false);
        builder.setView(editDialog);
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!TextUtils.isEmpty(copyTypeEt.getText().toString()) && !TextUtils.isEmpty(pageCountEt.getText().toString()) &&
                        !TextUtils.isEmpty(copyCostEt.getText().toString())){
                    progressDialog.show();
                    HashMap hm = new HashMap();
                    hm.put("SchoolName",copyTypeEt.getText().toString());
                    hm.put("Address",pageCountEt.getText().toString());
                    hm.put("Phone",copyCostEt.getText().toString());
                    if (isCopyAvailable.isChecked()) {
                        hm.put("SetAvailable","true");
                    }
                    else {
                        hm.put("SetAvailable","false");
                    }
                    Constant.deliveryRef.child(schoolId).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            if (task.isSuccessful()){
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "School Info Edited!!", Toast.LENGTH_SHORT).show();
                            }
                            else {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                else {
                    Toast.makeText(getApplicationContext(), "Please Fill All Fields...", Toast.LENGTH_SHORT).show();
                }
            }
        });

        final AlertDialog alertDialog = builder.create();

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constant.deliveryRef.child(schoolId).setValue(null);
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{
        View mView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mView=itemView;
        }
        public void setSchoolName(String name)
        {
            TextView userNameView= mView.findViewById(R.id.school_name);
            userNameView.setText(name);

        }
        public void setPhone(String phone)
        {
            TextView userNumberView= mView.findViewById(R.id.phone_number);
            userNumberView.setText(phone);
        }

        public void setAlphabetImage(String s){
            TextDrawable drawable = TextDrawable.builder()
                    .buildRound(s, randomColor());
            ImageView image = mView.findViewById(R.id.image_view);
            image.setImageDrawable(drawable);
        }
        public int randomColor() {

            int r = (int) (0xff * Math.random());
            int g = (int) (0xff * Math.random());
            int b = (int) (0xff * Math.random());

            return Color.rgb( r, g, b);
        }

    }
}
