package com.dits.akashonlinebooks.admin.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.dits.akashonlinebooks.admin.model.RequestListModel;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;

public class RequestDetails extends AppCompatActivity {
    RecyclerView itemList;
    ProgressDialog progressDialog;
    LinearLayoutManager linearLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_details);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);

        itemList = findViewById(R.id.itemList);
        itemList.setHasFixedSize(true);
        itemList.setLayoutManager(linearLayoutManager);

    }

    @Override
    protected void onStart() {
        super.onStart();
        refreshRequestRecyclerView();
    }

    private void refreshRequestRecyclerView()
    {
        FirebaseRecyclerOptions<RequestListModel> options =
                new FirebaseRecyclerOptions.Builder<RequestListModel>()
                        .setQuery(Constant.requestBookRef, RequestListModel.class)
                        .build();

        FirebaseRecyclerAdapter adapter = new FirebaseRecyclerAdapter<RequestListModel, ItemViewHolder>(options) {
            @Override
            public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.single_request_detail_layout, parent, false);

                return new ItemViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(ItemViewHolder holder, final int position, final RequestListModel model) {
                holder.setBookName(model.getBookName());
                holder.setPublisherName(model.getPublisherName());
                holder.setDescription(model.getDescription());
                holder.setName(model.getName());
                holder.setNumber(model.getPhone());
                holder.setDate(model.getDate());
                holder.setImage(model.getImage(),getApplicationContext());
                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(RequestDetails.this,RequestSearchActivity.class);
                        i.putExtra("uid",model.getUid());
                        i.putExtra("token",model.getToken());
                        startActivity(i);
                    }
                });

                holder.mView.findViewById(R.id.completeBtn).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showCompleteDialog(getRef(position).getKey(),model.getName(),model.getPhone(),model.getDescription(),model.getUid());
                    }
                });
            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

            }
        };
        adapter.startListening();
        itemList.setAdapter(adapter);
    }

    private void showCompleteDialog(final String key, final String name, final String phone, final String bookName, final String uid) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you completed this request?");
        builder.setCancelable(false);
        builder.setPositiveButton("Complete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                sendMessage(key,name,phone,bookName);
                setUserData(uid,key);
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    private void setUserData(String uid,String key) {
        Constant.rootRef.child("UserInfo").child("Jabalpur").child(uid).child("Requests").child(key).child("isCompleted").setValue("Completed");
    }

    private void sendMessage(final String key, String name, String phone, String bookName) {
        String url = "https://api.msg91.com/api/sendhttp.php?country=91&sender=AOBJBP&route=4&mobiles="+ phone +"&authkey=106940A18cEbcPcZ75c503f65&message="+
                "Dear "+name+","+" Your book request has completed.We have added your books in your wishlist. You can buy it from Akash Online Books app";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("----------->",response);
                deleteRequest(key);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), String.valueOf(error), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(request);
    }

    private void deleteRequest(String key) {
        Constant.requestBookRef.child(key).setValue(null);
    }

    private static class ItemViewHolder extends RecyclerView.ViewHolder
    {
        View mView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mView=itemView;
        }

        public void setBookName(String item) {
            TextView book_name = mView.findViewById(R.id.itemName);
            book_name.setText("Product Name : "+item);

        }

        public void setPublisherName(String item) {
            TextView book_name = mView.findViewById(R.id.publisherName);
            book_name.setText("Brand Name : "+item);

        }

        public void setDescription(String item) {
            TextView book_name = mView.findViewById(R.id.description);
            book_name.setText("Description : "+item);

        }

        public void setImage(String item, Context ctx) {
            ImageView itemImage = mView.findViewById(R.id.image);
            if (!item.equals("null")){
                Glide.with(ctx).load(item).apply(new RequestOptions().placeholder(R.drawable.no_image).error(R.drawable.no_image)).into(itemImage);
                itemImage.setVisibility(View.VISIBLE);
            }else {
                itemImage.setVisibility(View.GONE);
            }



        }

        public void setName(String item) {
            TextView book_name = mView.findViewById(R.id.userName);
            book_name.setText("User Name : "+item);
        }

        public void setNumber(String item) {
            TextView book_name = mView.findViewById(R.id.userNumber);
            book_name.setText("User Number : "+item);
        }

        public void setDate(String item) {
            TextView book_name = mView.findViewById(R.id.date);
            book_name.setText("Date : "+item);
        }

    }

}
