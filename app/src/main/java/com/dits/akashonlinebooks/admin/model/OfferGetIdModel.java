package com.dits.akashonlinebooks.admin.model;

public class OfferGetIdModel {
    String category,id;

    public OfferGetIdModel() {
    }

    public OfferGetIdModel(String category, String id) {
        this.category = category;
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
