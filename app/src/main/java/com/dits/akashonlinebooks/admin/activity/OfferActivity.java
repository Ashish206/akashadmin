package com.dits.akashonlinebooks.admin.activity;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.dits.akashonlinebooks.admin.fragments.OfferZoneFragment;
import com.dits.akashonlinebooks.admin.R;

import static com.dits.akashonlinebooks.admin.activity.CategoryActivity.fragmentManager;

public class OfferActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer);

        Fragment categoryFragment = new OfferZoneFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main_frame,categoryFragment)
                .commit();


    }
}
