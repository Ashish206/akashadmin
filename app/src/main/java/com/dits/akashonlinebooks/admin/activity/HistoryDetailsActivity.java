package com.dits.akashonlinebooks.admin.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Picture;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.activity.PackingActivity.PackingDetailActivity;
import com.dits.akashonlinebooks.admin.adapter.HistoryDetailAdapter;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.dits.akashonlinebooks.admin.model.HistoryDetailModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import static android.os.Environment.DIRECTORY_PICTURES;

public class HistoryDetailsActivity extends AppCompatActivity  implements HistoryDetailAdapter.CancelOrder,HistoryDetailAdapter.GenerateInvoice,HistoryDetailAdapter.GenerateDispatchSlip,HistoryDetailAdapter.SendInvoiceByMail,HistoryDetailAdapter.SendFeedbackMessage {
    ArrayList<HistoryDetailModel> bookDetailList;
    public static String Cost, ActualCost, date, delivered, orderKey, orderref, paymentMethod, userAddress, userId, userName, userPhone, userPostalCode, orderIdUser, deliveryCharge, referralAmount, pickedBy, packedBy, deliveredBy, pickedDateTime, packedDateTime, deliveredDateTime,userEmail, docketNo,couriorName;
    RecyclerView history_detail;
    LinearLayoutManager linearLayoutManager;
    public static int count = 0;
    ProgressDialog progressDialog;
    String base64String = null;
    float avg;
    WebView invoiceWebViewSave;
    Base64 bs=null;
    WebView dispatchWebView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_details);

        Cost = getIntent().getExtras().getString("Cost");
        ActualCost = getIntent().getExtras().getString("ActualCost");
        date = getIntent().getExtras().getString("date");
        delivered = getIntent().getExtras().getString("delivered");
        orderKey = getIntent().getExtras().getString("orderKey");
        orderref = getIntent().getExtras().getString("orderref");
        paymentMethod = getIntent().getExtras().getString("paymentMethod");
        userAddress = getIntent().getExtras().getString("userAddress");
        userId = getIntent().getExtras().getString("userId");
        userName = getIntent().getExtras().getString("userName");
        userPhone = getIntent().getExtras().getString("userPhone");
        userPostalCode = getIntent().getExtras().getString("userPostalCode");
        orderIdUser = getIntent().getExtras().getString("orderIdUser");
        deliveryCharge = getIntent().getExtras().getString("deliveryCharge");
        referralAmount = getIntent().getExtras().getString("referralAmount");
        pickedBy = getIntent().getExtras().getString("pickedBy");
        packedBy = getIntent().getExtras().getString("packedBy");
        deliveredBy = getIntent().getExtras().getString("deliveredBy");

        pickedDateTime = getIntent().getExtras().getString("pickedDateTime");
        packedDateTime = getIntent().getExtras().getString("packedDateTime");
        deliveredDateTime = getIntent().getExtras().getString("deliveredDateTime");
        userEmail = getIntent().getExtras().getString("userEmail");

        history_detail = findViewById(R.id.history_detail);
        history_detail.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        linearLayoutManager = new LinearLayoutManager(this);
        history_detail.setLayoutManager(linearLayoutManager);
        history_detail.setHasFixedSize(true);
        bookDetailList = new ArrayList<>();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);

        dispatchWebView = findViewById(R.id.myWebView);
        dispatchWebView.getSettings().setDomStorageEnabled(true);
        dispatchWebView.getSettings().setAppCacheEnabled(true);
        dispatchWebView.getSettings().setLoadsImagesAutomatically(true);
        dispatchWebView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        dispatchWebView.getSettings().setJavaScriptEnabled(true);
        dispatchWebView.loadUrl(Constant.BASE_URL + "getDispatchSlipSql/" + orderKey);


        loadinvoice();
        getCourierDetails();
    }

    void loadHistoryDetails() {
        Constant.orderref.child(orderref).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                bookDetailList.clear();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    if (ds.hasChild("product_id")) {
                        HistoryDetailModel historyDetailModel = new HistoryDetailModel();
                        historyDetailModel.setActualAmount(ds.child("ActualAmount").getValue().toString());
                        historyDetailModel.setActualCost(ds.child("Amount").getValue().toString());
                        historyDetailModel.setAuthor(ds.child("Author").getValue().toString());
                        historyDetailModel.setBookName(ds.child("BookName").getValue().toString());
                        historyDetailModel.setImage(ds.child("Image").getValue().toString());
                        historyDetailModel.setMRP(ds.child("MRP").getValue().toString());
                        historyDetailModel.setPublisher(ds.child("Publisher").getValue().toString());
                        historyDetailModel.setKey(ds.child("key").getValue().toString());
                        historyDetailModel.setActualCost(ds.child("actualCost").getValue().toString());
                        historyDetailModel.setQuantity(ds.child("quantity").getValue().toString());
                        historyDetailModel.setActualCost(ds.child("actualCost").getValue().toString());
                        historyDetailModel.setRated(ds.child("Rated").getValue().toString());
                        historyDetailModel.setHistoryKey(ds.getKey());
                        bookDetailList.add(historyDetailModel);
                        count++;
                    }
                }

                HistoryDetailAdapter historyDetailAdapter = new HistoryDetailAdapter(getApplicationContext(), bookDetailList, HistoryDetailsActivity.this, HistoryDetailsActivity.this, HistoryDetailsActivity.this, HistoryDetailsActivity.this);
                history_detail.setAdapter(historyDetailAdapter);
                historyDetailAdapter.setData(bookDetailList);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        progressDialog.dismiss();
        count = 0;
    }

    @Override
    protected void onStart() {
        super.onStart();
        count = 0;
    }

    @Override
    public void onClacelOrderClick(final String key) {
        AlertDialog.Builder builder = new AlertDialog.Builder(HistoryDetailsActivity.this);
        builder.setMessage("Do you really want to cancel this order?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        //Constant.orderref.child(HistoryDetailsActivity.orderref).child("delivered").setValue("Cancelled");
                        FirebaseDatabase.getInstance().getReference().child("Orders").child("Jabalpur").child("Orders").
                                child(key).child("delivered").setValue("Cancelled");
                        FirebaseDatabase.getInstance().getReference().child("UserInfo").child("Jabalpur").child(userId).child("history")
                                .child(orderIdUser).child("delivered").setValue("Cancelled");
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onGenerateInvoiceClick(String key) {
        WebView invoiceWebView;
        invoiceWebView = findViewById(R.id.myWebView22);
        invoiceWebView.loadUrl(Constant.BASE_URL + "getInvoice/" + key);
        invoiceWebView.getSettings().setJavaScriptEnabled(true);
        invoiceWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageCommitVisible(WebView view, String url) {
                // progressDialog.dismiss();
            }
        });
        createWebPrintJob(invoiceWebView);
    }

    private void createWebPrintJob(WebView webView) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            PrintManager printManager = (PrintManager) this
                    .getSystemService(Context.PRINT_SERVICE);

            PrintDocumentAdapter printAdapter =
                    null;
            printAdapter = webView.createPrintDocumentAdapter("MyDocument");
            String jobName = getString(R.string.app_name) + " Print Test";

            printManager.print(jobName, printAdapter,
                    new PrintAttributes.Builder().build());
        } else {
            Toast.makeText(HistoryDetailsActivity.this, "Print job has been canceled! ", Toast.LENGTH_SHORT).show();
        }

    }

    private void getCourierDetails(){
        docketNo = "";
        couriorName = "";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest request = new StringRequest(Request.Method.GET, Constant.BASE_URL+"getCourierDetails?orderId="+orderKey, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject job = new JSONArray(response).getJSONObject(0);
                    docketNo = job.optString("docketNo");
                    couriorName = job.optString("courierName");
                    loadHistoryDetails();
                } catch (JSONException e) {
                    e.printStackTrace();
                    loadHistoryDetails();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                loadHistoryDetails();
            }
        });
        requestQueue.add(request);
    }

    @SuppressLint("JavascriptInterface")
    private void loadinvoice()
    {
        progressDialog.show();
        WebView invoiceWebView;
        invoiceWebView = findViewById(R.id.myWebView22);
        invoiceWebView.getSettings().setJavaScriptEnabled(true);
        String newUA= invoiceWebView.getSettings().getUserAgentString().replace("Mobile", "eliboM").replace("Android", "diordnA");
        invoiceWebView.getSettings().setUserAgentString(newUA);
        invoiceWebView.getSettings().setUseWideViewPort(true);
        invoiceWebView.getSettings().setLoadWithOverviewMode(true);
        invoiceWebView.getSettings().setSupportZoom(true);
        invoiceWebView.getSettings().setBuiltInZoomControls(true);
        invoiceWebView.loadUrl(Constant.BASE_URL + "getInvoice/" + orderKey);
        invoiceWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                if(consoleMessage.message().length() > 10000)
                {
                    base64String = consoleMessage.message();
                    android.util.Log.d("WebView", "------------->"+base64String.length());
                    progressDialog.dismiss();
                }
                progressDialog.dismiss();
                return true;
            }
        });

    }

    @Override
    public void onGenerateDispatchSlipClick(String key) {


        dispatchWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                Log.d("", "-------------********------: ");
                String s = consoleMessage.message();
                return true;
            }
        });
        dispatchWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageCommitVisible(WebView view, String url) {
                progressDialog.dismiss();
            }
        });
        createWebPrintJob(dispatchWebView);
    }

    @Override
    public void onSendInvoiceByMailClick(String key) {

        if (base64String != null) {
            progressDialog.show();
            byte[] decodedString = Base64.decode(base64String, Base64.DEFAULT);
          try{
              Bitmap invoiceImage = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
              String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/Invoice");
            myDir.mkdirs();
            String fname = key + ".jpg";
            File file = new File(myDir, fname);
            Log.i("------>", "" + file);
            if (file.exists())
                file.delete();
            try {
                FileOutputStream out = new FileOutputStream(file);
                invoiceImage.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();
              //------gen pdf---------
                createPdf(key);

            } catch (Exception e) {
                progressDialog.dismiss();
                e.printStackTrace();
            }
          }
          catch (Exception e)
          {
              Log.d("", "onSendInvoiceByMailClick: "+e);
              progressDialog.dismiss();
          }
//

            // saveImage(key, invoiceImage);
        } else {
            Toast.makeText(this, "Generating Invoice in The Background Please wait for some time", Toast.LENGTH_SHORT).show();
            Log.d("----->", "" + base64String);
        }
    }

    private  void createPdf(String key)
    {
        try {
            Document document = new Document();

            String directoryPath =  Environment.getExternalStorageDirectory().toString() + "/Invoice/";

            PdfWriter.getInstance(document, new FileOutputStream(directoryPath +key+".pdf")); //  Change pdf's name.

            document.open();

            Image image = Image.getInstance(directoryPath  + key+".jpg");  // Change image's name and extension.

            float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
                    - document.rightMargin() - 0) / image.getWidth()) * 100; // 0 means you have no indentation. If you have any, change it.
            image.scalePercent(scaler);
            image.setAlignment(Image.ALIGN_CENTER | Image.ALIGN_TOP);

            document.add(image);
            document.close();
            progressDialog.dismiss();
            //opening Gmail Intent-------------------
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
                Intent in = new Intent(Intent.ACTION_SEND);
                    in.setType("application/pdf");
                    in.putExtra(Intent.EXTRA_EMAIL  , new String[]{userEmail});
                    in.putExtra(Intent.EXTRA_SUBJECT, "Invoice for OrderId: "+ key.substring(key.length()-8).toUpperCase());
                    in.putExtra(Intent.EXTRA_TEXT   , "Dear Valued Customer,\n\n The Invoice has been attached with the mail. \n \n \n Thanks And Regards \n Akash Pustak Sadan \n Super Market, Malviya Chowk \n Jabalpur(482002)\n Email:akashonlinebooks@gmail.com \n Ph:07612403099");
                    in.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+directoryPath+key+".pdf"));
                    in.setPackage("com.google.android.gm");

                    try {
                        startActivity(Intent.createChooser(in, "Send mail..."));
                    } catch (android.content.ActivityNotFoundException ex) {
                        progressDialog.dismiss();
                        Toast.makeText(HistoryDetailsActivity.this, "There are no email clients installed.",Toast.LENGTH_SHORT).show();
                    }


        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onSendInterfaceClick() {
        sendSms("Thank you for using Akash Online Books. Please rate us on ");
    }

    private void sendSms(String smstext) {
        String url = "https://api.msg91.com/api/sendhttp.php?country=91&sender=AOBJBP&route=4&mobiles="+userPhone+"&authkey=106940A18cEbcPcZ75c503f65&message="+smstext+
                "https://play.google.com/store/apps/details?id=com.dits.akashonlinebooks %0a%0aRegards,%0aAkash Online Books";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(HistoryDetailsActivity.this, "Sent", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), String.valueOf(error), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(request);

    }

}

