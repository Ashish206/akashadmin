package com.dits.akashonlinebooks.admin.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.dits.akashonlinebooks.admin.model.DeliveryBoyModel;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class RegisterAdminGroup extends AppCompatActivity {

    FloatingActionButton addFab;
    ProgressDialog progressDialog;
    RecyclerView itemList;
    String adminType[] = {"Main","Picking","Packing","Dispatch"};
    String homeOption[] = {"Home Screen","Notification","Order Details","Book Details","Statistics",
            "Admin Group","Book Requests","Offer Zone","Extra","Picked Orders","Pack Orders","Undelivered Packed Orders","Order Delivery","Non Image Books","FeedBack","Web Panel"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_delivery_boy);

        addFab = findViewById(R.id.add_fab_btn);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        addFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddItemDialog();
            }
        });


        itemList = findViewById(R.id.itemList);
        itemList.setHasFixedSize(true);
        itemList.setLayoutManager(new LinearLayoutManager(this));

    }

    private void showAddItemDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterAdminGroup.this);
        builder.setTitle("Add Admin");
        LayoutInflater inflater = LayoutInflater.from(RegisterAdminGroup.this);
        View editDialog = inflater.inflate(R.layout.add_delivery_boy_layout,null,false);
        final TextInputEditText copyTypeEt = editDialog.findViewById(R.id.school_name);
        final Spinner pageCountEt = editDialog.findViewById(R.id.adminType);
        final TextInputEditText copyCostEt = editDialog.findViewById(R.id.school_phone);
        final CheckBox isCopyAvailable = editDialog.findViewById(R.id.new_set_available);
        Button deleteBtn = editDialog.findViewById(R.id.delete_btn);

        final GridView gridView = editDialog.findViewById(R.id.grid_cb);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_multiple_choice, homeOption) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                    if (convertView == null || !(convertView instanceof CheckBox)) {
                        CheckBox checkBox = new CheckBox(getContext());
                        convertView = checkBox;
                    }
                    ((CheckBox)convertView).setText(getItem(position));
                    return convertView;

            }
        };

        gridView.setAdapter(adapter);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, adminType);
        pageCountEt.setAdapter(arrayAdapter);

        deleteBtn.setVisibility(View.GONE);
        isCopyAvailable.setChecked(true);
        builder.setCancelable(false);
        builder.setView(editDialog);
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!TextUtils.isEmpty(copyTypeEt.getText().toString()) &&
                        !TextUtils.isEmpty(copyCostEt.getText().toString())){
                    progressDialog.show();
                    HashMap hm = new HashMap();
                    hm.put("deliveryBoyModel",copyTypeEt.getText().toString());
                    hm.put("AdminType",pageCountEt.getSelectedItem().toString());
                    hm.put("Phone",copyCostEt.getText().toString());
                    hm.put("permission",checkGridView(gridView));
                    if (isCopyAvailable.isChecked()) {
                        hm.put("SetAvailable","true");
                    }
                    else {
                        hm.put("SetAvailable","false");
                    }


                    String key =  Constant.deliveryBoyRegistrationRef.push().getKey();
                    Constant.deliveryBoyRegistrationRef.child(key).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            if (task.isSuccessful()){
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Saved!!", Toast.LENGTH_SHORT).show();
                            }
                            else {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                else {
                    Toast.makeText(getApplicationContext(), "Please Fill All Fields...", Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.show();
    }

    private String checkGridView(GridView gridView) {
        String s = "";
        for (int i = 0; i < gridView.getChildCount() ; i++ ){
            View v = gridView.getChildAt(i);
            CheckBox checkBox = (CheckBox) v;
            if (checkBox.isChecked()){
                s+=" "+checkBox.getText().toString();
            }
        }
        return s;
    }


    @Override
    public void onStart() {
        super.onStart();
        refreshBookRecyclerView();
    }

    private void refreshBookRecyclerView() {
        FirebaseRecyclerOptions<DeliveryBoyModel> options =
                new FirebaseRecyclerOptions.Builder<DeliveryBoyModel>()
                        .setQuery(Constant.deliveryBoyRegistrationRef.orderByChild("deliveryBoyModel"), DeliveryBoyModel.class)
                        .build();

        FirebaseRecyclerAdapter adapter = new FirebaseRecyclerAdapter<DeliveryBoyModel, ItemViewHolder>(options) {
            @Override
            public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.delivery_boy_list_single_layout , parent, false);

                return new ItemViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(ItemViewHolder holder, final int position, final DeliveryBoyModel model) {
                holder.setSchoolName(model.getDeliveryBoyModel());
                holder.setPhone(model.getPhone());
                holder.setAlphabetImage(model.getDeliveryBoyModel().substring(0, 1));
                final String id =getRef(position).getKey();

                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        Intent i = new Intent(RegisterAdminGroup.this, DeliveryBoyHistory.class);
//                        i.putExtra("Key",getRef(position).getKey());
//                        startActivity(i);
                    }
                });

                holder.mView.findViewById(R.id.menu_btn).setVisibility(View.VISIBLE);
                holder.mView.findViewById(R.id.menu_btn).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                            showEditSchoolInfoDialog(id,model.getDeliveryBoyModel(),model.getAdminType(),model.getPhone(),model.getSetAvailable());


                    }
                });

            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

            }
        };
        adapter.startListening();
        itemList.setAdapter(adapter);
    }


    private void showEditSchoolInfoDialog(final String schoolId, final String school_name, String schooladdress, String schoolPhone, String setAvailavle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterAdminGroup.this);
        builder.setTitle("Edit Delivery Boy Details");
        LayoutInflater inflater = LayoutInflater.from(RegisterAdminGroup.this);
        View editDialog = inflater.inflate(R.layout.add_delivery_boy_layout,null,false);
        final TextInputEditText copyTypeEt = editDialog.findViewById(R.id.school_name);
        final Spinner pageCountEt = editDialog.findViewById(R.id.adminType);
        final TextInputEditText copyCostEt = editDialog.findViewById(R.id.school_phone);
        final CheckBox isCopyAvailable = editDialog.findViewById(R.id.new_set_available);
        Button deleteBtn = editDialog.findViewById(R.id.delete_btn);
        copyTypeEt.setText(school_name);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, adminType);
        pageCountEt.setAdapter(arrayAdapter);

        copyCostEt.setText(schoolPhone);
        pageCountEt.setSelection(getIndex(adminType,schooladdress));

        final GridView gridView = editDialog.findViewById(R.id.grid_cb);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_multiple_choice, homeOption) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                if (convertView == null || !(convertView instanceof CheckBox)) {
                    CheckBox checkBox = new CheckBox(getContext());
                    convertView = checkBox;
                }
                ((CheckBox)convertView).setText(getItem(position));
                return convertView;

            }
        };

        gridView.setAdapter(adapter);

        if (setAvailavle.equals("true")){
            isCopyAvailable.setChecked(true);
        }else {
            isCopyAvailable.setChecked(false);
        }

        builder.setCancelable(false);
        builder.setView(editDialog);
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!TextUtils.isEmpty(copyTypeEt.getText().toString()) &&
                        !TextUtils.isEmpty(copyCostEt.getText().toString())){
                    progressDialog.show();
                    HashMap hm = new HashMap();
                    hm.put("deliveryBoyModel",copyTypeEt.getText().toString());
                    hm.put("AdminType",pageCountEt.getSelectedItem().toString());
                    hm.put("Phone",copyCostEt.getText().toString());
                    hm.put("permission",checkGridView(gridView));
                    if (isCopyAvailable.isChecked()) {
                        hm.put("SetAvailable","true");
                    }
                    else {
                        hm.put("SetAvailable","false");
                    }
                    Constant.deliveryBoyRegistrationRef.child(schoolId).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            if (task.isSuccessful()){
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Edited!!", Toast.LENGTH_SHORT).show();
                            }
                            else {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                else {
                    Toast.makeText(getApplicationContext(), "Please Fill All Fields...", Toast.LENGTH_SHORT).show();
                }
            }
        });

        final AlertDialog alertDialog = builder.create();

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constant.deliveryBoyRegistrationRef.child(schoolId).setValue(null);
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{
        View mView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mView=itemView;
        }
        public void setSchoolName(String name)
        {
            TextView userNameView= mView.findViewById(R.id.school_name);
            userNameView.setText(name);

        }
        public void setPhone(String phone)
        {
            TextView userNumberView= mView.findViewById(R.id.phone_number);
            userNumberView.setText(phone);
        }

        public void setAlphabetImage(String s){
            TextDrawable drawable = TextDrawable.builder()
                    .buildRound(s, randomColor());
            ImageView image = mView.findViewById(R.id.image_view);
            image.setImageDrawable(drawable);
        }
        public int randomColor() {

            int r = (int) (0xff * Math.random());
            int g = (int) (0xff * Math.random());
            int b = (int) (0xff * Math.random());

            return Color.rgb( r, g, b);
        }

    }

    int getIndex(String[] test,String txt) {
        List<String> str = Arrays.asList(test);
        return str.indexOf(txt);
    }
}

