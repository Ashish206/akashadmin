package com.dits.akashonlinebooks.admin.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.dits.akashonlinebooks.admin.model.FeedbackModel;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.HashMap;

public class FeedBackActivity extends AppCompatActivity {
    ProgressDialog progressDialog;
    private Uri resultUri;
    private ImageView profileImage;
    RecyclerView itemList;
    TextView notDataText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        notDataText = findViewById(R.id.not_data_text);


        itemList = findViewById(R.id.itemList);
        itemList.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        itemList.setLayoutManager(linearLayoutManager);
        loadQna();
    }

    private void loadQna() {
        FirebaseRecyclerOptions<FeedbackModel> options =
                new FirebaseRecyclerOptions.Builder<FeedbackModel>()
                        .setQuery(Constant.rootRef.child("FeedBack"), FeedbackModel.class)
                        .build();

        FirebaseRecyclerAdapter adapter3 = new FirebaseRecyclerAdapter<FeedbackModel, QnAViewHolder>(options) {
            @Override
            public QnAViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.single_feedback_custom_layout, parent, false);

                return new QnAViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(final QnAViewHolder holder, final int position, final FeedbackModel model) {

                holder.setQuestion(model.getName());
                holder.setAnswer(model.getFeedback());
                holder.setButton(model.getDate());
            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                if (getItemCount()!=0){
                    notDataText.setVisibility(View.GONE);

                }else {
                    notDataText.setVisibility(View.VISIBLE);
                }
                progressDialog.dismiss();
            }
        };
        adapter3.startListening();
        itemList.setAdapter(adapter3);
    }

    public class QnAViewHolder extends RecyclerView.ViewHolder {
        View mView;


        public QnAViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setQuestion(String item) {
            TextView name = mView.findViewById(R.id.question);
            name.setText(item);

        }

        public void setAnswer(String item) {
            TextView book_name = mView.findViewById(R.id.answer);
            book_name.setText(item);
        }

        public void setButton(final String item) {
            TextView book_name = mView.findViewById(R.id.write_ans_btn);
            book_name.setText(item);

        }
    }
}
