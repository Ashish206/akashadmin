package com.dits.akashonlinebooks.admin.model;

public class HistoryDetailModel {

    String ActualAmount,Amount,Author,BookName,Image,MRP,Publisher,actualCost,key,product_id,quantity,Rated,HistoryKey;

    public HistoryDetailModel() {
    }

    public HistoryDetailModel(String actualAmount, String amount, String author, String bookName, String image, String MRP, String publisher, String actualCost, String key, String product_id, String quantity, String rated, String historyKey) {
        ActualAmount = actualAmount;
        Amount = amount;
        Author = author;
        BookName = bookName;
        Image = image;
        this.MRP = MRP;
        Publisher = publisher;
        this.actualCost = actualCost;
        this.key = key;
        this.product_id = product_id;
        this.quantity = quantity;
        Rated = rated;
        HistoryKey = historyKey;
    }

    public String getActualAmount() {
        return ActualAmount;
    }

    public void setActualAmount(String actualAmount) {
        ActualAmount = actualAmount;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getBookName() {
        return BookName;
    }

    public void setBookName(String bookName) {
        BookName = bookName;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getMRP() {
        return MRP;
    }

    public void setMRP(String MRP) {
        this.MRP = MRP;
    }

    public String getPublisher() {
        return Publisher;
    }

    public void setPublisher(String publisher) {
        Publisher = publisher;
    }

    public String getActualCost() {
        return actualCost;
    }

    public void setActualCost(String actualCost) {
        this.actualCost = actualCost;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getRated() {
        return Rated;
    }

    public void setRated(String rated) {
        Rated = rated;
    }

    public String getHistoryKey() {
        return HistoryKey;
    }

    public void setHistoryKey(String historyKey) {
        HistoryKey = historyKey;
    }
}
