package com.dits.akashonlinebooks.admin.model;

public class OfferModel {
    String category;

    public OfferModel() {
    }

    public OfferModel(String category) {
        this.category = category;

    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

}

