package com.dits.akashonlinebooks.admin.model;

public class SubCategoryModel {
    String subcategory;

    public SubCategoryModel() {
    }

    public SubCategoryModel(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getSubcategory() {
        return subcategory;
    }
}
