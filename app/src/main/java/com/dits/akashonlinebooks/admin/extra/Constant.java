package com.dits.akashonlinebooks.admin.extra;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dits.akashonlinebooks.admin.activity.NotificationActivity;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Constant {
    static JSONObject info,json;
    public static DatabaseReference deliveryBoyRegistrationRef,rootRef,orderref,requestBookRef,pickingOrderRef,packingOrderRef,dispatchOrder,deliveryRef,deliveryOrderRef;
    public static boolean admin = true;
    public static DatabaseReference categoryRef;
    public static DatabaseReference subcategoryRef;
    public static DatabaseReference questionRef,offerZoneRef;
    public static String selectedItemName = "";
    public static String selectedCategory="";
    public static String selectedSubCategory="";
    public static String categoryId="";
    public static String subCategoryId="";
    public static String adminType,adminName,adminUID,adminPhone,permission;
    public static String picking = "Picking", main = "Main", dispatch = "Dispatch", mainPermission = "Home Screen Notification Order Details Book Details Statistics Admin Group Book Requests Offer Zone Extra Picked Orders Pack Orders Dispatch Orders Register Order Delivery";
    public static List<String> categoryList,authorList,publishList;
    public static String BASE_URL = "https://akashpustaksadan.com/app/";

    private OnResponseFromServer onResponseFromServer;


    public static String upperCaseWords(String sentence) {
        String words[] = sentence.replaceAll("\\s+", " ").trim().split(" ");
        String newSentence = "";
        for (String word : words) {
            for (int i = 0; i < word.length(); i++)
                newSentence = newSentence + ((i == 0) ? word.substring(i, i + 1).toUpperCase():
                        (i != word.length() - 1) ? word.substring(i, i + 1).toLowerCase() : word.substring(i, i + 1).toLowerCase().toLowerCase() + " ");
        }

        return newSentence;
    }

    public static void sendNotification(Context ctx,String tokenId,String title,String body){
        json = new JSONObject();
        info = new JSONObject();
        try {
            info.put("title", title);
            info.put("body", body);
            json.put("notification", info);
            json.put("to",tokenId);

        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            sendNotification(ctx);
        }
    }

    static void sendNotification(final Context ctx){
        String url = "https://fcm.googleapis.com/fcm/send";
        RequestQueue queue = Volley.newRequestQueue(ctx);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.POST,url, json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(ctx, "sent", Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ctx, "Opps!! "+error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", "key=AIzaSyCJOrn9A1ZnhnowMrTmxyrNEVqp12758zI");
                return headers;
            }
        };


        queue.add(jsonObjReq);
    }


    public interface OnResponseFromServer{
        void onEvent(String response);
    }

    public void getDataFromServer(Context ctx, int type, String path, OnResponseFromServer eventListener, final HashMap hm){
        onResponseFromServer=eventListener;
        RequestQueue queue = Volley.newRequestQueue(ctx);
        StringRequest stringRequest = new StringRequest(type, Constant.BASE_URL + path, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    if(onResponseFromServer!=null){
                        onResponseFromServer.onEvent(response);
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                    if(onResponseFromServer!=null){
                        onResponseFromServer.onEvent("error");
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("RESPONSE:>>> ", error.toString() + "<<<");
                if(onResponseFromServer!=null){
                    onResponseFromServer.onEvent("error");
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return hm;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded";
            }
        };

        queue.add(stringRequest);

    }


}
