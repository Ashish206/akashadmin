package com.dits.akashonlinebooks.admin.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dits.akashonlinebooks.admin.R;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;

import static com.dits.akashonlinebooks.admin.extra.Constant.offerZoneRef;

public class HomeScreenActivity extends AppCompatActivity {
    FloatingActionButton addFab;
    ProgressDialog progressDialog;
    private Uri resultUri;
    private ImageView profileImage;
    RecyclerView itemList;
    TextView notDataText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        addFab = findViewById(R.id.add_fab_btn);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        notDataText = findViewById(R.id.not_data_text);

        addFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddItemDialog();
            }

        });

        itemList = findViewById(R.id.itemList);
        itemList.setHasFixedSize(true);
        itemList.setLayoutManager(new GridLayoutManager(this, 1));
    }

    private void showAddItemDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Add Category");
        LayoutInflater inflater = LayoutInflater.from(this);
        View addBooks = inflater.inflate(R.layout.add_offer_activity,null);
        final TextInputEditText fieldFirst = addBooks.findViewById(R.id.field_first);
        fieldFirst.setHint("Category Name");

        final Button saveBtn = addBooks.findViewById(R.id.saveBtn);
        final Button cancelBtn = addBooks.findViewById(R.id.cancelBtn);
        profileImage = addBooks.findViewById(R.id.productImage);


        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, 1);
            }
        });
        dialog.setCancelable(false);
        dialog.setView(addBooks);


        final AlertDialog alertDialog = dialog.create();

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(fieldFirst.getText().toString())
                        && resultUri != null && !Uri.EMPTY.equals(resultUri)){
                    progressDialog.show();

                    final StorageReference filePath = FirebaseStorage.getInstance().getReference().child("OfferZone").child("Jabalpur").child("OfferZone").child(resultUri.getLastPathSegment());

                    Bitmap bitmap = null;
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), resultUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 20, baos);
                    byte[] data = baos.toByteArray();
                    UploadTask uploadTask = filePath.putBytes(data);

                    uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if (!task.isSuccessful()) {
                                throw task.getException();
                            }
                            return filePath.getDownloadUrl();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            if (task.isSuccessful()) {
                                Uri downloadUri = task.getResult();
                                String key =  offerZoneRef.push().getKey();
                                HashMap hm = new HashMap();
                                hm.put("category",fieldFirst.getText().toString().toLowerCase());
                                hm.put("photo",String.valueOf(downloadUri));
                                hm.put("id",key);


                                offerZoneRef.child(key).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                                    @Override
                                    public void onComplete(Task task) {
                                        if (task.isSuccessful()){
                                            progressDialog.dismiss();
                                            alertDialog.dismiss();
                                            resultUri =null;
                                            Toast.makeText(getApplicationContext(), "Item Added!!", Toast.LENGTH_SHORT).show();
                                        }else {
                                            progressDialog.dismiss();
                                            alertDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Oops/"+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });

                            } else {
                                progressDialog.dismiss();
                                alertDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Oops/"+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                }
                else {
                    Toast.makeText(getApplicationContext(), "Please Fill All Fields...", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });


        alertDialog.show();
    }
}
