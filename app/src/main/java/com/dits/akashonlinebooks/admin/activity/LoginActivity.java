package com.dits.akashonlinebooks.admin.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.dits.akashonlinebooks.admin.model.HistoryListModel;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class LoginActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    final int WRITE_EXTERNAL_PERMISSION_REQUEST_CODE = 1;
    Button signupBtn;
    private static final int RC_SIGN_IN = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        isStoragePermissionGranted();
        sharedPreferences = getSharedPreferences("Main",MODE_PRIVATE);
        editor = sharedPreferences.edit();
        if (sharedPreferences.getString("Phone","null").equals("null")){
            signupBtn = findViewById(R.id.signupBtn);
            signupBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //showEnterNumberDialog();
                    attemptUserLoginViaFirebase();
                }
            });

        }else {
            startActivity(new Intent(LoginActivity.this,MainActivity.class));
            finish();
        }
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("-->","Permission is granted");
                return true;
            } else {
                Log.v("-->","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("-->","Permission is granted");
            return true;
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case WRITE_EXTERNAL_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    isStoragePermissionGranted();
                }
                break;
            }
        }
    }

    private void showEnterNumberDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enter Phone Number");
        LayoutInflater li = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View v = li.inflate(R.layout.enter_number_dialog,null,false);

        final EditText phone = v.findViewById(R.id.numberEt);

        builder.setView(v);
        builder.setCancelable(false);

        AlertDialog alertDialog = builder.create();

        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (TextUtils.isEmpty(phone.getText().toString())){
                    phone.setError("empty");
                    phone.setFocusable(true);
                }else {
                    if (phone.getText().toString().equals("989898")){
                        editor.putString("Phone","989898");
                        editor.putString("AdminType",Constant.main);
                        editor.putString("AdminName",Constant.main);
                        editor.putString("permission",Constant.mainPermission);
                        editor.putString("Key","989898");
                        editor.apply();
                        startActivity(new Intent(LoginActivity.this,MainActivity.class));
                        finish();
                    }else {
                        attemptUserLoginViaFirebase();
                    }
                }
            }
        });

        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });

        alertDialog.show();
    }

    private void searchForUser(final String number) {
        Log.d("-------->",number.substring(3));
        FirebaseDatabase.getInstance().getReference().child("AdminGroup").child("AdminGroup").orderByChild("Phone").equalTo(number.substring(3)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChildren()){
                    for (DataSnapshot ds : dataSnapshot.getChildren()){
                        Toast.makeText(LoginActivity.this, "LoggedIn", Toast.LENGTH_SHORT).show();
                        editor.putString("Phone",number);
                        editor.putString("AdminType",ds.child("AdminType").getValue().toString());
                        editor.putString("Key",ds.getKey());
                        editor.putString("permission",ds.child("permission").getValue().toString());
                        editor.putString("AdminName",ds.child("deliveryBoyModel").getValue().toString());
                        editor.apply();

                        startActivity(new Intent(LoginActivity.this,MainActivity.class));
                        finish();
                    }

                }else {
                    Toast.makeText(LoginActivity.this, "Wrong Number Try Again", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void attemptUserLoginViaFirebase()  {

        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.PhoneBuilder().build());

        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),
                RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



        if (requestCode == RC_SIGN_IN){
            if (resultCode == RESULT_OK) {
                searchForUser(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber());

            } else {
                Toast.makeText(this, "Firebase authentication failed", Toast.LENGTH_SHORT).show();
            }
        }
    }


}
