package com.dits.akashonlinebooks.admin.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.model.CategoryModel;

import java.util.ArrayList;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ItemViewHolder> implements Filterable {

    ArrayList<CategoryModel> arrayList;
    ArrayList<CategoryModel> newArrayList;
    Context context;
    ItemListener itemListener;

    public CategoryAdapter(ArrayList<CategoryModel> arrayList,Context ctx,ItemListener itemListener){
        this.arrayList = arrayList;
        this.newArrayList = arrayList;
        context = ctx;
        this.itemListener = itemListener;
    }


    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.category_single_layout, viewGroup, false);

        return new CategoryAdapter.ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder itemViewHolder, int i) {

        itemViewHolder.setItemName(newArrayList.get(i).getCategory(),newArrayList.get(i).getKey());
    }

    @Override
    public int getItemCount() {
        return newArrayList.size();
    }


    public interface ItemListener {
        void onItemClick(String key,String category);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    newArrayList = arrayList;
                } else {
                    ArrayList filteredList = new ArrayList<>();

                    for (int i = 0; i<newArrayList.size();i++) {
                        String row = newArrayList.get(i).getCategory();
                        if (row.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(newArrayList.get(i));
                        }
                    }

                    newArrayList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = newArrayList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                newArrayList = (ArrayList) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        View mView;
        String key,category;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            mView.setOnClickListener(this);

        }

        public void setItemName(String item,String key) {
            TextView book_name = mView.findViewById(R.id.textView);
            book_name.setText(item);
            this.key = key;
            this.category = item;
        }

        public void setItemImage(String item, Context context) {
            ImageView itemImage = mView.findViewById(R.id.categoryImage);
            Glide.with(context).load(item).apply(new RequestOptions().placeholder(R.drawable.no_image).error(R.drawable.no_image)).into(itemImage);
        }

        @Override
        public void onClick(View view) {
            if (itemListener != null){
                itemListener.onItemClick(key,category);
            }
        }
    }
}
