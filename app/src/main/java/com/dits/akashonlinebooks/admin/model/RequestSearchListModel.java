package com.dits.akashonlinebooks.admin.model;

public class RequestSearchListModel {

    String bookId,bookName;

    public RequestSearchListModel() {
    }

    public RequestSearchListModel(String bookId, String bookName) {
        this.bookId = bookId;
        this.bookName = bookName;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }
}
