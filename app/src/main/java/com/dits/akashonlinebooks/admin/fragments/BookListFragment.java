package com.dits.akashonlinebooks.admin.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.activity.BookDetailEditActivity;
import com.dits.akashonlinebooks.admin.activity.BookDetailsActivity;
import com.dits.akashonlinebooks.admin.adapter.BookDetailGridAdapter;
import com.dits.akashonlinebooks.admin.extra.ApiClient;
import com.dits.akashonlinebooks.admin.extra.ApiInterface;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.dits.akashonlinebooks.admin.model.BookDataModel;
import com.dits.akashonlinebooks.admin.model.OfferGetIdModel;
import com.dits.akashonlinebooks.admin.utils.PaginationAdapterCallback;
import com.dits.akashonlinebooks.admin.utils.PaginationScrollListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookListFragment extends Fragment implements PaginationAdapterCallback,
        BookDetailsActivity.ClearFilterListenerHome,BookDetailGridAdapter.ItemListener,BookDetailGridAdapter.ItemLongClickListener
        {


    public static BookDetailGridAdapter adapter;
    GridLayoutManager manager;
    RecyclerView rv;
    ProgressBar progressBar;
    LinearLayout errorLayout;
    Button btnRetry;
    TextView txtError;
    SwipeRefreshLayout swipeRefreshLayout;
    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    public static boolean isLastPage = false;
    private static final int TOTAL_PAGES = 100;
    private int currentPage = PAGE_START;
    private ApiInterface movieService;
    public static BookListFragment ctx;
            List<OfferGetIdModel> offerList;
            DatabaseReference offerBooksRef;
    View v;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_book_list, container, false);

        rv = v.findViewById(R.id.bookList);
        progressBar = v.findViewById(R.id.main_progress);
        errorLayout = v.findViewById(R.id.error_layout);
        btnRetry = v.findViewById(R.id.error_btn_retry);
        txtError = v.findViewById(R.id.error_txt_cause);
        swipeRefreshLayout = v.findViewById(R.id.main_swiperefresh);

        offerList = new ArrayList();
        ctx = BookListFragment.this;
        offerBooksRef = FirebaseDatabase.getInstance().getReference().child("OfferZoneBooks");
        rv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return false;
            }
        });

        adapter = new BookDetailGridAdapter(getContext(),BookListFragment.this,BookListFragment.this,BookListFragment.this);

        manager = new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false);
        rv.setLayoutManager(manager);
        rv.setItemAnimator(new DefaultItemAnimator());

        rv.setAdapter(adapter);

        movieService = ApiClient.getClient().create(ApiInterface.class);

        loadFirstPage();

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadFirstPage();
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!isLastPage){
                    doRefresh();
                }else {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
        Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {

            @Override
            public void run() {
                //start your activity here
                BookDetailsActivity.searchView.requestFocus();
            }

        }, 500);

        loadOffer();
        return v;
    }

            private void loadOffer() {
                Constant.offerZoneRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot ds : dataSnapshot.getChildren()){
                            offerList.add(ds.getValue(OfferGetIdModel.class));
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }


    @Override
    public void onResume() {
        super.onResume();
        swipeRefreshLayout.requestFocus();
    }


    private void doRefresh() {
        progressBar.setVisibility(View.VISIBLE);
        if (callTopRatedMoviesApi().isExecuted())
            callTopRatedMoviesApi().cancel();

        // TODO: Check if data is stale.
        //  Execute network request if cache is expired; otherwise do not update data.
        adapter.clear();
        adapter.notifyDataSetChanged();
        loadFirstPage();
        swipeRefreshLayout.setRefreshing(false);
    }

    public void loadFirstPage() {

        rv.addOnScrollListener(new PaginationScrollListener(manager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                loadNextPage();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        // To ensure list is visible when retry button in error view is clicked
        hideErrorView();
        currentPage = PAGE_START;

        callTopRatedMoviesApi().enqueue(new Callback<List<BookDataModel>>() {
            @Override
            public void onResponse(Call<List<BookDataModel>> call, Response<List<BookDataModel>> response) {
                hideErrorView();


//                SearchResultFragment searchResultFragment = new SearchResultFragment();
//                searchResultFragment.show(HomeActivity.fragmentManager, searchResultFragment.getTag());

                progressBar.setVisibility(View.GONE);
                adapter.addAll(response.body());

                if (currentPage <= TOTAL_PAGES) adapter.addLoadingFooter();
                else isLastPage = true;
            }

            @Override
            public void onFailure(Call<List<BookDataModel>> call, Throwable t) {
                t.printStackTrace();
                showErrorView(t);
            }
        });
    }

    public void loadFirstPage(String filterKey,String filterBy) {

        // To ensure list is visible when retry button in error view is clicked
        hideErrorView();
//        currentPage = PAGE_START;
        Log.d("------->","---");

        filterBy(filterKey,filterBy).enqueue(new Callback<List<BookDataModel>>() {
            @Override
            public void onResponse(Call<List<BookDataModel>> call, Response<List<BookDataModel>> response) {
                hideErrorView();

                isLastPage = true;

                progressBar.setVisibility(View.GONE);
                adapter.addAll(response.body());

//                if (currentPage <= TOTAL_PAGES) adapter.addLoadingFooter();
//                else isLastPage = true;
            }

            @Override
            public void onFailure(Call<List<BookDataModel>> call, Throwable t) {
                t.printStackTrace();
                showErrorView(t);
            }
        });
    }



    private void loadNextPage() {


        callTopRatedMoviesApi().enqueue(new Callback<List<BookDataModel>>() {
            @Override
            public void onResponse(Call<List<BookDataModel>> call, Response<List<BookDataModel>> response) {
//                Log.i(TAG, "onResponse: " + currentPage
//                        + (response.raw().cacheResponse() != null ? "Cache" : "Network"));

                adapter.removeLoadingFooter();
                isLoading = false;

//                List<BookDetailsModel> results = fetchResults(response);
                adapter.addAll(response.body());

                if (currentPage != TOTAL_PAGES) adapter.addLoadingFooter();
                else isLastPage = true;
            }

            @Override
            public void onFailure(Call<List<BookDataModel>> call, Throwable t) {
                t.printStackTrace();
                adapter.showRetry(true, fetchErrorMessage(t));
            }
        });
    }


    /**
     * Performs a Retrofit call to the top rated movies API.
     * Same API call for Pagination.
     * As {@link #currentPage} will be incremented automatically
     * by @{@link PaginationScrollListener} to load next page.
     */
    private Call<List<BookDataModel>> callTopRatedMoviesApi() {
        return movieService.getAllBookDetails(
                currentPage
        );
    }

    private Call<List<BookDataModel>> filterBy(String filterKey,String filterBy) {
        return movieService.getFilteredList(
                currentPage,
                filterKey,
                filterBy
        );
    }


    @Override
    public void retryPageLoad() {
        loadNextPage();
    }


    /**
     * @param throwable required for {@link #fetchErrorMessage(Throwable)}
     * @return
     */
    private void showErrorView(Throwable throwable) {

        if (errorLayout.getVisibility() == View.GONE) {
            errorLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

            txtError.setText(fetchErrorMessage(throwable));
        }
    }

    /**
     * @param throwable to identify the type of error
     * @return appropriate error message
     */
    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);

        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }

        return errorMsg;
    }


    private void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Remember to add android.permission.ACCESS_NETWORK_STATE permission.
     *
     * @return
     */
    private void showDialog(String[] values, final String id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Select");

        builder.setItems(values, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String key = offerBooksRef.child(offerList.get(which).getId()).push().getKey();
                offerBooksRef.child(offerList.get(which).getId()).child(key).child("id").setValue(id).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(getContext(), "Added.", Toast.LENGTH_SHORT).show();
//                        loadBooks();
                    }
                });
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
    @Override
    public void onItemClick(String key, ImageView imageView) {
        Intent intent = new Intent(getContext(), BookDetailEditActivity.class);
        intent.putExtra("key",key);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                getActivity(), imageView, "image_transition");
        startActivity(intent);
    }

            @Override
            public void ClearFilterListenerHome() {
                adapter.clear();
                loadFirstPage();
            }

            @Override
            public void onItemLongClickListener(String key) {
                String[] values = new String[offerList.size()];
                for(int i=0; i<offerList.size(); i++){
                    values[i] = offerList.get(i).getCategory();
                }
                showDialog(values,key);
            }
        }
