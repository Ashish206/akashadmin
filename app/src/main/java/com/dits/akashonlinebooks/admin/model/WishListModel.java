package com.dits.akashonlinebooks.admin.model;

public class WishListModel {
    String key;

    public WishListModel() {
    }

    public WishListModel(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
