package com.dits.akashonlinebooks.admin.model;

public class HomeMenuModel {
    public String text;
    public int drawable;
    public String color;

    public HomeMenuModel(String t, int d, String c )
    {
        text=t;
        drawable=d;
        color=c;
    }
}
