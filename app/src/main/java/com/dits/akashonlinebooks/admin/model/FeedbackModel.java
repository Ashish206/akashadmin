package com.dits.akashonlinebooks.admin.model;

public class FeedbackModel {
    String uid,name,date,feedback;

    public FeedbackModel() {
    }

    public FeedbackModel(String uid, String name, String date, String feedback) {
        this.uid = uid;
        this.name = name;
        this.date = date;
        this.feedback = feedback;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
}
