package com.dits.akashonlinebooks.admin.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.adapter.CategoryAdapter;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.dits.akashonlinebooks.admin.model.CategoryModel;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;


public class CategoryFragment extends Fragment implements CategoryAdapter.ItemListener {
    FloatingActionButton addFab;
    ProgressDialog progressDialog;
    private Uri resultUri;
    private ImageView profileImage;
    SearchView searchView;
    RecyclerView itemList;
    SubCategoryFragment subCategoryFragment;
    ArrayList<CategoryModel> categoryModelArrayList;
    CategoryAdapter categoryAdapter;
    @SuppressLint("RestrictedApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_category, container, false);

        addFab = v.findViewById(R.id.add_fab_btn);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        categoryModelArrayList = new ArrayList<>();
        subCategoryFragment = new SubCategoryFragment();
        addFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddItemDialog();
            }

        });
        itemList = v.findViewById(R.id.itemList);
        itemList.setHasFixedSize(true);
        itemList.setLayoutManager(new GridLayoutManager(getContext(), 1));

        searchView = v.findViewById(R.id.search_bar);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                categoryAdapter.getFilter().filter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                categoryAdapter.getFilter().filter(s);
                return false;
            }
        });

        return v;
    }

    private void showAddItemDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setTitle("Add "+Constant.selectedItemName);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View addBooks = inflater.inflate(R.layout.add_category,null);
        final TextInputEditText fieldFirst = addBooks.findViewById(R.id.field_first);
        fieldFirst.setHint(Constant.selectedItemName+" Name");

        final Button saveBtn = addBooks.findViewById(R.id.saveBtn);
        final Button cancelBtn = addBooks.findViewById(R.id.cancelBtn);
        profileImage = addBooks.findViewById(R.id.productImage);


        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, 1);
            }
        });
        dialog.setCancelable(false);
        dialog.setView(addBooks);


        final AlertDialog alertDialog = dialog.create();

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(fieldFirst.getText().toString())){
                    progressDialog.show();

                    HashMap hm = new HashMap();
                    hm.put("category",fieldFirst.getText().toString().trim().toLowerCase());
                    // hm.put("photo",String.valueOf(downloadUri));
                    String key =  Constant.categoryRef.child(Constant.selectedItemName).push().getKey();

                    Constant.categoryRef.child(Constant.selectedItemName).child(key).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(Task task) {
                            if (task.isSuccessful()){
                                progressDialog.dismiss();
                                alertDialog.dismiss();
                                resultUri =null;
                                Toast.makeText(getContext(), "Item Added!!", Toast.LENGTH_SHORT).show();
                            }else {
                                progressDialog.dismiss();
                                alertDialog.dismiss();
                                Toast.makeText(getContext(), "Oops/"+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                else {
                    Toast.makeText(getContext(), "Please Fill All Fields...", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });


        alertDialog.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        loadCategoryList();
    }

    private void loadCategoryList() {
        progressDialog.show();
        categoryModelArrayList.clear();
        Constant.categoryRef.child(Constant.selectedItemName).orderByChild("category").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    CategoryModel categoryModel = new CategoryModel();
                    categoryModel.setCategory(ds.child("category").getValue().toString());
                    categoryModel.setKey(ds.getKey());
                    categoryModelArrayList.add(categoryModel);
                }
                progressDialog.dismiss();
                categoryAdapter= new CategoryAdapter(categoryModelArrayList,getContext(),CategoryFragment.this);
                itemList.setAdapter(categoryAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

//        FirebaseRecyclerOptions<CategoryModel> options =
//                new FirebaseRecyclerOptions.Builder<CategoryModel>()
//                        .setQuery(Constant.categoryRef.child(Constant.selectedItemName).orderByChild("category"), CategoryModel.class)
//                        .build();
//        loadList(options);
    }

    private void loadList(FirebaseRecyclerOptions<CategoryModel> options) {



        FirebaseRecyclerAdapter adapter = new FirebaseRecyclerAdapter<CategoryModel, ItemViewHolder>(options) {
            @Override
            public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.category_single_layout, parent, false);

                return new ItemViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(ItemViewHolder holder, int position, final CategoryModel model) {
                holder.setItemName(Constant.upperCaseWords(model.getCategory()));
                final String id =getRef(position).getKey();
                holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        if (Constant.admin){
                            editDialog(model.getCategory(),id);
                        }
                        return false;
                    }
                });

            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                progressDialog.dismiss();
            }
        };
        adapter.startListening();
        itemList.setAdapter(adapter);
    }

    private void editDialog(String bookName, final String id) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setTitle("Edit Subject");
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View addBooks = inflater.inflate(R.layout.add_category,null);
        final TextInputEditText fieldFirst = addBooks.findViewById(R.id.field_first);
        fieldFirst.setHint("Subject");

        final Button saveBtn = addBooks.findViewById(R.id.saveBtn);
        final Button cancelBtn = addBooks.findViewById(R.id.cancelBtn);
        profileImage = addBooks.findViewById(R.id.productImage);
        ImageView deleteBtn = addBooks.findViewById(R.id.delete_btn);



        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, 1);
            }
        });
        dialog.setCancelable(false);
        dialog.setView(addBooks);
//        Glide.with(getActivity().getApplicationContext()).load(photo).into(profileImage);
        fieldFirst.setText(bookName);

        final AlertDialog alertDialog = dialog.create();
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                deleteDialog(id);
            }
        });
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(fieldFirst.getText().toString())){
                    HashMap hm = new HashMap();
                    hm.put("category",fieldFirst.getText().toString().toLowerCase());
                   // hm.put("photo",photo);


                    Constant.categoryRef.child(Constant.selectedItemName).child(id).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(Task task) {
                            if (task.isSuccessful()){
                                progressDialog.dismiss();
                                alertDialog.dismiss();
                                loadCategoryList();
                                Toast.makeText(getContext(), "Item Edited!!", Toast.LENGTH_SHORT).show();
                            }else {
                                progressDialog.dismiss();
                                alertDialog.dismiss();
                                Toast.makeText(getContext(), "Oops/"+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                else {
                    Toast.makeText(getContext(), "Please Fill All Fields...", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });


        alertDialog.show();
    }

    private void deleteDialog(final String id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Delete");
        builder.setMessage("Do you want to delete this item?");
        builder.setCancelable(false);
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Constant.categoryRef.child(Constant.selectedItemName).child(id).setValue(null);
                loadCategoryList();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            final Uri imageUri = data.getData();
            resultUri = imageUri;
            profileImage.setImageURI(resultUri);
        }
    }

    @Override
    public void onItemClick(String key, String category) {
        if (Constant.admin){
            editDialog(category,key);
        }
    }

    private static class ItemViewHolder extends RecyclerView.ViewHolder {
        View mView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setItemName(String item) {
            TextView book_name = mView.findViewById(R.id.textView);
            book_name.setText(item);

        }

        public void setItemImage(String item, Context context) {
            ImageView itemImage = mView.findViewById(R.id.categoryImage);
            Glide.with(context).load(item).apply(new RequestOptions().placeholder(R.drawable.no_image).error(R.drawable.no_image)).into(itemImage);
        }

    }
}

