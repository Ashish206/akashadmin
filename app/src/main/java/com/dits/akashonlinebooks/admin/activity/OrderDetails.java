package com.dits.akashonlinebooks.admin.activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.dits.akashonlinebooks.admin.model.HistoryListModel;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class OrderDetails extends AppCompatActivity {

    RecyclerView historyList;
    LinearLayoutManager linearLayoutManager;
    ProgressDialog progressDialog;
    public static HashMap infoHashMap;
    int screen = 0;
    FragmentManager fragmentManager;
    Fragment mFragment;
    public static String OrderKey;
    DatePickerDialog datePickerDialog;
    ImageView filterBtn,undelivered_filter,delivered_filter,clearFilter;
    Calendar c;
    int year;
    int month;
    int day;
    DateFormat dateFormat;
    FirebaseRecyclerOptions<HistoryListModel> options;
    String formatedDate;
    TextView countText;
    public static String orderKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);


        historyList = findViewById(R.id.historyList);
        infoHashMap = new HashMap();
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        historyList.setLayoutManager(linearLayoutManager);
        historyList.setHasFixedSize(true);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        countText = findViewById(R.id.count);

        undelivered_filter = findViewById(R.id.undelivered_filter);
        delivered_filter = findViewById(R.id.delivered_filter);
        clearFilter = findViewById(R.id.clear_filter);

        filterBtn = findViewById(R.id.filterBtn);
        dateFormat = new SimpleDateFormat("EEE, MMM d, ''yy");

        c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);




        clearFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadHistory("null");
            }
        });

        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                int modMonth = month+1;
                try {
                    Date oldDate = new SimpleDateFormat("yyyy-MM-dd").parse(year+"-"+modMonth+"-"+dayOfMonth);
                    formatedDate = dateFormat.format(oldDate);
                    loadHistory("Date");

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        },year,month,day);


        undelivered_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadHistory("UnDelivered");
            }
        });

        delivered_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadHistory("Delivered");
            }
        });

        filterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        loadHistory("null");
    }

    private void loadHistory(String filter) {
        if (filter.equals("null")){
            options = new FirebaseRecyclerOptions.Builder<HistoryListModel>()
                    .setQuery(Constant.orderref.limitToLast(100).orderByChild("delivered"),HistoryListModel.class)
                    .build();
        }
        else if (filter.equals("Delivered")){
            options = new FirebaseRecyclerOptions.Builder<HistoryListModel>()
                    .setQuery(Constant.orderref.orderByChild("delivered").equalTo("Delivered").limitToLast(100),HistoryListModel.class)
                    .build();
        }else if (filter.equals("UnDelivered")){
            options = new FirebaseRecyclerOptions.Builder<HistoryListModel>()
                    .setQuery(Constant.orderref.orderByChild("delivered").equalTo("Order Packed").limitToLast(100),HistoryListModel.class)
                    .build();
        }else if (filter.equals("Date")){
            options = new FirebaseRecyclerOptions.Builder<HistoryListModel>()
                    .setQuery(Constant.orderref.orderByChild("date").equalTo(formatedDate),HistoryListModel.class)
                    .build();
        }

        FirebaseRecyclerAdapter firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<HistoryListModel, HistoryViewHolder>(options) {
            @NonNull
            @Override
            public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.single_history_layout, viewGroup, false);
                return new HistoryViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull final HistoryViewHolder holder, @SuppressLint("RecyclerView") final int position, @NonNull final HistoryListModel model) {
                holder.setOrderDate(model.getDate());
                holder.setBookName(getRef(position).getKey(),position);
                holder.setCost(model.getTotalAmount());
                holder.setActualCost(model.getActualTotalAmount());
                holder.setDiscount(Float.parseFloat(model.getTotalAmount())-Float.parseFloat(model.getDeliveryCharge()),Float.parseFloat(model.getActualTotalAmount()));
                holder.setDeliveryStatus(model.getDelivered());
                holder.setDeliveryBoy(model.getDeliveryBoyName());


                if (model.getPickedBy().toLowerCase().equals("-")){
                    holder.mView.findViewById(R.id.deliveryBtn).setVisibility(View.VISIBLE);
                }else {
                    holder.mView.findViewById(R.id.deliveryBtn).setVisibility(View.GONE);
                }

                if (model.getDelivered().equals("Cancelled")){
                    holder.itemView.findViewById(R.id.deliveryBtn).setVisibility(View.GONE);
                }

                //holder.mView.findViewById(R.id.deliveryBoyDetails).setVisibility(View.VISIBLE);

                holder.mView.findViewById(R.id.deliveryBtn).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        orderKey = getRef(position).getKey();
                        if (model.getPickedBy().toLowerCase().equals("-")){
                            final String key = Constant.deliveryBoyRegistrationRef.child(Constant.adminUID).push().getKey();
                            Date today = new Date();
                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                            String dateToStr = format.format(today);
                            HashMap hm = new HashMap();
                            hm.put("pickedBy",Constant.adminName);
                            hm.put("pickedDateTime",dateToStr);
                            hm.put("delivered","Picked");
                            Constant.orderref.child(OrderDetails.orderKey).updateChildren(hm).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    Constant.orderref.child(orderKey).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            FirebaseDatabase.getInstance().getReference().child("Orders").child("Jabalpur").child("Orders").
                                                    child(model.getOrder_id()).child("delivered").setValue("Picked");

                                            Constant.deliveryBoyRegistrationRef.child(Constant.adminUID).child("Orders").child(key).setValue(dataSnapshot.getValue()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()){
                                                        Toast.makeText(getApplicationContext(), "Accepted!!", Toast.LENGTH_SHORT).show();
                                                    }else {
                                                        Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            });
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });

                                }
                            });

                        }else {
                            Toast.makeText(OrderDetails.this, "Order Accepted By Any Other Partner.", Toast.LENGTH_SHORT).show();
                        }

                    }
                });


                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Toast.makeText(HistoryActivity.this, model.getCost(), Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(OrderDetails.this,HistoryDetailsActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        i.putExtra("Cost",model.getTotalAmount());
                        i.putExtra("ActualCost",model.getActualTotalAmount());
                        i.putExtra("date",model.getDate());
                        i.putExtra("delivered",model.getDelivered());
                        i.putExtra("orderKey",model.getOrder_id());
                        i.putExtra("orderref",getRef(position).getKey());
                        i.putExtra("paymentMethod",model.getPaymentMethod());
                        i.putExtra("userAddress",model.getUserAddress());
                        i.putExtra("userId",model.getUserId());
                        i.putExtra("userName",model.getUserName());
                        i.putExtra("userEmail",model.getUserEmail());
                        i.putExtra("userPhone",model.getUserPhone());
                        i.putExtra("userPostalCode",model.getUserPostalCode());
                        i.putExtra("orderIdUser",model.getOrderIdUser());
                        i.putExtra("deliveryCharge",model.getDeliveryCharge());
                        i.putExtra("referralAmount",model.getReferralAmount());
                        i.putExtra("pickedBy",model.getPickedBy());
                        i.putExtra("packedBy",model.getPackedBy());
                        i.putExtra("deliveredBy",model.getDeliveryBoyName());
                        i.putExtra("pickedDateTime",model.getPickedDateTime());
                        i.putExtra("packedDateTime",model.getPackedDateTime());
                        i.putExtra("deliveredDateTime",model.getDeliveredDateTime());
                        startActivity(i);
                        overridePendingTransition(0,0);

                    }
                });
            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                progressDialog.dismiss();
                countText.setText(String.valueOf(getItemCount()));
            }
            @Override
            public int getItemCount() {
                return super.getItemCount();
            }
        };
        firebaseRecyclerAdapter.startListening();
        historyList.setAdapter(firebaseRecyclerAdapter);
    }

    @Override
    public void onBackPressed() {
       super.onBackPressed();
    }

    private class HistoryViewHolder extends RecyclerView.ViewHolder {

        View mView;
        public HistoryViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setBookName(final String item, final int position) {
            final String[] name ={""};
            final TextView item_name = mView.findViewById(R.id.book_name);
            item_name.setText(item);

            Constant.orderref.child(item).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot ds : dataSnapshot.getChildren()){
                        if (ds.hasChild("BookName")){
                            name[0] += ds.child("BookName").getValue().toString()+", ";
                        }
                    }

                    item_name.setText(position+" : "+name[0]);

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }

        public void setOrderDate(String item) {
            TextView subtxt = mView.findViewById(R.id.order_date);
            subtxt.setText("Ordered On "+item);
        }

        public void setCost(String item) {
            TextView subtxt = mView.findViewById(R.id.cost_text);
            subtxt.setText("₹"+item);
        }

        public void setActualCost(String item) {
            TextView subtxt = mView.findViewById(R.id.actual_cost_text);
            subtxt.setText("₹"+item);
        }

        public void setDiscount(float numerator,float denominator) {
            TextView itemImage = mView.findViewById(R.id.discount);
            int percentage = (int)(numerator * 100.0 / denominator + 0.5);
            itemImage.setText(100-percentage + "% off");
        }

        public void setDeliveryBoy(String item) {
            TextView subtxt = mView.findViewById(R.id.deliveryBoyDetails);
            subtxt.setText("Accepted By : "+item);
        }

        public void setDeliveryStatus(String totalCost) {
            TextView book_name = mView.findViewById(R.id.delivery_status);
            book_name.setText(totalCost);
            if (totalCost.equals("Order Placed"))
            {
                book_name.setTextColor(getResources().getColor(R.color.yello));
                book_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_yello_dot, 0, 0, 0);
            }
            else if (totalCost.equals("Cancelled"))
            {
                book_name.setTextColor(getResources().getColor(R.color.red));
                book_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_red_dot, 0, 0, 0);
            }
            else if (totalCost.equals("Picked"))
            {
                book_name.setTextColor(getResources().getColor(R.color.orange));
                book_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_orange_dot, 0, 0, 0);
            }
            else if (totalCost.equals("Order Packed"))
            {
                book_name.setTextColor(getResources().getColor(R.color.colorPrimary));
                book_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_light_blue_dot, 0, 0, 0);
            }
            else if (totalCost.equals("On Way"))
            {
                book_name.setTextColor(getResources().getColor(R.color.yello));
                book_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_yello_dot, 0, 0, 0);
            }
            else {
                book_name.setTextColor(getResources().getColor(R.color.green));
                book_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_green_dot, 0, 0, 0);
            }

        }

    }

}
