package com.dits.akashonlinebooks.admin.model;

public class AuthorModel {
    String Author;

    public AuthorModel() {
    }

    public AuthorModel(String author) {
        Author = author;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }
}
