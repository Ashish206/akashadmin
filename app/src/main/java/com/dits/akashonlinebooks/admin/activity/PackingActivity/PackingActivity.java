package com.dits.akashonlinebooks.admin.activity.PackingActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.activity.PickingActivity.PickingDetailsActivity;
import com.dits.akashonlinebooks.admin.activity.PickingActivity.PickingOrderDetailActivity;
import com.dits.akashonlinebooks.admin.adapter.PackingOrderDetailAdapter;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.dits.akashonlinebooks.admin.extra.Notification;
import com.dits.akashonlinebooks.admin.model.HistoryListModel;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class PackingActivity extends AppCompatActivity {
    RecyclerView historyList;
    LinearLayoutManager linearLayoutManager;
    ProgressDialog progressDialog;
    public static HashMap infoHashMap;
    FirebaseRecyclerOptions<HistoryListModel> options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_packing);

        historyList = findViewById(R.id.historyList);
        infoHashMap = new HashMap();
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        historyList.setLayoutManager(linearLayoutManager);
        historyList.setHasFixedSize(true);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }
    @Override
    protected void onStart() {
        super.onStart();
        loadHistory("null");
    }


    private void loadHistory(String filter) {
        if (filter.equals("null")){
            options = new FirebaseRecyclerOptions.Builder<HistoryListModel>()
                    .setQuery(Constant.packingOrderRef,HistoryListModel.class)
                    .build();
        }

        FirebaseRecyclerAdapter firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<HistoryListModel, HistoryViewHolder>(options) {
            @NonNull
            @Override
            public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.single_history_layout, viewGroup, false);

                return new HistoryViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull final HistoryViewHolder holder, final int position, @NonNull final HistoryListModel model) {

                holder.setOrderDate(model.getDate());
                holder.setBookName(getRef(position).getKey());
                holder.setCost(model.getTotalAmount());
                holder.setActualCost(model.getActualTotalAmount());
                holder.setDiscount(Float.parseFloat(model.getTotalAmount())-Float.parseFloat(model.getDeliveryCharge()),Float.parseFloat(model.getActualTotalAmount()));
                holder.setDeliveryStatus(model.getDelivered());
                holder.setDeliveryBoy(model.getDeliveryBoyName());
                holder.setCallBtn(model.getUserPhone());

                Button b = holder.mView.findViewById(R.id.deliveryBtn);
                b.setText("Send for Dispatch");

                holder.mView.findViewById(R.id.deliveryBtn).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Constant.packingOrderRef.child(getRef(position).getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {

                                //String key = Constant.dispatchOrder.push().getKey();
                                AlertDialog.Builder builder = new AlertDialog.Builder(PackingActivity.this);
                                builder.setMessage("Delete?");
                                builder.setMessage("Did you print invoice and dispatch slip. Do you want to send order for dispatch.");
                                builder.setPositiveButton("Send", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Constant.dispatchOrder.child(model.getOrder_id()).setValue(dataSnapshot.getValue()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()){
                                                    Date today = new Date();
                                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                                                    String dateToStr = format.format(today);
                                                    HashMap hm = new HashMap();
                                                    hm.put("packedBy",Constant.adminName);
                                                    hm.put("packedDateTime",dateToStr);
                                                    hm.put("delivered","Order Packed");
                                                    //new Notification(getApplication()).sendNotificationToSubs("New Order Alert","You have received a new order for packing.","Packing");
                                                    FirebaseDatabase.getInstance().getReference().child("Orders").child("Jabalpur").child("Orders").
                                                            child(model.getOrder_id()).updateChildren(hm);
                                                    FirebaseDatabase.getInstance().getReference().child("UserInfo").child("Jabalpur").child(model.getUserId()).child("history")
                                                            .child(model.getOrderIdUser()).child("delivered").setValue("Order Packed");
                                                    Constant.packingOrderRef.child(getRef(position).getKey()).setValue(null);
                                                }else {
                                                    Toast.makeText(PackingActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });

                                    }
                                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                });
                                builder.show();

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                });

                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent i = new Intent(PackingActivity.this, PackingDetailActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        i.putExtra("Cost",model.getTotalAmount());
                        i.putExtra("ActualCost",model.getActualTotalAmount());
                        i.putExtra("date",model.getDate());
                        i.putExtra("delivered",model.getDelivered());
                        i.putExtra("orderKey",model.getOrder_id());
                        i.putExtra("orderref",getRef(position).getKey());
                        i.putExtra("paymentMethod",model.getPaymentMethod());
                        i.putExtra("deliveryBoyOrderKey",model.getDeliveryBoyOrderKey());
                        i.putExtra("userAddress",model.getUserAddress());
                        i.putExtra("userId",model.getUserId());
                        i.putExtra("userName",model.getUserName());
                        i.putExtra("userPhone",model.getUserPhone());
                        i.putExtra("userPostalCode",model.getUserPostalCode());
                        i.putExtra("orderIdUser",model.getOrderIdUser());
                        i.putExtra("deliveryCharge",model.getDeliveryCharge());
                        i.putExtra("referralAmount",model.getReferralAmount());
                        startActivity(i);
                        overridePendingTransition(0,0);

                    }
                });
            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                progressDialog.dismiss();
            }
            @Override
            public int getItemCount() {
                return super.getItemCount();
            }
        };
        firebaseRecyclerAdapter.startListening();
        historyList.setAdapter(firebaseRecyclerAdapter);
    }

    private class HistoryViewHolder extends RecyclerView.ViewHolder {

        View mView;
        public HistoryViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setBookName(final String item) {
            final String[] name ={""};
            final TextView item_name = mView.findViewById(R.id.book_name);
            item_name.setText(item);

            Constant.packingOrderRef.child(item).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot ds : dataSnapshot.getChildren()){
                        if (ds.hasChild("BookName")){
                            name[0] += ds.child("BookName").getValue().toString()+", ";
                        }
                    }

                    item_name.setText(name[0]);

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }

        public void setOrderDate(String item) {
            TextView subtxt = mView.findViewById(R.id.order_date);
            subtxt.setText("Ordered On "+item);
        }

        public void setCost(String item) {
            TextView subtxt = mView.findViewById(R.id.cost_text);
            subtxt.setText("₹"+item);
        }

        public void setActualCost(String item) {
            TextView subtxt = mView.findViewById(R.id.actual_cost_text);
            subtxt.setText("₹"+item);
        }

        public void setDiscount(float numerator,float denominator) {
            TextView itemImage = mView.findViewById(R.id.discount);
            int percentage = (int)(numerator * 100.0 / denominator + 0.5);
            itemImage.setText(100-percentage + "% off");
        }

        public void setDeliveryBoy(String item) {
            TextView subtxt = mView.findViewById(R.id.deliveryBoyDetails);
            subtxt.setText("Accepted By : "+item);
        }
        public  void setCallBtn(final String phone){
            TextView tv = mView.findViewById(R.id.call_btn);
            tv.setVisibility(View.VISIBLE);
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:"+phone));
                    startActivity(intent);
                }
            });

        }

        public void setDeliveryStatus(String totalCost) {
            TextView book_name = mView.findViewById(R.id.delivery_status);
            book_name.setText(totalCost);
            if (totalCost.equals("Order Placed"))
            {
                book_name.setTextColor(getResources().getColor(R.color.yello));
                book_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_yello_dot, 0, 0, 0);
            }
            else if (totalCost.equals("Cancelled"))
            {
                book_name.setTextColor(getResources().getColor(R.color.red));
                book_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_red_dot, 0, 0, 0);
            }
            else {
                book_name.setTextColor(getResources().getColor(R.color.green));
                book_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_green_dot, 0, 0, 0);
            }

        }

    }
}
