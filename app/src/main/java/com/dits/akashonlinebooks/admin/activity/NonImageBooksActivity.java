package com.dits.akashonlinebooks.admin.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Parcelable;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.adapter.BookDetailGridAdapter;
import com.dits.akashonlinebooks.admin.adapter.BooksDetailsAdapter;
import com.dits.akashonlinebooks.admin.extra.ApiClient;
import com.dits.akashonlinebooks.admin.extra.ApiInterface;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.dits.akashonlinebooks.admin.fragments.BookListFragment;
import com.dits.akashonlinebooks.admin.fragments.FilterFragment;
import com.dits.akashonlinebooks.admin.model.BookDataModel;
import com.dits.akashonlinebooks.admin.model.OfferGetIdModel;
import com.dits.akashonlinebooks.admin.model.SearchResultModel;
import com.dits.akashonlinebooks.admin.utils.PaginationAdapterCallback;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NonImageBooksActivity extends AppCompatActivity implements PaginationAdapterCallback,BookDetailGridAdapter.ItemLongClickListener,
        BookDetailGridAdapter.ItemListener, FilterFragment.ClearFilterListener,FilterFragment.SetFilter {
        SearchView searchView;
        TextView heading;
        RecyclerView recyclerView;
        BookDetailGridAdapter bookDetailsAdapter;
        ArrayList<BookDataModel> bookData;
        ProgressDialog progressDialog;
        ArrayList<BookDataModel> bookList;
        FloatingActionButton addBtn;
        Parcelable state;
        LinearLayoutManager linearLayoutManager;
        List<OfferGetIdModel> offerList;
        String id;
        private ApiInterface movieService;
        public Context ctx;
        ImageView filterBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_non_image_books);
        ctx = this;
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        bookList = new ArrayList<>();


        movieService = ApiClient.getClient().create(ApiInterface.class);

        searchView = findViewById(R.id.search_bar);
        addBtn = findViewById(R.id.addBtn);
        heading = findViewById(R.id.titleText);
        heading.setText("Book Details");


        offerList = new ArrayList();

        recyclerView = findViewById(R.id.recycler_view);

        linearLayoutManager = new LinearLayoutManager(this);
        if(state != null) {
            linearLayoutManager.onRestoreInstanceState(state);
        }
        recyclerView.setLayoutManager( new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false));

        bookDetailsAdapter = new BookDetailGridAdapter(getApplicationContext(),NonImageBooksActivity.this,NonImageBooksActivity.this,NonImageBooksActivity.this);
        recyclerView.setAdapter(bookDetailsAdapter);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                bookDetailsAdapter.getFilter().filter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                bookDetailsAdapter.getFilter().filter(s);
                return false;
            }
        });

        filterBtn = findViewById(R.id.filter_btn);
        filterBtn.setVisibility(View.VISIBLE);
        filterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                FilterFragment filterFragment = new FilterFragment();
                filterFragment.show(ft, "dialog");
            }
        });
        loadDetails();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    private void loadDetails() {
        bookList.clear();
        progressDialog.show();
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<List<BookDataModel>> call = apiService.getNonImageBooks();
        call.enqueue(new Callback<List<BookDataModel>>() {
            @Override
            public void onResponse(Call<List<BookDataModel>> call, retrofit2.Response<List<BookDataModel>> response) {
                for (BookDataModel result : response.body()) {
                    bookList.add(result);
                }
                showList();

            }

            @Override
            public void onFailure(Call<List<BookDataModel>> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }
    private void showList(){
        progressDialog.dismiss();
        bookDetailsAdapter.addList(bookList);
        bookDetailsAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        bookDetailsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(String key, ImageView imageView) {
        Intent i = new Intent(NonImageBooksActivity.this,BookDetailEditActivity.class);
        i.putExtra("key",key);
        startActivity(i);
    }

    @Override
    public void onItemLongClickListener(final String key) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete");
        builder.setMessage("Do you want to delete this book from offer zone?");
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                progressDialog.show();


            }
        });
//        builder.show();
    }

    @Override
    public void retryPageLoad() {

    }

    @Override
    public void clearFilterListener() {
        bookDetailsAdapter.getFilter().filter("");
    }

    @Override
    public void setFilter(String filterKey, String filterBy) {
        bookDetailsAdapter.getFilter().filter(filterBy);
    }
}
