package com.dits.akashonlinebooks.admin.model;

public class DeliveryBoyModel {

    String deliveryBoyModel,Phone,AdminType,SetAvailable;

    public DeliveryBoyModel() {
    }

    public DeliveryBoyModel(String deliveryBoyModel, String phone, String adminType, String setAvailable) {
        this.deliveryBoyModel = deliveryBoyModel;
        Phone = phone;
        AdminType = adminType;
        SetAvailable = setAvailable;
    }

    public String getDeliveryBoyModel() {
        return deliveryBoyModel;
    }

    public void setDeliveryBoyModel(String deliveryBoyModel) {
        this.deliveryBoyModel = deliveryBoyModel;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getAdminType() {
        return AdminType;
    }

    public void setAdminType(String adminType) {
        AdminType = adminType;
    }

    public String getSetAvailable() {
        return SetAvailable;
    }

    public void setSetAvailable(String setAvailable) {
        SetAvailable = setAvailable;
    }
}

