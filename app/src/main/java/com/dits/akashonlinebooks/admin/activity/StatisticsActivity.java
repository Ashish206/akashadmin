package com.dits.akashonlinebooks.admin.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dits.akashonlinebooks.admin.R;
import com.dits.akashonlinebooks.admin.extra.Constant;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class StatisticsActivity extends AppCompatActivity {

    DatabaseReference bookRef,orderRef,userRef;
    TextView totalUser,totalBooks,totalOrders,totalOrderDelivered,totalOrderNotDelivered,totalOrdersCancelled,totalEarnings,dateTxt;
    ImageView calBtn;
    ProgressDialog progressDialog;
    DatePickerDialog datePickerDialog;
    Calendar c;
    int year;
    int month;
    int day;
    DateFormat dateFormat;
    String formatedDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        bookRef = Constant.rootRef.child("BooksDetail");
        orderRef = Constant.rootRef.child("Orders").child("Jabalpur").child("Orders");
        userRef = Constant.rootRef.child("UserInfo").child("Jabalpur");

        totalUser = findViewById(R.id.totalUser);
        totalBooks = findViewById(R.id.totalBooks);
        totalOrders = findViewById(R.id.totalOrders);
        totalOrderDelivered = findViewById(R.id.totalOrderDelivered);
        totalOrdersCancelled = findViewById(R.id.totalOrdersCancelled);
        totalEarnings = findViewById(R.id.totalEarnings);
        totalOrderNotDelivered = findViewById(R.id.totalOrderNotDelivered);
        calBtn = findViewById(R.id.calanderBtn);
        dateTxt = findViewById(R.id.date);



        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);


        dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);


        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                int modMonth = month+1;
                try {
                    Date oldDate = new SimpleDateFormat("yyyy-MM-dd").parse(year+"-"+modMonth+"-"+dayOfMonth);
                    formatedDate = dateFormat.format(oldDate);
                    dateTxt.setText(formatedDate);
                    getStatistics(formatedDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        },year,month,day);

        calBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show();
            }
        });


        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                totalUser.setText("Total Users : "+dataSnapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        getStatistics("all");
    }

    void getStatistics(String date){
        progressDialog.show();
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = Constant.BASE_URL+"getStatistics?date="+date;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                setData(response);
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(StatisticsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(stringRequest);
    }

    private void setData(String response) {
        try {
            JSONArray jsonArray = new JSONArray(response);
            JSONObject job = jsonArray.getJSONObject(0);

            totalBooks.setText("Total Books : "+job.optString("totalBooks"));
            totalOrders.setText("Total Orders : "+job.optString("totalOrder"));
            totalOrderDelivered.setText("Total Delivered Orders : "+job.optString("deliveredOrder"));
            totalOrderNotDelivered.setText("Total Undelivered Orders : "+job.optString("undeliveredOrder"));
            totalOrdersCancelled.setText("Total Cancelled Orders : "+job.optString("cancelledOrder"));
            totalEarnings.setText("Total Earnings : "+job.optString("totalEarnings"));


        }catch (Exception e){
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

}
